package es.uam.clavi.test.view;

import javax.swing.JFrame;

import es.uam.clavi.controllers.FilterPanelController;

public class AuctionFrame extends JFrame {
	
	private static final long serialVersionUID = -3790263811790691101L;

	public AuctionFrame (FilterPanelController parent) {
		super ("Clavichord");
		add (parent.getAucP());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350,180);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}
}
