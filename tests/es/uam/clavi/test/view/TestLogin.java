package es.uam.clavi.test.view;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.gpers.GpersLoadException;
import es.uam.clavi.controllers.FrameController;
import es.uam.clavi.controllers.LoginPanelController;

/**
 * Pone a prueba el despliegue visual de la ventana de artículos.
 * @author Miguel Laseca
 * @version 1.3
 */
public class TestLogin {

	public static void main (String[] args) throws GclienLoadException, GpersLoadException, GartLoadException {
		Aplicacion app = new Aplicacion ();
		FrameController fc = FrameController.getInstance();
		fc.setModel(app);
		LoginPanelController controller = LoginPanelController.getInstance(fc);
		new LoginFrame(controller);
	}
}
