package es.uam.clavi.test.view;

import javax.swing.JFrame;

import es.uam.clavi.controllers.FilterPanelController;


public class ArticleFrame extends JFrame {

	private static final long serialVersionUID = 5870987435160859125L;
	
	public ArticleFrame (FilterPanelController parent) {
		super ("Clavichord");
		add (parent.getArtP());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350,180);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}
}
