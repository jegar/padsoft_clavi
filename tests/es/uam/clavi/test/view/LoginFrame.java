package es.uam.clavi.test.view;

import javax.swing.JFrame;

import es.uam.clavi.controllers.LoginPanelController;

/**
 * 
 * @author miguel
 * @version 1.0
 */
public class LoginFrame extends JFrame {
	
	private static final long serialVersionUID = 7182345147152172377L;

	public LoginFrame (LoginPanelController parent) {
		super ("CLAVICHORD");
		add (parent.getPanel());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350,180);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}
}
