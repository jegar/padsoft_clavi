package es.uam.clavi.test.view;

import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.controllers.FrameController;
import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.gpers.GpersLoadException;

public class TestArticle {
	
	public static void main (String[] args) throws GclienLoadException, GpersLoadException, GartLoadException {
		Aplicacion app = new Aplicacion();
		// Para comprobar los botones del admin
		app.validarLogin("admin", "1234");
		
		FrameController cardManager = FrameController.getInstance();
		cardManager.setModel(app);
		FilterPanelController controller = FilterPanelController.getInstance(cardManager);
		new ArticleFrame(controller);
	}
}
