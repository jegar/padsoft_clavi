package es.uam.clavi.test.view;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.gpers.GpersLoadException;
import es.uam.clavi.controllers.FrameController;
import es.uam.clavi.view.frames.Frame;;

public class TestSwing {

	public static void main(String[] args) {
		try {
			Aplicacion modelo = new Aplicacion();
			FrameController controlador = FrameController.getInstance();
			Frame vista = new Frame();
			vista.setController(controlador);
			controlador.setView(vista);
			controlador.setModel(modelo);
		} catch (GclienLoadException | GpersLoadException | GartLoadException e) {
			e.printStackTrace();
			return;
		}
	}
}
