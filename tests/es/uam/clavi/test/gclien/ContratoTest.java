package es.uam.clavi.test.gclien;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gclien.Normal;
import es.uam.clavi.model.gclien.Premium;

public class ContratoTest {

	private Premium premium;
	private Normal normal;
	@Before
	public void setUp() throws Exception {
		premium = new Premium();
		normal = new Normal();
	}

	@Test
	public void testCaducado() {
		assertFalse(premium.caducado());
	}

	@Test
	public void testAddSubsUsadas(){
		normal.addSubUsadas();
		normal.addSubUsadas();
		assertTrue(normal.getUsadas() == 2);
	}
	
	@Test
	public void testGastoVentas(){
		normal.addGastoVentas((float)10.50);
		normal.addGastoVentas(100);
		normal.subGastoVentas((float)25.25);
		assertTrue(normal.getGastoVentas()==(float) 85.25);
	}
}
