package es.uam.clavi.test.gclien;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.*;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.*;

import java.io.*;
import java.util.Collection;

public class GestorClientesTest {

	private String path_to_file = "tests" + File.separator + "test_save";
	private GestorArticulos gart;
	private GestorClientes gestor;
	
	/**
	 * Instanciamos el gestor primero
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		gart = new GestorArticulos (path_to_file);
		gestor = new GestorClientes(path_to_file);
	}

	/**
	 * Si hemos realizado la prueba de guardado, se habrá quedado este archivo,
	 * el cual tendremos que eliminar
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception{
		File file = new File(path_to_file + File.separator +"gclien");
		if(file.isFile()){
			file.delete();
			File dir = new File(file.getParent());
			if (dir.isDirectory())
				dir.delete();
		}
	}

	
	/**
	 * Anyadimos y buscamos un par de clientes 
	 */
	@Test
	public void testAnhBusCliente() {
		gestor.nuevoCliente("910123456", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("919876543", "elmio", "", "", "", "", "", "",
				"", "", "", "", "", "");
		Cliente cliente1 = gestor.buscarCliente("910123456");
		Cliente cliente2 = gestor.buscarCliente("919876543");
		Cliente cliente3 = gestor.buscarCliente("999999999");/*Este debería dar null*/
		
		assertNull(cliente3);
		assertEquals(cliente1.geteMail(),"estoesun@email.com");
		assertEquals(cliente2.getNombre(), "elmio");
		
	}

	/**
	 * Le añadimos un contrato a un cliente y comprobamos que lo tiene
	 */
	@Test
	public void testClientContr() {
		gestor.nuevoCliente("910123456", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("919876543", "elmio", "", "", "", "", "", "",
				"", "", "", "", "", "");
		/*Comprobamos que si le pedimos los clientes con contrato, devuelve una lista vacia*/
		assertTrue(gestor.clientConContr().isEmpty());
		/*Comprobamos que si es un cliente sin email, devuelve false*/
		assertFalse(gestor.nuevoContrato(gestor.buscarCliente("919876543"), true));
		/*Nos aseguramos de que, efectivamente, no ha anhadido contrato a la lista*/
		assertTrue(gestor.clientConContr().isEmpty());
		/*Comprobamos al situacion de exito*/
		Cliente cliente = gestor.buscarCliente("910123456");
		assertTrue(gestor.nuevoContrato(cliente, true));
		assertEquals(gestor.clientConContr().get(0), cliente);
	}

	/**
	 * Comprueba la creacción correcta de contratos y modificacion
	 */
	@Test
	public void testCaractContr() {

		assertTrue(gestor.getPrecioNormal()== (float)25);
		assertTrue(gestor.getSubGratisNormal() == (int)5);
		assertTrue(gestor.getGastoMenudNormal()== (float)200);
		assertTrue(gestor.getPrecioPrem()== (float)100);
		assertTrue(gestor.getDescEnvPrem()== (int)50);
		gestor.modContrNorm((float)12.0, 32, 29, (float)124.50);
		gestor.modContrPrem((float)15.30, 10, 1, (float)200.22);
		assertTrue(gestor.getPrecioNormal() == (float)124.50);
		assertTrue(gestor.getSubGratisNormal() == (int)29);
		assertTrue(gestor.getGastoMenudNormal() == (float)12.0);
		assertTrue(gestor.getPrecioPrem() == (float)200.22);
		assertTrue(gestor.getDescEnvPrem() == (int)10);
	}

	/**
	 * Notifica de una subasta a los clientes que corresponda 
	 */
	@Test
	public void testNotificarSubasta(){
		try{
			Subasta subasta = new Subasta(1, 50, "Este es un mensaje", 5,
					new Arte (1, "cosa", new java.util.Date(), 12, 34, TipoObra.Escultura,
							true,"Piezazzo", "1990", gart), gart);
			gestor.nuevoCliente("910123456", "un_nombre", "un primerApellido", "un segundoApellido",
					"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
					"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
			gestor.nuevoCliente("910123452", "un_nombre", "un primerApellido", "un segundoApellido",
					"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
					"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
			gestor.nuevoCliente("920123452", "un_nombre", "un primerApellido", "un segundoApellido",
					"789101112", "131415161", "718192021", "", "c/Falsa 123", "Gotham City",
					"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
			assertTrue(gestor.nuevoContrato(gestor.buscarCliente("910123456"), true));
			assertTrue(gestor.nuevoContrato(gestor.buscarCliente("910123452"), true));
			assertFalse(gestor.nuevoContrato(gestor.buscarCliente("920123452"), true));
			assertTrue(gestor.notificarSubasta(subasta).isEmpty());
		}catch (Exception e){
			fail("Error, se ha lanzado" + e);
			e.printStackTrace();
		}
	}

	/**
	 * Comprobamos si lleva bien las gestion de cantidad de pujas gratuitas
	 */
	@Test
	public void testPujasGratis() {
		gestor.nuevoCliente("920123452", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("920123453", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesotr@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		Cliente cl = gestor.buscarCliente("920123452");
		Cliente cl2 = gestor.buscarCliente("920123453");
		gestor.nuevoContrato(cl,false);
		gestor.nuevoContrato(cl2, true);
		try{
			assertTrue(gestor.pujasGratis(cl));
			assertTrue(gestor.pujasGratis(cl2));
			int i = 0;
			while (gestor.gastarSubasta(cl)){
				i++;
			}
			assertTrue(i == 5);
			assertFalse(gestor.pujasGratis(cl));
			assertFalse(gestor.gastarSubasta(cl));
			assertTrue(gestor.pujasGratis(cl2));
		}catch (ContractNotFoundException e){
			e.printStackTrace();
			fail("Contrato no encontrado, error");
		}
		
	}

	/*
	 * Guardar y cargar gestor
	 */
	@Test
	public void testCerrarGestor() {
		gestor.nuevoCliente("920123452", "un_nombre", "un primerApellido", "un segundoApellido",
			"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
			"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("920123453", "un_nombre", "un primerApellido", "un segundoApellido",
			"789101112", "131415161", "718192021", "estoesotr@email.com", "c/Falsa 123", "Gotham City",
			"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		Cliente cl = gestor.buscarCliente("920123452");
		Cliente cl2 = gestor.buscarCliente("920123453");
		gestor.nuevoContrato(cl,false);
		gestor.nuevoContrato(cl2, true);
		cl = null;
		cl2 = null;
		try {
			gestor.cerrarGestor();
		} catch (GclienSaveException e) {
			fail(e.toString());
		}
		try {
			gestor = new GestorClientes(path_to_file);
		} catch (GclienLoadException e) {
			fail(e.toString());
		}
		cl = gestor.buscarCliente("920123452");
		assertNotNull(cl);
		cl2 = gestor.buscarCliente("920123453");
		assertNotNull(cl2);
		assertTrue("un segundoApellido".compareTo(cl2.getSegundoApellido()) == 0 );
	}

	/*
	 * Para comprobar que los filtrados funcionan vamos a comprobar los singulares y luego comprobaremos
	 * el general
	 */

	@Test
	public void testFiltrado() {
		gestor.nuevoCliente("910123452", "otro_nombre", "un primerApellido", "un segundoApellido",
			"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
			"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("920123452", "un_nombre", "otro primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("930123452", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesotr@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("940123452", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		gestor.nuevoCliente("950123452", "un_nombre", "un primerApellido", "un segundoApellido",
				"789101112", "131415161", "718192021", "estoesun@email.com", "c/Falsa 123", "Gotham City",
				"The Carpathian Fangs", "Uno de ellos", "98065", "Tyrell Corporation");
		Cliente cl1 = gestor.buscarCliente("910123452");
		Cliente cl2 = gestor.buscarCliente("920123452");
		Cliente cl3 = gestor.buscarCliente("930123452");
		Cliente cl4 = gestor.buscarCliente("940123452");
		Cliente cl5 = gestor.buscarCliente("950123452");
		gestor.nuevoContrato(cl1, true);
		gestor.nuevoContrato(cl2, true);
		gestor.nuevoContrato(cl3, true);
		gestor.nuevoContrato(cl4, true);
		
		Collection<Cliente> clientes = gestor.filtrado(true, null, null, null);
		assertTrue(clientes.contains(cl3) && !clientes.contains(cl5) && clientes.contains(cl1)
				&& clientes.contains(cl2) && clientes.contains(cl4));
		
		clientes = gestor.filtrado(false, "un_nombre", "", "");
		assertTrue(clientes.contains(cl3) && clientes.contains(cl5) && !clientes.contains(cl1)
				&& clientes.contains(cl2) && clientes.contains(cl4));
		
		clientes = gestor.filtrado(false, "", "un primerApellido", "");
		assertTrue(clientes.contains(cl3) && clientes.contains(cl5) && clientes.contains(cl1)
				&& !clientes.contains(cl2) && clientes.contains(cl4));
		
		clientes = gestor.filtrado(false, "", "", "estoesun@email.com");
		assertTrue(!clientes.contains(cl3) && clientes.contains(cl5) && clientes.contains(cl1)
				&& clientes.contains(cl2) && clientes.contains(cl4));
		
		clientes = gestor.filtrado(true, "un_nombre", "un primerApellido","estoesun@email.com");
		assertTrue(!clientes.contains(cl3) && !clientes.contains(cl5) && !clientes.contains(cl1)
				&& !clientes.contains(cl2) && clientes.contains(cl4));
	}
}
