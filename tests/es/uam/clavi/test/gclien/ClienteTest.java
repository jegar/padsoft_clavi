package es.uam.clavi.test.gclien;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.Contrato;
import es.uam.clavi.model.gclien.Premium;;

public class ClienteTest {

	private Cliente cliente;
	private Cliente cliente2;
	
	@Before
	public void setUp() throws Exception {
		cliente = new Cliente("910123456", "nombre", "primerApellido", "segundoApellido",
				"nif", "dnia", "movil", "eMail@gmail.com", "c/direccion", "ciudad",
				"provincia", "pais", "cp", "empresa");
		cliente2 = new Cliente("910123456", "nombre");
		Contrato contrato = new Premium();
		cliente.setContrato(contrato);
		Contrato contrato2 = new Premium();
		cliente.setContrato(contrato2);
	}

	@Test
	public void testPoseeContrato() {
		assertTrue(cliente.poseeContrato());
		assertFalse(cliente2.poseeContrato());
	}

}
