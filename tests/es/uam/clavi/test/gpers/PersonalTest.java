package es.uam.clavi.test.gpers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gpers.Empleado;
import es.uam.clavi.model.gpers.Gerente;

public class PersonalTest {

	private Empleado empl;
	private Gerente ger;
	
	@Before
	public void setUp() throws Exception {
		empl = new Empleado("user", "1111", "", "", "", "", "");
		ger = new Gerente("admin","1234");
	}

	@Test
	public void testValidar() {
		assertTrue(empl.validar("1111"));
		assertFalse(empl.validar("0000"));
		assertTrue(ger.validar("1234"));
	}
}
