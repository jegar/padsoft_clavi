package es.uam.clavi.test.gpers;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gpers.Empleado;
import es.uam.clavi.model.gpers.GestorPersonal;
import es.uam.clavi.model.gpers.GpersLoadException;
import es.uam.clavi.model.gpers.GpersSaveException;
import es.uam.clavi.model.gpers.Personal;

public class GestorPersonalTest {

	private GestorPersonal gestor;
	private String path;
	private Personal personal;
	
	@Before
	public void setUp() throws Exception {
		path = "test_gpers";
		gestor = new GestorPersonal(path);
		personal = new Empleado("user","1234","tu","","","","");
	}

	@After
	public void tearDown() throws Exception{
		File file = new File(path + File.separator +"gpers");
		if(file.isFile()){
			file.delete();
			File dir = new File(file.getParent());
			if (dir.isDirectory())
				dir.delete();
		}
	}
	
	@Test
	public void testPutGet() {
		gestor.put(personal.getUsername(), personal);
		assertEquals(gestor.get(personal.getUsername()),personal);
	}

	@Test
	public void testNuevoEmpleado() {
		gestor.nuevoEmpleado("another", "4321", "", "", "", "", "");
		assertEquals(gestor.get("another").getUsername(),"another");
	}

	@Test
	public void testAbrirCerrarGestor() {
		gestor.nuevoEmpleado("dood", "4321", "", "", "", "", "");
		gestor.nuevoEmpleado("usr", "4321", "carlos", "", "", "", "");
		try {
			gestor.cerrarGestor();
		} catch (GpersSaveException e) {
			fail(e.toString());
		}
		try {
			gestor = new GestorPersonal(path);
		} catch (GpersLoadException e) {
			fail(e.toString());
		}
		Personal cl = gestor.get("dood");
		assertNotNull(cl);
		Personal gl = gestor.get("admin");
		Personal cl2 = gestor.get("usr");
		assertTrue(gl.getUsername().compareTo("admin")==0);
		Empleado emp2 = (Empleado)cl2;
		assertTrue(emp2.getNombre().compareTo("carlos")==0);
	}

	@Test
	public void testModEmpleado() {
		gestor.nuevoEmpleado("usr", "4321", "carlos", "", "", "", "");
		gestor.modEmpleado((Empleado)gestor.get("usr"), "usr2 el retorno",
				"DICTS", "paco", "alguno tendra", "hay gente mu rara", "324567123", "987654");
		assertNull(gestor.get("usr"));
		Empleado empl = (Empleado)gestor.get("usr2 el retorno");
		assertEquals(empl.getApellido1(),"alguno tendra");
	}
}
