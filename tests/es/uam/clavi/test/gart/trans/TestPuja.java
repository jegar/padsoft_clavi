package es.uam.clavi.test.gart.trans;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.trans.Puja;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.GestorClientes;

public class TestPuja {
	
	private GestorClientes gclien;
	private Cliente cl;
	private Puja p;
	private String path;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		new GestorArticulos(path);
		gclien = new GestorClientes ("es/uam/clavi");
		gclien.nuevoCliente("1234567890",
				"Philip", "Fry", "No me acuerdo", "Tengo papelé", "1",
				"555555555", "foo@foo.com", "C. Falsa 123", "Nueva Nueva York",
				"'murica", "Tierra", "00000", "Planet Express");
		cl = gclien.buscarCliente("1234567890");
		p = new Puja (5, cl);
	}
	
	@Test
	public void testBuild() {
		assertNotNull (p);
		assertEquals (p.getCl(), cl);
		assertTrue (p.getCantidad() == 5);
	}

	@Test
	public void testValidarPuja () {
		assertFalse (p.validarPuja((float)5.04));
		assertTrue (p.validarPuja((float)5.05));
	}
}
