package es.uam.clavi.test.gart.trans;

import static org.junit.Assert.*;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Arte;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.GestorClientes;
import es.uam.eps.padsof.emailconnection.FailedInternetConnectionException;
import es.uam.eps.padsof.emailconnection.InvalidEmailAddressException;

public class TestSub {

	private Articulo art;
	private GestorArticulos gart;
	private GestorClientes gclien;
	private Subasta s;
	private String path;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		gart = new GestorArticulos (path);
		gclien = new GestorClientes ("es/uam/clavi");
		try {
			art = new Arte (4, "Niña con flores (Óleo)",
					(new SimpleDateFormat("dd/MM/yy")).parse ("08/10/2014"),
					150, 900,TipoObra.Pintura, false, "Meyer Von Bremen", "185x", gart);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		s = new Subasta (1, 100, "Probando las subastas.", 0, art, gart);
	}
	
	@Test
	public void testBuild () {
		assertNotNull (s);
		assertEquals (s.getArt(), art);
		assertNull (s.getCl());
		assertNotNull (s.getClientes());
		assertTrue (s.getClientes().isEmpty());
		assertFalse (s.ultimoDia());
	}
	
	@Test
	public void testIsIniciada () throws InvalidEmailAddressException, FailedInternetConnectionException {
		assertFalse (s.isIniciada());
		gclien.nuevoCliente("1234567890",
		"Philip", "Fry", "No me acuerdo", "Tengo papelé", "1",
		"555555555", "foo@foo.com", "C. Falsa 123", "Nueva Nueva York",
		"'murica", "Tierra", "00000", "Planet Express");
		Cliente cl = gclien.buscarCliente("1234567890");
		assertFalse (s.anh(cl, (float)900.04));
		assertTrue (s.anh(cl, (float) 900.05));
	}
	
	@Test
	public void testAnh () throws InvalidEmailAddressException, FailedInternetConnectionException {
		gclien.nuevoCliente("1234567890",
				"Philip", "Fry", "No me acuerdo", "Tengo papelé", "1",
				"555555555", "foo@foo.com", "C. Falsa 123", "Nueva Nueva York",
				"'murica", "Tierra", "00000", "Planet Express");
		Cliente cl = gclien.buscarCliente("1234567890");
		assertFalse (s.anh(cl, (float)900.04));
		assertTrue (s.anh(cl, (float)900.05));
		assertFalse (s.anh(cl, (float)900.05));
		Cliente cl1 = new Cliente ("A", "A", "A", "foo", "foo", "foo", "foo", "foo1@foo.com", "foo","foo",
				"foo", "foo", "foo", "foo");
		assertTrue (s.anh(cl1, 950));
		assertTrue (s.getClientes().containsValue(cl));
		assertTrue (s.getClientes().containsValue(cl1));
	}
	
	@Test (expected = InvalidEmailAddressException.class)
	public void testExceptionEmailSystem () throws InvalidEmailAddressException, FailedInternetConnectionException {
		s.setDuracion(1);
		Cliente cl1 = new Cliente ("A", "A", "A", "foo", "foo", "foo", "foo", "farol", "foo","foo",
				"foo", "foo", "foo", "foo");
		s.anh(cl1, 950);
	}
	
	@Test (expected = NotValidArtException.class)
	public void testException () throws NotValidArtException {
		art.setEst(Estado.Vendido);
		gart.iniciarSubasta(100, "Probando las subastas", 0, art);
	}
}
