package es.uam.clavi.test.gart.trans;

import static org.junit.Assert.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Arte;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.Lote;
import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.SoldArtException;
import es.uam.clavi.model.gart.trans.Venta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.GestorClientes;

public class TestVen {

	private Articulo art;
	private GestorArticulos gart;
	private GestorClientes gclien;
	private Venta v;
	private Cliente cl;
	private String path;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		gart = new GestorArticulos (path);
		gclien = new GestorClientes ("es/uam/clavi");
		
		art = new Arte (4, "Niña con flores (Óleo)",
				(new SimpleDateFormat("dd/MM/yy")).parse ("08/10/2014"),
				150, 900,TipoObra.Pintura, false, "Meyer Von Bremen", "185x", gart);
		gclien.nuevoCliente("1234567890",
				"Philip", "Fry", "No me acuerdo", "Tengo papelé", "1",
				"555555555", "foo@foo.com", "C. Falsa 123", "Nueva Nueva York",
				"'murica", "Tierra", "00000", "Planet Express");
		cl = gclien.buscarCliente("1234567890");
		v = new Venta (1, cl, gart);
	}
	
	@Test
	public void testBuild () {
		assertNotNull (v);
		assertEquals (v.getCl(), cl);
		assertTrue (v.getId() == 1);
	}
	
	@Test (expected = NotValidArtException.class)
	public void testAnh () throws NotValidArtException, MalformedArtException {
		assertTrue(v.anhArticulo(art));
		assertTrue(v.getLista().contains(art));
		assertTrue(v.getLista().size() == 1);
		assertFalse(v.anhArticulo(new Lote (0, "Lote de pruebas.", Calendar.getInstance().getTime(), 0, 1, null, gart)));
	}
	
	@Test
	public void testEliArt1 () throws NotValidArtException, SoldArtException {
		assertTrue (v.anhArticulo(art));
		assertTrue (v.eliArticulo(art));
		assertTrue (v.getLista().isEmpty());
		assertNotEquals (art.getEst(), Estado.EnVenta);
	}
	
	@Test (expected = SoldArtException.class)
	public void testEliArt2 () throws NotValidArtException, SoldArtException {
		assertTrue (v.anhArticulo(art));
		v.getLista().get(0).setEst(Estado.Vendido);
		v.eliArticulo(art);
	}
	
	@Test
	public void testEliArt3 () throws NotValidArtException, SoldArtException {
		assertTrue (v.anhArticulo(art));
		assertTrue (v.eliArticulo(0));
		assertTrue (v.getLista().isEmpty());
		assertNotEquals (art.getEst(), Estado.EnVenta);
	}
	
	@Test (expected = SoldArtException.class)
	public void testEliArt4 () throws NotValidArtException, SoldArtException {
		assertTrue (v.anhArticulo(art));
		v.getLista().get(0).setEst(Estado.Vendido);
		v.eliArticulo(0);
	}
}
