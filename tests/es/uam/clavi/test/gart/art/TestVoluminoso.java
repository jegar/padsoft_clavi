package es.uam.clavi.test.gart.art;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Voluminoso;

public class TestVoluminoso {

	private Voluminoso v;
	
	@Before
	public void set () throws Exception {
		v = new Voluminoso (0, null, Calendar.getInstance().getTime(), 0, 1, 1, 1, 1, 1, null, new GestorArticulos ("save" + File.separator));
	}
	
	@Test
	public void testCalcularDescuento1() {
		assertTrue (v.clacularGastosEnv() == 20);
		v.setPeso(20);
		assertTrue (v.clacularGastosEnv() == 28);
		v.setAlto(201);
		assertTrue (v.clacularGastosEnv() == 50);
	}
}
