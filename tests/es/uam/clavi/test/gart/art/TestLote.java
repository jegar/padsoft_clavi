package es.uam.clavi.test.gart.art;

import static org.junit.Assert.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Arte;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.Lote;
import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.SoldArtException;

public class TestLote {

	private Articulo art;
	private String path;
	private GestorArticulos gart;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		gart = new GestorArticulos(path);
		art = new Arte (4, "Niña con flores (Óleo)",
				(new SimpleDateFormat("dd/MM/yy")).parse ("08/10/2014"),
				150, 900,TipoObra.Pintura, false, "Meyer Von Bremen", "185x", gart);
	}
	
	@Test
	public void testBuild1 () throws MalformedArtException {
		ArrayList<Articulo> list = new ArrayList<Articulo> (gart.getRepoArticulos().values());
		assertNotNull (new Lote (list.size()-1, "Lote de absolutamente todo.", Calendar.getInstance().getTime(), 0, 3000, "2016", list, gart));
		//gart.anhArticulo(l);
	}
	
	@Test
	public void testBuild2 () throws MalformedArtException {
		assertNotNull (new Lote (0, "Lote de pruebas.", Calendar.getInstance().getTime(), 0, 3000, "2016", gart));
	}

	@Test (expected = MalformedArtException.class)
	public void testBuild3 () throws MalformedArtException {
		new Lote (0, "Lote de pruebas.", Calendar.getInstance().getTime(), 0, 0, "2016", gart);
	}
	
	@Test
	public void testAnh () throws MalformedArtException, NotValidArtException, SoldArtException {
		Lote l = new Lote (0, "Lote de pruebas.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		assertTrue (l.anh(art));
		assertEquals (art.getEst(), Estado.EnLote);
	}
	
	@Test
	public void testAnh2 () throws MalformedArtException, NotValidArtException, SoldArtException {
		Lote l1 = new Lote (0, "Lote de pruebas 1.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		Lote l2 = new Lote (1, "Lote de pruebas 2.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		assertTrue (l1.anh(art));
		assertTrue (l2.anh(l1));
	}
	
	@Test (expected = NotValidArtException.class)
	public void testAnh3 () throws MalformedArtException, NotValidArtException, SoldArtException {
		Lote l1 = new Lote (0, "Lote de pruebas 1.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		Lote l2 = new Lote (1, "Lote de pruebas 2.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		assertTrue (l1.anh(art));
		l2.anh(art);
	}
	
	@Test
	public void testEquals () throws MalformedArtException, NotValidArtException, SoldArtException {
		Lote l1 = new Lote (0, "Lote de pruebas 1.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		Lote l2 = new Lote (0, "Lote de pruebas 1.", Calendar.getInstance().getTime(), 0, 3000, "2016",gart);
		assertEquals (l1, l2);
		l1.anh(art);
		assertNotEquals (l1, l2);
	}
	
	@Test
	public void testToString () throws MalformedArtException, NotValidArtException, SoldArtException {
		SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
		Lote l = new Lote (0, "Lote de pruebas.", Calendar.getInstance().getTime(), 0, 3000, "2016", gart);
		l.anh(art);
		assertEquals ("L;Lote de pruebas.;2016;"+sdf.format(Calendar.getInstance().getTime())+";0.00;3000.00;4", l.toString());
	}
}
