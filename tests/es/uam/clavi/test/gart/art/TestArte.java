package es.uam.clavi.test.gart.art;

import static org.junit.Assert.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Arte;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.TipoObra;

public class TestArte {

	private Arte art;
	private String path;
	private GestorArticulos gart;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		gart = new GestorArticulos (path);
		
		art = new Arte (4, "Niña con flores (Óleo)",
				(new SimpleDateFormat("dd/MM/yy")).parse ("08/10/2014"),
				150, 900,TipoObra.Pintura, false, "Meyer Von Bremen", "185x", gart);
	}
	
	@Test
	public void testException () {
		
	}
	
	@Test
	public void testEquals () {
		assertTrue (art.equals(gart.getRepoArticulos().get(art.getID())));
	}
	
	@Test
	public void testGetTipo () {
		assertNotEquals (art.getTipo(), gart.getRepoArticulos().get(0));
	}
	
	@Test
	public void testToString () {
		ArrayList<Articulo> list = gart.filtrarArtCert();
		assertNotNull (list.get(0));
		assertEquals ("A;Mater Amabilis Ora Pro Novis;1951;15/09/2014;65.00;120.00;Joan Llimona Bruguera;Grabado;S", list.get(0).toString());
	}

}
