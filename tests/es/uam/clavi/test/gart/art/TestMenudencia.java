package es.uam.clavi.test.gart.art;

import static org.junit.Assert.*;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.Menudencia;

public class TestMenudencia {

	private Articulo art;
	private String path;
	private GestorArticulos gart;
	
	@Before
	public void setup () throws Exception {
		
		path = "save" + File.separator;
		gart = new GestorArticulos (path);
		art = new Menudencia (4, "BOTELLA CRISTAL S.XIX MANO CON REVOLVER",
				(new SimpleDateFormat("dd/MM/yyyy")).parse ("21/12/2015"),
				55, 125, 5, "18xx", gart);
	}
	
	@Test (expected = MalformedArtException.class)
	public void testBuild () throws MalformedArtException, ParseException {
		new Menudencia (4, "BOTELLA CRISTAL S.XIX MANO CON REVOLVER",
				(new SimpleDateFormat("dd/MM/yyyy")).parse ("21/12/2015"),
				55, 125, 101, "18xx", gart);
	}

	@Test
	public void testToString () {
		assertEquals ("M;BOTELLA CRISTAL S.XIX MANO CON REVOLVER;18xx;21/12/2015;55.00;125.00;5", art.toString());
	}
}
