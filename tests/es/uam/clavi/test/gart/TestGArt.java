/**
 * @author Miguel Laseca
 * @version 1.2.1
 * Última modificación:
 * 1.2.1) Añadido dos assert para añadir pujas nuevas.
 * 1.2) Se ha adaptado el test al nuevo modelo de gestor.
 * También se ha añadido pruebas para la inicialización y búsqueda de artículos,
 * y para la inicialización de subastas.
 * @see GestorArticulos
 */

package es.uam.clavi.test.gart;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.*;
import es.uam.clavi.model.gart.trans.*;
import es.uam.clavi.model.gclien.*;
import es.uam.eps.padsof.emailconnection.FailedInternetConnectionException;
import es.uam.eps.padsof.emailconnection.InvalidEmailAddressException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TestGArt {

	private Articulo art;
	private GestorArticulos gart;
	private GestorClientes gclien;
	private Cliente cl;
	private String path;
	
	@Before
	public void setup() throws Exception {
		path = "save";
		gart = new GestorArticulos(path);
		gclien = new GestorClientes ("save");
		gclien.nuevoCliente("1234567890",
				"Philip", "Fry", "No me acuerdo", "Tengo papelé", "1",
				"555555555", "foo@foo.com", "C. Falsa 123", "Nueva Nueva York",
				"'murica", "Tierra", "00000", "Planet Express");
		cl = gclien.buscarCliente("1234567890");
		art = new Arte (4, "Niña con flores (Óleo)",
				(new SimpleDateFormat("dd/MM/yy")).parse ("08/10/2014"),
				150, 900,TipoObra.Pintura, false, "Meyer Von Bremen", "185x", gart);
	}
	
	@Test
	public void testActArt () throws Exception {
		setup ();
		Lote l = (Lote) gart.anhLote(null, Calendar.getInstance().getTime(), 0, 1, null);
		l.anh(art);
		assertEquals (art.getEst(), Estado.EnLote);
	}
	
	@Test
	public void testAnhArticulos () throws MalformedArtException, IOException, ParseException {
			assertEquals (20, gart.anhArticulos("art_nuevos.txt"));
	}
	
	@Test
	public void testArtCert () {
		assertTrue (gart.filtrarArtCert().contains(gart.buscarArticulo((long)5)));
	}
	
	@Test
	public void testArtDat () {
		assertTrue (gart.filtrarArtDat("19xx").size() == 2);
	}
	
	@Test
	public void testArtEst () {
		assertTrue (gart.filtrarArtEst(Estado.Vendible).size() == 6);
		assertTrue (gart.filtrarArtEst(Estado.Vendido).size() == 1);
	}
	
	@Test
	public void testArtFadq () throws ParseException {
		assertEquals (gart.filtrarArtFadq ((new SimpleDateFormat ("dd/MM/yyyy")).parse("7/7/2013")).size(), 1);
	}
	
	@Test
	public void testArtPrecio () {
		assertEquals (gart.filtrarArtPrecioM(125).size(), 5);
		assertEquals (gart.filtrarArtPreciom(110).size(), 3);
		assertEquals (gart.artPrecioM(gart.filtrarArtPreciom(120), 120).size(), 2);
	}
	
	@Test
	public void testArtRecien () {
		ArrayList<Articulo> list = GestorArticulos.artRecien(1);
		assertFalse (list.isEmpty());
	}
	
	@Test
	public void testCancelarSubasta () throws NotValidArtException, InvalidEmailAddressException, FailedInternetConnectionException {
		
		Subasta s = gart.iniciarSubasta(100, "Probando las subastas", 0, art);
		assertFalse (gart.cancelarSubasta(s));
		Articulo test = gart.getRepoArticulos().get((long)6);
		s = gart.iniciarSubasta(100, "Probando las subastas", 0, test);
		assertTrue (gart.cancelarSubasta(s));
		assertFalse (gart.getRepoSubastas().containsValue(s));
		s = gart.iniciarSubasta(100, "Probando las subastas", 0, test);
		s.anh(cl, (float) 900.06);
		assertFalse (gart.cancelarSubasta(s));
		assertTrue (gart.getRepoSubastas().containsValue(s));
	}
	
	@Test
	public void testElLote () throws MalformedArtException, NotValidArtException, SoldArtException {
		
		Lote l = (Lote) gart.anhLote(null, Calendar.getInstance().getTime(), 0, 1, null);
		l.anh(art);
		assertEquals (art.getEst(), Estado.EnLote);
		assertTrue (gart.elLote(l));
		assertEquals (art.getEst(), Estado.Ambos);
	}
	
	@Test (expected = NotValidArtException.class)
	public void testException () throws NotValidArtException {
		art.setEst(Estado.Vendido);
		gart.iniciarSubasta(100, "Probando las subastas", 0, art);
	}
	
	@Test
	public void testFiltArt () throws ParseException {
		
		assertFalse (gart.filtrarArtCert().contains(art));
		assertTrue (gart.filtrarArtDat("185x").contains(art));
		assertTrue (gart.filtrarArtEst(Estado.Ambos).contains(art));
		assertTrue (gart.filtrarArtFadq((new SimpleDateFormat("dd/MM/yy")).
						parse ("08/10/2014")).contains(art));
		assertTrue (gart.filtrarArtPreciom(901).contains(art));
		assertTrue (gart.filtrarArtPrecioM(899).contains(art));
		assertTrue (gart.filtrarArtTipo("Arte").contains(art));
	}
	
	@Test
	public void testInforme () throws NotValidArtException, ParseException {
		Venta v = gart.iniciarVenta(cl);
		v.anhArticulo(art);
		gart.finalizarVenta(v, 0, false);
		assertTrue (gart.getRepoFin().size() == 2);
		assertEquals (gart.informeMensual().size(), 2);
		assertTrue (gart.informeMensual().contains(v));
	}
	
	@Test
	public void testFinalizarSubastas () throws NotValidArtException, InvalidEmailAddressException, FailedInternetConnectionException {
		
		Subasta s;
			s = gart.iniciarSubasta(100, "Probando las subastas", 0, art);
			s.anh(cl, (float) 900.06);
			assertTrue (s.ultimoDia());
			gart.finalizarSubastas();
			assertTrue (art.getEst() == Estado.Vendido);
			assertTrue (gart.getRepoFin().containsValue(s));
	}
	
	@Test
	public void testFinalizarVenta () throws NotValidArtException {
		Venta v = gart.iniciarVenta(cl);
		v.anhArticulo(art);
		assertTrue (gart.finalizarVenta(v, 0, false) == 900);
		assertEquals (art.getEst(), Estado.Vendido);
	}
	
	@Test
	public void testFiltradoArt () {
		assertTrue (gart.filtrado(true, null, null, null, 0, 0, "Menudencia").isEmpty());
		assertTrue (gart.filtrado(false, null, Estado.Ambos, null, 0, 0, null).size() == 3);
	}
	
	@Test
	public void testFiltradoSub () throws NotValidArtException, InvalidEmailAddressException, FailedInternetConnectionException, ParseException {
		Subasta s = gart.iniciarSubasta(100, "Probando las subastas", 0, art);
		s.anh(cl, (float) 900.06);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		assertFalse (gart.filtrado(art, cl, null, null, 0, 0).isEmpty());
		assertFalse (gart.filtrado(art, null, null, null, 0, 0).isEmpty());
		assertFalse (gart.filtrado(null, cl, null, null, 0, 0).isEmpty());
		assertTrue (gart.filtrado(null, cl, Calendar.getInstance().getTime(), null, 0, 0).isEmpty());
		assertFalse (gart.filtrado(null, null, null, sdf.parse(sdf.format(Calendar.getInstance().getTime())), 0, 0).isEmpty());
		assertFalse (gart.filtrado(null, null, null, null, 900, 0).isEmpty());
		assertFalse (gart.filtrado(null, cl, null, null, 900, 0).isEmpty());
		assertFalse (gart.filtrado(null, null, null, null, 0, 901).isEmpty());
	}
	
	@Test
	public void testGuardar () throws ClassNotFoundException, IOException, MalformedArtException, ParseException, GartLoadException {
		gart.finDia ("save" + File.separator);
		gart = new GestorArticulos(path);
	}
	
	@Test
	public void testNuevaVenta () throws NotValidArtException {
		ArrayList<Articulo> list = new ArrayList<Articulo> ();
		list.add(art);
		assertTrue (gart.nuevaVenta(cl, list, false, 0) == 900);
		assertTrue (gart.getRepoFin().size() == 2);
	}
	
	@Test
	public void testVenArt () throws NotValidArtException {
		Venta v = gart.iniciarVenta(cl);
		v.anhArticulo(art);
		assertEquals (gart.filtrarVenArt(art), v);
	}
	
	@Test
	public void testVenCl () throws NotValidArtException {
		
		Venta v = gart.iniciarVenta(cl);
		v.anhArticulo(art);
		assertTrue (gart.filtrarVenCl(cl).contains(v));
	}
	
	@Test
	public void testVenFecha () {
		
		Venta v = gart.iniciarVenta(cl);
		assertTrue (gart.filtrarVenFecha(Calendar.getInstance().getTime(), false).contains(v));
	}
}