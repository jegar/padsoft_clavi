package es.uam.clavi.test.general;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.model.gpers.GpersLoadException;

public class AplicacionTest {

	private Aplicacion aplicacion;
	
	@Before
	public void setUp() throws Exception {
		aplicacion = new Aplicacion();
	}

	@After
	public void tearDown() throws Exception{
		aplicacion.validarLogin(null, null);
	}
	
	@Test
	public void testValidarLogin() {
		aplicacion.validarLogin("cosa", "1111");
		assertFalse(aplicacion.esAdmin());
		aplicacion.validarLogin("admin", "1234");
		assertTrue(aplicacion.esAdmin());
		aplicacion.validarLogin("admin", "1234");
		assertTrue(aplicacion.esAdmin());
	}

	@Test
	public void testCerrarAplicacion() throws InvalidUserException,
		GclienLoadException, GpersLoadException, GartLoadException {
		
		aplicacion.validarLogin("admin", "1234");
		aplicacion.nuevoEmpleado("usr", "12", "Nombrecosa", "", "", "", "");
		aplicacion.cerrarAplicacion();
		aplicacion = null;
		aplicacion = new Aplicacion();
		aplicacion.validarLogin("admin", "1234");
		assertTrue(aplicacion.buscarEmpleado("usr").getNombre().compareTo("Nombrecosa") == 0);
	}

	/**
	 * Este metodo tan solo se dedica a llamar a el metodo correspondiente de
	 * el gestor de clientes y contratos, por lo tanto si funciona alli funcionara
	 * aqui. Este Test solo es para si detecta correctamente el isAdmin;
	 */
	@Test
	public void testModContrs() {
		try{
			aplicacion.modContrNorm(0, 0, 0, 0);
		} catch (InvalidUserException e){
			aplicacion.validarLogin("admin", "1234");
			try{
				aplicacion.modContrNorm(0, 0, 0, 0);
			} catch (InvalidUserException f){
				fail("Ha saltado cuando no corresponde");
			}
			return;
		}
		fail("No ha saltado la excepcion");
	}

	/*@Test
	public void testNuevaSubasta() throws InvalidUserException {
		Articulo articulo= null;
		try {
			articulo = new Arte(1, "why would you", new Date(), 10, 100, TipoObra.Escultura, false, "fuuu", "idk");
		} catch (MalformedArtException e1) {
			e1.printStackTrace();
		}
		try{
			aplicacion.nuevaSubasta(12, 3, "SubAssTa", articulo);
		} catch (InvalidUserException e){
			aplicacion.validarLogin("admin", "1234");
			aplicacion.nuevaSubasta(120, 0, "Insta-basta, subasta al instante", articulo);
		}
	}*/

	@Test
	public void testModPasAdmin() throws InvalidUserException, GclienLoadException, GpersLoadException, GartLoadException {
		aplicacion.validarLogin("admin", "1234");
		aplicacion.modPasAdmin("1234", "1111");
		assertTrue(aplicacion.validarLogin("admin", "1111"));
		aplicacion.cerrarAplicacion();
		aplicacion = null;
		aplicacion = new Aplicacion();
		assertFalse(aplicacion.validarLogin("admin", "1234"));
		aplicacion.validarLogin("admin", "1111");
		aplicacion.modPasAdmin("1111", "1234");
		aplicacion.cerrarAplicacion();
	}

	@Test
	public void testBuscarEmpleado() throws InvalidUserException {
		aplicacion.validarLogin("admin", "1234");
		aplicacion.buscarEmpleado("usr");
	}

	@Test
	public void testCliente() throws InvalidUserException {
		aplicacion.validarLogin("admin", "1234");
		aplicacion.nuevoEmpleado("usr", "1111", "", "", "", "", "");
		aplicacion.validarLogin("usr", "1111");
		aplicacion.nuevoCliente("911234567", "nombre", "", "", "", "", "", "eMail@gmail.com",
				"", "", "", "", "", "");
		aplicacion.crearContrato(aplicacion.buscarCliente("911234567"), true);
		assertFalse(aplicacion.filtClien(true, "", "", "").isEmpty());
	}

}
