package es.uam.clavi.general;

import java.util.ArrayList;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.model.gpers.GpersLoadException;

public class Main {

	/**
	 * @param args
	 * @throws GpersLoadException 
	 * @throws GclienLoadException 
	 * @throws GartLoadException 
	 */
	public static void main(String[] args) throws GclienLoadException, GpersLoadException, GartLoadException {
		
		Aplicacion apl = new Aplicacion ();
		Cliente cl;
		if (!apl.validarLogin("admin", "1234")) return;
		if (!apl.esAdmin()) return;
		try {
			apl.nuevaSubasta(10, 2, "Probando subastas.", ((ArrayList<Articulo>)apl.filtArt(false, "185x", null, null, 0, 0, null)).get(0));
			apl.nuevoEmpleado("cosa", "1111", null, null, null, null, null);
			apl.validarLogin("cosa", "1111");
			if (!apl.nuevoCliente("1234", " ", " ", " ", " ", " ", null, "foo@foo.net", null, null, null, null, null, null)) {
				System.out.println ("Me cago en la hostia");
				return;
			}
			if ((cl = apl.buscarCliente("1234")) == null) System.out.println ("Mierda");
			if (apl.realizarPuja(((ArrayList<Subasta>)apl.filtSub
					(((ArrayList<Articulo>)apl.filtArt(false, "185x", null, null, 0, 0, null)).get(0),
							null, null, null, 0, 0)).get(0), apl.buscarCliente("1234"), 1050)) {
				System.out.println ("No debería tener certificado el payo este");
			}
			apl.crearContrato(cl, true);
			if (!apl.realizarPuja(((ArrayList<Subasta>)apl.filtSub
					(((ArrayList<Articulo>)apl.filtArt(false, "185x", null, null, 0, 0, null)).get(0),
							null, null, null, 0, 0)).get(0), apl.buscarCliente("1234"), 1050)) {
				System.out.println ("Debería funcionar esta vez");
			} 
		} catch (InvalidUserException e) {
			e.printStackTrace();
			return;
		}
		
	}

}
