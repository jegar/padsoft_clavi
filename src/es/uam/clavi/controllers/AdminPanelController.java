package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.AdminButtonNames;
import es.uam.clavi.view.panel.AdminPanel;

public class AdminPanelController implements ActionListener {

	private static AdminPanelController instance = null;
	private FrameController cardManager;
	private AdminPanel adminPanel;

	public static AdminPanelController getInstance(FrameController cardManager) {
		if (instance == null) {
			instance = new AdminPanelController(cardManager);
		}
		return instance;
	}

	private AdminPanelController(FrameController cardManager) {
		super();
		this.cardManager = cardManager;
	}

	public void setView(AdminPanel adminPanel) {
		this.adminPanel = adminPanel;
	}

	public AdminPanel getPanel() {
		return adminPanel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		ClaviButton<AdminButtonNames> selector;

		if (e.getSource() instanceof ClaviButton<?>) {
			if (((ClaviButton<?>) e.getSource()).getTag() instanceof AdminButtonNames) {
				selector = (ClaviButton<AdminButtonNames>) e.getSource();
				if (selector.getTag() instanceof AdminButtonNames) {

					switch ((AdminButtonNames) selector.getTag()) {
					case PRCN:
						this.cardManager.goToCard("ctrPropPan");
						break;
					case NVLT:
						this.cardManager.goToCard("lotePan");
						break;
					case NVAR:
						cardManager.goToCard("nuevoArtPan");
						break;
					case ANEM:
						cardManager.goToCard("emplPan");
						break;
					case CESE:
						this.cardManager.goToCard("logPan");
						break;
					case SALI: /* */
						try {
							this.cardManager.closeApp();
						} catch (InvalidUserException invUsrExcpt) {
							invUsrExcpt.printStackTrace();
							return;
						}
						break;
					case LIAR:
						cardManager.goToCard("artPan");
						break;
					case LISU:
						cardManager.goToCard("aucPan");
						break;
					default:
						return;
					}
				}
			}
		}
		return;
	}
}
