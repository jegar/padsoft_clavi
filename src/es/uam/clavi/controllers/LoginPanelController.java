package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.panel.LoginPanel;

/**
 * 
 * @author Miiguel Laseca Espiga
 * @version 2.0
 */
public class LoginPanelController implements ActionListener, KeyListener {

	private static LoginPanelController instance = null;
	private Aplicacion app;
	private LoginPanel panel;
	private FrameController cardManager;

	public static LoginPanelController getInstance(FrameController cardManager) {
		if (instance == null) {
			instance = new LoginPanelController(cardManager);
		}
		return instance;
	}

	private LoginPanelController(FrameController cardManager) {
		app = cardManager.getAplic();
		this.cardManager = cardManager;
		panel = new LoginPanel();
	}

	/**
	 * @return the panel
	 */
	public LoginPanel getPanel() {
		return panel;
	}

	/**
	 * 
	 * @param panel
	 */
	public void setView(LoginPanel panel) {
		this.panel = panel;
	}

	public void setModel(Aplicacion aplic) {
		this.app = aplic;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!app.validarLogin(panel.getText(), panel.getPassword())) {
			JOptionPane.showMessageDialog(null, "Invalid username or password");
		} else {
			if (app.esAdmin()) {
				this.cardManager.goToCard("admPan");
				this.panel.resetFields();
			} else {
				this.cardManager.getView().getUserPanel().getCurrentUser().setText(panel.getText());
				this.cardManager.getView().getUserPanel().getCurrentUser().setEditable(false);
				this.cardManager.goToCard("usrPan");
				this.panel.resetFields();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getExtendedKeyCode() != KeyEvent.VK_ENTER)
			return;
		ClaviButton<Void> button = (ClaviButton<Void>) e.getComponent();
		actionPerformed(new ActionEvent(button, 0, null));
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
