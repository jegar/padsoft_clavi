package es.uam.clavi.controllers;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.frames.Frame;

public class FrameController {

	private static FrameController instance = null;
	private Frame frame;
	private Aplicacion app;

	/**
	 * Constructor privado
	 */
	private FrameController() {
		super();
	}

	/**
	 * Instance para el Singleton
	 * 
	 * @return Una isnstance del controlador
	 */
	public static FrameController getInstance() {
		if (instance == null) {
			instance = new FrameController();
		}
		return instance;
	}

	/**
	 * Pon visible una carta por el nombre de la misma
	 * 
	 * @param name
	 *            El nombre de la carta
	 */
	public void goToCard(String name) {
		frame.getCards().show(frame.getDeck(), name);
		if (name.equals ("artPan")) frame.getArtPanel().setModel(GestorArticulos.artRecien(10));
	}

	/**
	 * Devuelve el modelo
	 * 
	 * @return El modelo (la aplicacion) con la que estamos interactuando
	 */
	public Aplicacion getAplic() {
		return this.app;
	}
	
	/**
	 * Devuelve la vista
	 * 
	 * @return La vista (la ventana) con la que estamos interactuando
	 */
	public Frame getView() {
		return frame;
	}

	/**
	 * Cierra la aplicacion
	 * 
	 * @throws InvalidUserException
	 *             La lanza cuando el usuario actual no esta autorizado a cerrar
	 *             la aplicacion
	 * @throws EndAplicException
	 */
	public void closeApp() throws InvalidUserException {
		this.app.cerrarAplicacion();
		this.frame.dispose();
	}

	/**
	 * Asocia el controlador a la vista
	 * 
	 * @param mainFrame
	 *            Vista del Marco principal
	 */
	public void setView(Frame mainFrame) {
		this.frame = mainFrame;
	}

	/**
	 * Asocia el modelo a la vista
	 * 
	 * @param aplicacion
	 *            Modelo (la aplicacion)
	 */
	public void setModel(Aplicacion aplicacion) {
		this.app = aplicacion;
	}
}
