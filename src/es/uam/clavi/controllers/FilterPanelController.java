package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.Lote;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.FilterButtonNames;
import es.uam.clavi.view.panel.filter.ArticlePanel;
import es.uam.clavi.view.panel.filter.AuctionPanel;
import es.uam.clavi.view.panel.filter.ClientPanel;
import es.uam.clavi.view.panel.filter.FilterTableModel;

public class FilterPanelController implements ActionListener, MouseListener, KeyListener {

	private static FilterPanelController instance = null;
	private ArticlePanel artP;
	private AuctionPanel aucP;
	private ClientPanel cliP;
	private FrameController cardManager;
	// Resto de los paneles

	public static FilterPanelController getInstance(FrameController cardManager) {
		if (instance == null) {
			instance = new FilterPanelController(cardManager);
		}
		return instance;
	}

	private FilterPanelController(FrameController cardManager) {
		this.cardManager = cardManager;
	}

	/**
	 * @return El panel de artículos.
	 */
	public ArticlePanel getArtP() {
		return artP;
	}

	/**
	 * @return El panel de subastas activas.
	 */
	public AuctionPanel getAucP() {
		return aucP;
	}

	/**
	 * @return the cliP
	 */
	public ClientPanel getCliP() {
		return cliP;
	}

	/**
	 * Ejecuta la acción correspondiente de cada botón de los paneles de
	 * filtrado en función de su identificación numérica.
	 * 
	 * @param e
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		ClaviButton<FilterButtonNames> button = (ClaviButton<FilterButtonNames>) e.getSource();
		try {
			switch ((FilterButtonNames) button.getTag()) {
			case FILTERA:
				Date d;
				if (artP.getDd().getText().equals("DD") || artP.getMm().getText().equals("MM")
						|| artP.getAa().getText().equals("AA"))
					d = null;
				else {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					d = sdf.parse(artP.getDd().getText() + "/" + artP.getMm().getText() + "/" + artP.getAa().getText());
				}
				Collection<Articulo> filteredArt = cardManager.getAplic().filtArt(artP.getCert().isSelected(),
						artP.getfDat().getText(), (Estado) artP.getEst().getSelectedItem(), d,
						Float.parseFloat(artP.getPreciom().getText()), Float.parseFloat(artP.getPrecioM().getText()),
						(String) artP.getTipo().getSelectedItem());
				if (filteredArt == null || filteredArt.isEmpty())
					JOptionPane.showMessageDialog(null, "No se han encontrado artículos con esas características");
				else
					artP.setModel(new ArrayList<Articulo>(filteredArt));
				break;
			case FILTERAU:
				Date D;
				if (aucP.getDd().getText().equals("DD") || aucP.getMm().getText().equals("MM")
						|| aucP.getAa().getText().equals("AA"))
					d = null;
				else {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					d = sdf.parse(aucP.getDd().getText() + "/" + aucP.getMm().getText() + "/" + aucP.getAa().getText());
				}
				if (aucP.getDD().getText().equals("DD") || aucP.getMM().getText().equals("MM")
						|| aucP.getAa().getText().equals("AA"))
					D = null;
				else {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					D = sdf.parse(aucP.getDd().getText() + "/" + aucP.getMm().getText() + "/" + aucP.getAa().getText());
				}
				Collection<Subasta> filteredAuc = cardManager.getAplic().filtSub(
						cardManager.getAplic().buscarArticulo(Long.parseLong(aucP.getArt().getText())),
						cardManager.getAplic().buscarCliente(aucP.getCl().getText()), D, d,
						Float.parseFloat(aucP.getPreciom().getText()), Float.parseFloat(aucP.getPrecioM().getText()));
				if (filteredAuc == null || filteredAuc.isEmpty())
					JOptionPane.showMessageDialog(null, "No se han encontrado subastas con esas características");
				else
					aucP.setModel(new ArrayList<Subasta>(filteredAuc));
				break;
			case FILTERC:
				Collection<Cliente> filteredCli = cardManager.getAplic().filtClien(cliP.getContrato().isSelected(),
						cliP.getNombre().getText(), cliP.getpApellido().getText(), cliP.geteMail().getText());
				if (filteredCli == null || filteredCli.isEmpty())
					JOptionPane.showMessageDialog(null, "No se han encontrado clientes con esas características");
				else
					cliP.setModel(new ArrayList<Cliente>(filteredCli));
				break;
			case LINK:
				ArrayList<Articulo> result = new ArrayList<Articulo>();
				result.add(cardManager.getAplic()
						.buscarLote((Long) artP.getModel().getValueAt(artP.getTable().getSelectedRow(), 0)));
				artP.setModel(result);
				break;
			case LINKS:
				cardManager.goToCard("aucPan");
				Articulo selected = cardManager.getAplic()
						.buscarArticulo((Long) artP.getModel().getValueAt(artP.getTable().getSelectedRow(), 0));
				ArrayList<Subasta> list = new ArrayList<>(
						cardManager.getAplic().filtSub(selected, null, null, null, 0, 0));
				aucP.setModel(list);
				break;
			case SELL:
				String tlf = JOptionPane.showInputDialog("Introduzca el teléfono del cliente:");
				if (cardManager.getAplic().buscarCliente(tlf) == null) JOptionPane.showMessageDialog
				(null, "El cliente "+tlf+" no existe en la base de datos", "Cliente no encontrado", JOptionPane.ERROR_MESSAGE);
				else cardManager.getAplic().nuevaVenta(cardManager.getAplic().buscarCliente(tlf), cardManager.getAplic()
						.buscarArticulo((Long) artP.getModel().getValueAt(artP.getTable().getSelectedRow(), 0)));
				break;
			case SPARE:
				Lote sel = (Lote) cardManager.getAplic()
						.buscarArticulo((Long) artP.getModel().getValueAt(artP.getTable().getSelectedRow(), 0));
				cardManager.getAplic().eliminarLote(sel);
				break;
			case NAUC:
				float entrada = 0;
				int duracion = 0;
				selected = null;
				try {
				selected = cardManager.getAplic()
				.buscarArticulo((Long) artP.getModel().getValueAt(artP.getTable().getSelectedRow(), 0));
				entrada = Float.parseFloat(JOptionPane.showInputDialog("Introduzca el precio de entrada"));
				duracion = Integer.parseInt(JOptionPane.showInputDialog("Introduzca la duración de la subasta"));
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Entrada o duración incorrectasa", "Error de formato", JOptionPane.ERROR_MESSAGE);
				}
				String mensaje = JOptionPane.showInputDialog("Introduzca el mensaje de notificación");
				cardManager.getAplic().nuevaSubasta(entrada, duracion, mensaje, selected);
				break;
			case BID:
				Subasta sub = ((ArrayList<Subasta>) cardManager.getAplic().filtSub(
						(Articulo) aucP.getModel().getValueAt(aucP.getTable().getSelectedColumn(), 1), null, null, null,
						0, 0)).get(0);

				tlf = JOptionPane.showInputDialog("Introduzca el teléfono del cliente");
				float puja = Float.parseFloat(JOptionPane.showInputDialog("Introduzca cantidad de la puja"));
				if (cardManager.getAplic().buscarCliente(tlf) == null)
					JOptionPane.showMessageDialog(null, "Error: No existe un usuario con este número", tlf,
							JOptionPane.ERROR_MESSAGE);
				else {
					if (cardManager.getAplic().realizarPuja(sub, cardManager.getAplic().buscarCliente(tlf), puja)) {
						JOptionPane.showMessageDialog(null,
								"Se ha aceptado la puja. Se ha consumido una entrada gratuita de esta cuenta.");
						aucP.revalidate();
						aucP.repaint();
					} else if (sub.getPuja().getCantidad() == puja)
						JOptionPane.showMessageDialog(null, "Se ha aceptado la puja.");
					else
						JOptionPane.showMessageDialog(null, "Puja rechazada.");
				}
				break;
			case CANCEL:
				sub = ((ArrayList<Subasta>) cardManager.getAplic().filtSub(
						(Articulo) aucP.getModel().getValueAt(aucP.getTable().getSelectedColumn(), 1), null, null, null,
						0, 0)).get(0);

				if (cardManager.getAplic().cancelarSubasta(sub))
					JOptionPane.showMessageDialog(null, "Se ha cancelado la subasta exitosamente.");
				else
					JOptionPane.showMessageDialog(null, "Surgió un error durante la cancelación de la subasta.", null,
							JOptionPane.ERROR_MESSAGE);
				break;
			case SUSCRIBE:
				sub = ((ArrayList<Subasta>) cardManager.getAplic().filtSub(
						(Articulo) aucP.getModel().getValueAt(aucP.getTable().getSelectedColumn(), 1), null, null, null,
						0, 0)).get(0);

				tlf = JOptionPane.showInputDialog("Introduzca el teléfono del cliente");
				if (sub.anhSuscripcion(cardManager.getAplic().buscarCliente(tlf)))
					JOptionPane.showMessageDialog(null, "Se ha añadido la subscripción exitosamente.");
				else
					JOptionPane.showMessageDialog(null, "El cliente " + tlf + " ya está suscrito o no existe.", null,
							JOptionPane.ERROR_MESSAGE);
				break;
			case UNSUSCRIBE:
				sub = ((ArrayList<Subasta>) cardManager.getAplic().filtSub(
						(Articulo) aucP.getModel().getValueAt(aucP.getTable().getSelectedColumn(), 1), null, null, null,
						0, 0)).get(0);

				tlf = JOptionPane.showInputDialog("Introduzca el teléfono del cliente");
				if (sub.anhSuscripcion(cardManager.getAplic().buscarCliente(tlf)))
					JOptionPane.showMessageDialog(null, "Se ha eliminado la subscripción exitosamente.");
				else
					JOptionPane.showMessageDialog(null, "El cliente " + tlf + " no estaba suscrito o no existe.", null,
							JOptionPane.ERROR_MESSAGE);
				break;
			case LINKA:
				Articulo art = (Articulo) aucP.getModel().getValueAt(aucP.getTable().getSelectedRow(), 1);
				ArrayList<Articulo> list1 = new ArrayList<>();

				list1.add(art);
				cardManager.goToCard("artPan");
				artP.setModel(list1);
				break;
			case BACK:
				if (cardManager.getAplic().esAdmin())
					cardManager.goToCard("admPan");
				else
					cardManager.goToCard("usrPan");
				cliP.revalidate();
				artP.revalidate();
				aucP.revalidate();
				cliP.revalidate();
				artP.revalidate();
				aucP.revalidate();
				break;
			case RENOVE:
				Cliente slctdCli = cardManager.getAplic()
						.buscarCliente((String) cliP.getModel().getValueAt(cliP.getTable().getSelectedRow(), 0));
				int tipo = JOptionPane.showConfirmDialog(null, "¿Quiere que el" + "contrato sea preferente?",
						"Elección de contrato", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				float total;
				switch (tipo) {
				case JOptionPane.YES_OPTION:
					total = cardManager.getAplic().crearContrato(slctdCli, true);
					JOptionPane.showMessageDialog(null, "Total a pagar: " + total + "€");
					break;
				case JOptionPane.NO_OPTION:
					total = cardManager.getAplic().crearContrato(slctdCli, false);
					JOptionPane.showMessageDialog(null, "Total a pagar: " + total + "€");
					break;
				default:
					JOptionPane.showMessageDialog(null, "Suscripción cancelada");
					break;
				}
				break;
			case MODIFY:
				slctdCli = cardManager.getAplic()
						.buscarCliente((String) cliP.getModel().getValueAt(cliP.getTable().getSelectedRow(), 0));
				cardManager.goToCard("clientePan");
				cliP.revalidate();
				cliP.repaint();
				cardManager.getView().getNuevoClientePanel().getController().startWith(slctdCli);
				break;
			/*
			 * case SUSALL: slctdCli = cardManager.getAplic().buscarCliente
			 * ((String)cliP.getModel().getValueAt(cliP.getTable().
			 * getSelectedRow(), 0)); //cardManager.getAplic().s break; case
			 * UNSUSALL: break;
			 */
			default:
				break;
			}
		} catch (ParseException | InvalidUserException e1) {
			// No debería saltar jamás de los jamases
			e1.printStackTrace();
		}
	}

	/**
	 * @param artP
	 *            the artP to set
	 */
	public void setArtP(ArticlePanel artP) {
		this.artP = artP;
	}

	/**
	 * @param aucP
	 *            the aucP to set
	 */
	public void setAucP(AuctionPanel aucP) {
		this.aucP = aucP;
	}

	/**
	 * @param cliP
	 *            the cliP to set
	 */
	public void setCliP(ClientPanel cliP) {
		this.cliP = cliP;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		JTable table = (JTable) e.getSource();
		setButtons(table);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	/**
	 * Cuando presiona una tecla, se reubican los botones en la tabla (de esta
	 * forma no hará falta hacer click siempre) o se activarán los botones
	 * cuando se presiona Enter.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void keyReleased(KeyEvent e) {
		// Tabla
		if (e.getComponent() instanceof JTable) {
			JTable table = (JTable) e.getComponent();
			setButtons(table);
			// Botones
		} else if (e.getComponent() instanceof ClaviButton<?>) {
			if (e.getExtendedKeyCode() != KeyEvent.VK_ENTER)
				return;
			ClaviButton<FilterButtonNames> button = (ClaviButton<FilterButtonNames>) e.getComponent();
			actionPerformed(new ActionEvent(button, 0, null));
		}
	}

	private void setButtons(JTable table) {
		FilterTableModel model = (FilterTableModel) table.getModel();
		if (model.getColumnName(1).equals("Descripción")) {
			// Cosos de los artículos
			Articulo selection = cardManager.getAplic()
					.buscarArticulo((Long) model.getValueAt(table.getSelectedRow(), 0));
			if (cardManager.getAplic().esAdmin()) {
				switch (selection.getEst()) {
				case EnLote:
					artP.setButtonsDisplay((byte) 8);
					break;
				case EnSubasta:
					artP.setButtonsDisplay((byte) 2);
					break;
				case Subastable:
					artP.setButtonsDisplay((byte) 17);
					break;
				case Ambos:
					artP.setButtonsDisplay((byte) 16);
					break;
				default:
					artP.setButtonsDisplay((byte) 0);
				}
			} else {
				switch (selection.getEst()) {
				case Ambos:
					artP.setButtonsDisplay((byte) 8);
					break;
				case EnLote:
					artP.setButtonsDisplay((byte) 4);
					break;
				case EnSubasta:
					artP.setButtonsDisplay((byte) 2);
					break;
				case Subastable:
					artP.setButtonsDisplay((byte) 1);
					break;
				case Vendible:
					artP.setButtonsDisplay((byte) 8);
					break;
				default:
					artP.setButtonsDisplay((byte) 0);
				}
			}
		} else if (model.getColumnName(1).equals("Artículo subastado")) {
			Subasta selection = ((ArrayList<Subasta>) cardManager.getAplic()
					.filtSub((Articulo) model.getValueAt(table.getSelectedRow(), 1), null, null, null, 0, 0)).get(0);
			byte b = 2;
			if (!selection.isIniciada() && cardManager.getAplic().esAdmin())
				b += 4;
			if (!cardManager.getAplic().esAdmin())
				b += 25;
			aucP.setButtonsDisplay(b);
		} else {
			byte b = 1;
			// TODO añadir los casos de los demás botones de ser preciso
			cliP.setButtonsDisplay(b);
		}
	}
}
