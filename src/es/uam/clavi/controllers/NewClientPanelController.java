package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.ForgeButtonNames;
import es.uam.clavi.view.enums.UserButtonNames;
import es.uam.clavi.view.panel.NewClientPanel;

public class NewClientPanelController implements ActionListener, KeyListener {

	private static NewClientPanelController instance = null;
	private Aplicacion model;
	private NewClientPanel panel;
	private FrameController cardManager;
	private Cliente clienteAMod;

	public static NewClientPanelController getInstance(FrameController cardManager) {
		if (instance == null)
			instance = new NewClientPanelController(cardManager);
		return instance;
	}

	private NewClientPanelController(FrameController cardManager) {
		this.cardManager = cardManager;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Do nothing

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Do nothing
	}

	@SuppressWarnings("unchecked")
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getExtendedKeyCode() != KeyEvent.VK_ENTER)
			return;
		ClaviButton<UserButtonNames> button = (ClaviButton<UserButtonNames>) e.getComponent();
		actionPerformed(new ActionEvent(button, 0, null));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		ClaviButton<ForgeButtonNames> button = (ClaviButton<ForgeButtonNames>) e.getSource();
		if (button.getTag() == ForgeButtonNames.CRAR) {
			if (panel.getNombre().getText() == null || panel.getTelefono().getText() == null
					|| panel.getNombre().getText().isEmpty() || panel.getTelefono().getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "El nombre y el teléfono son campos obligatorios", "Input inválido",
						JOptionPane.ERROR_MESSAGE);
			} else {
				try {
					if (!model.nuevoCliente(panel.getTelefono().getText(), panel.getNombre().getText(),
							panel.getpApellido().getText(), panel.getsApellido().getText(), panel.getNif().getText(),
							panel.getDnia().getText(), panel.getMovil().getText(), panel.geteMail().getText(),
							panel.getDireccion().getText(), panel.getCiudad().getText(), panel.getProvincia().getText(),
							panel.getPais().getText(), panel.getcP().getText(), panel.getEmpresa().getText())) {
						JOptionPane.showMessageDialog(null, "Error al crear el nuevo cliente", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Cliente añadido a la base de datos", "Cliente añadido",
								JOptionPane.INFORMATION_MESSAGE);
						panel.revalidate();
						panel.repaint();
					}
				} catch (InvalidUserException e1) {
					e1.printStackTrace();
				}
				return;
			}
		} else {
			if (model.esAdmin())
				cardManager.goToCard("admPan");
			else
				cardManager.goToCard("usrPan");
			panel.revalidate();
			panel.repaint();
		}
	}

	public void setView(NewClientPanel nuevoClPanel) {
		panel = nuevoClPanel;
	}

	public void setModel(Aplicacion app) {
		model = app;
	}

	public void startWith(Cliente client) {
		panel.getTelefono().setText(client.getTlf());
		panel.getNombre().setText(client.getNombre());
		panel.getpApellido().setText(client.getPrimerApellido());
		panel.getsApellido().setText(client.getSegundoApellido());
		panel.getNif().setText(client.getNif());
		panel.getDnia().setText(client.getDnia());
		panel.getMovil().setText(client.getMovil());
		panel.geteMail().setText(client.geteMail());
		panel.getDireccion().setText(client.getDireccion());
		panel.getCiudad().setText(client.getCiudad());
		panel.getProvincia().setText(client.getProvincia());
		panel.getPais().setText(client.getPais());
		panel.getcP().setText(client.getCp());
		panel.getEmpresa().setText(client.getEmpresa());
		this.clienteAMod = client;
	}

	public void modifCliente() throws InvalidUserException {
		int cambios = this.model.modCliente(this.clienteAMod, panel.getTelefono().getText(),
				panel.getNombre().getText(), panel.getpApellido().getText(), panel.getsApellido().getText(),
				panel.getNif().getText(), panel.getDnia().getText(), panel.getMovil().getText(),
				panel.geteMail().getText(), panel.getDireccion().getText(), panel.getCiudad().getText(),
				panel.getProvincia().getText(), panel.getPais().getText(), panel.getcP().getText(),
				panel.getEmpresa().getText());
		if (this.clienteAMod.getContrato() != null) {
			if (!this.panel.getNotificar().isSelected()) {
				this.clienteAMod.getContrato().setNotificar(false);
			} else {
				this.clienteAMod.getContrato().setNotificar(true);
			}
		}
		JOptionPane.showMessageDialog(null, "Se han realizado " + cambios);
	}
}
