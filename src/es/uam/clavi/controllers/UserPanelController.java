package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.UserButtonNames;

public class UserPanelController implements ActionListener {

	private static UserPanelController instance = null;
	private FrameController cardManager;

	private UserPanelController(FrameController controller) {
		this.cardManager = controller;
	}

	public static UserPanelController getInstance(FrameController controller) {
		if (instance == null) {
			return new UserPanelController(controller);
		} else {
			return instance;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		
		ClaviButton<UserButtonNames> button = (ClaviButton<UserButtonNames>)e.getSource();
		if (button.getTag() instanceof UserButtonNames) {
			switch ((UserButtonNames)button.getTag()) {
				case ARTS:
					cardManager.goToCard("artPan");
					break;
				case CESE:
					cardManager.goToCard("logPan");
					break;
				case NVCL:
					cardManager.goToCard("clientePan");
					break;
				case NVCT:
					cardManager.goToCard("cliPan");
					break;
				case SUBS:
					cardManager.goToCard("aucPan");
					break;
				default:
					break;
			}
		}	
	}
}
