package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.ForgeButtonNames;
import es.uam.clavi.view.panel.ContractPropScreen;

public class ContractPropController implements ActionListener, KeyListener {

	private static ContractPropController instance = null;
	private FrameController cardManager;
	private ContractPropScreen panel;
	private Aplicacion aplic;

	public static ContractPropController getInstance(FrameController cardManager) {
		if (instance == null) {
			instance = new ContractPropController(cardManager);
		}
		return instance;
	}

	private ContractPropController(FrameController cardManager) {
		super();
		this.cardManager = cardManager;
		this.aplic = cardManager.getAplic();
	}

	public void setView(ContractPropScreen contractPanel) {
		panel = contractPanel;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Do nothing

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Do nothing

	}

	@SuppressWarnings("unchecked")
	@Override
	public void keyReleased(KeyEvent e) {
		ClaviButton<ForgeButtonNames> button = (ClaviButton<ForgeButtonNames>) e.getComponent();
		actionPerformed(new ActionEvent(button, 0, null));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		ClaviButton<ForgeButtonNames> button = (ClaviButton<ForgeButtonNames>) e.getSource();
		switch ((ForgeButtonNames) button.getTag()) {
		case SAVE:
			Float gastoParaDescuento = Float.valueOf(0);
			Float precioNorm = Float.valueOf(0);
			Float precioPrem = Float.valueOf(0);
			Integer subsGratNorm;
			Integer subsGratPrem;
			Integer descEnvPrem = 0;
			try {
				if (!this.panel.getGastEnvio().getText().isEmpty())
					descEnvPrem = Integer.valueOf(this.panel.getGastEnvio().getText());
				if (!this.panel.getAplicPasados().getText().isEmpty())
					gastoParaDescuento = Float.valueOf(this.panel.getAplicPasados().getText());
				if (!this.panel.getPrecioNorm().getText().isEmpty())
					precioNorm = Float.valueOf(this.panel.getPrecioNorm().getText());
				if (!this.panel.getPrecioPrem().getText().isEmpty())
					precioPrem = Float.valueOf(this.panel.getPrecioPrem().getText());

				subsGratNorm = Integer.valueOf(this.panel.getnSubasNorm().getSelectedIndex()) - 1;
				subsGratPrem = Integer.valueOf(this.panel.getnSubasPrem().getSelectedIndex()) - 1;

				this.aplic.modContrNorm(gastoParaDescuento, 0, subsGratNorm, precioNorm);
				this.aplic.modContrPrem(0, descEnvPrem, subsGratPrem, precioPrem);
			} catch (InvalidUserException e1) {
				e1.printStackTrace();
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(null, "Error en los argumentos de entrada", "Input inválido",
						JOptionPane.ERROR_MESSAGE);
			}
			JOptionPane.showMessageDialog(null, "Cambios aceptados correctamente");
			panel.revalidate();
			panel.repaint();
			break;
		default:
			this.resetPanel();
			if (cardManager.getAplic().esAdmin())
				cardManager.goToCard("admPan");
			else
				cardManager.goToCard("usrPan");
		}
	}

	public void resetPanel() {

		this.panel.getAplicPasados().setText(this.aplic.getGastMenudNorm() + "");
		this.panel.getGastEnvio().setText(this.aplic.getDescEnvPrem() + "");
		this.panel.getnSubasNorm().setSelectedIndex(this.aplic.getInscrGratNorm() + 1);
		this.panel.getPrecioPrem().setText(this.aplic.getPrecioPrem() + "");
		this.panel.getPrecioNorm().setText(this.aplic.getPrecioNorm() + "");
		this.panel.getnSubasPrem().setSelectedIndex(0);
		this.panel.getnSubasPrem().setEditable(false);
	}

}
