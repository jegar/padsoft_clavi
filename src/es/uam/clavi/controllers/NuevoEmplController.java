package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.panel.EmplAttPanel;

public class NuevoEmplController implements ActionListener {

	private EmplAttPanel panel;
	private Aplicacion aplic;
	private FrameController cardManager;

	public NuevoEmplController(FrameController cardManager) {
		this.cardManager = cardManager;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.panel.getBotAtt()) {
			if (aplic.esAdmin()) {
				this.panel.getUserName().setText("");
				this.panel.getPassword().setText("");
				this.panel.getNomField().setText("");
				this.panel.getApellidoField().setText("");
				this.panel.getApellidoField().setText("");
				this.panel.getTelefono().setText("");
				this.panel.getMovil().setText("");
				this.cardManager.goToCard("admPan");
			} else {
				JOptionPane.showMessageDialog(null, "Usuario erróneo");
				this.cardManager.goToCard("logPan");
				return;
			}
		} else if (e.getSource() == this.panel.getBotEnv()) {
			try {
				if (this.panel.getUserName().getText().isEmpty() || this.panel.getPassword().getText().isEmpty()) {
					JOptionPane.showMessageDialog(null,
							"El nuevo empleado debe tener por lo menos usuario y contraseña");
					return;
				}
				this.aplic.nuevoEmpleado(this.panel.getUserName().getText(), this.panel.getPassword().getText(),
						this.panel.getNomField().getText(), this.panel.getApellidoField().getText(),
						this.panel.getApellidoField().getText(), this.panel.getTelefono().getText(),
						this.panel.getMovil().getText());
				JOptionPane.showMessageDialog(null,
						"Nuevo empleado creado con username" + this.panel.getUserName().getText());
			} catch (InvalidUserException e1) {
				JOptionPane.showMessageDialog(null, "Usuario erróneo");
				this.cardManager.goToCard("logPan");
				return;
			}

		}
	}

	public void setModel(Aplicacion aplic) {
		this.aplic = aplic;
	}

	public void setView(EmplAttPanel panel) {
		this.panel = panel;
	}

}
