package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FilterTableModel;
import es.uam.clavi.view.enums.LoteButtonNames;
import es.uam.clavi.view.panel.NuevoLotePanel;

public class LoteController implements MouseListener, ActionListener {

	private Aplicacion aplic;
	private NuevoLotePanel panel;
	private FilterTableModel artTMod;
	private FilterTableModel lotMod;
	private List<Articulo> listaFiltro;
	private FrameController cardManager;

	public LoteController(FrameController cardManager) {
		this.cardManager = cardManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		ClaviButton<LoteButtonNames> selector;
		if (e.getSource() instanceof ClaviButton<?>) {
			if (((ClaviButton<?>) e.getSource()).getTag() instanceof LoteButtonNames) {
				selector = (ClaviButton<LoteButtonNames>) e.getSource();
				switch ((LoteButtonNames) selector.getTag()) {
				case CRLO:
					ArrayList<Articulo> nuevoLote = new ArrayList<Articulo>();
					if (this.panel.getNuevaDescripcion().getText().length() < 5) {
						JOptionPane.showMessageDialog(null, "Por favor introduce una descripcion para el lote");
						return;
					}
					int i = 0;
					if (this.panel.getLoteTable().getRowCount() > 0) {
						float precioAdquisicion = 0;
						float precioObjetivo = 0;
						while (i < this.panel.getLoteTable().getRowCount()) {
							Articulo articuloAnyadido = this.aplic
									.buscarArticulo(Long.valueOf((String) this.panel.getLoteTable().getValueAt(i, 0)));
							precioAdquisicion += articuloAnyadido.getPrecioAdquisicion();
							precioObjetivo += articuloAnyadido.getPrecioObjetivo();
							nuevoLote.add(articuloAnyadido);
							i++;
						}
						Date date = new Date();
						try {
							this.cardManager.getAplic().crearLote(this.panel.getNuevaDescripcion().getText(), date,
									precioAdquisicion, precioObjetivo, "N/A", nuevoLote);
						} catch (InvalidUserException e1) {
							JOptionPane.showMessageDialog(null, "Usuario no valido detectado");
							this.cardManager.goToCard("logPan");
							return;
						}
						JOptionPane.showMessageDialog(null, "Nuevo lote creado");
						this.panel.getNuevaDescripcion().setText("");
						i = 0;
						while (this.lotMod.getRowCount() != 0) {
							this.lotMod.removeRow(0);
						}
						this.panel.getLoteTable().revalidate();
					} else {
						JOptionPane.showMessageDialog(null, "El lote debe contener por lo menos un elemento");
					}
					break;
				case VLVR:
					this.listaFiltro = (List<Articulo>) this.aplic.filtArt(false, null, Estado.Ambos, null, 0, 0, null);
					this.listaFiltro.addAll(
							(List<Articulo>) this.aplic.filtArt(false, null, Estado.Vendible, null, 0, 0, null));
					this.listaFiltro.addAll(
							(List<Articulo>) this.aplic.filtArt(false, null, Estado.Subastable, null, 0, 0, null));
					this.panel.getDescripcion().setText("");
					while (this.artTMod.getRowCount() != 0) {
						this.artTMod.removeRow(0);
					}
					SimpleDateFormat tempDate = new SimpleDateFormat("dd/MM/yyyy");
					for (Articulo art : this.listaFiltro) {
						Object[] rowData = { art.getID(), art.getTipo(), tempDate.format(art.getFechaAdquisicion()),
								art.getFechaDatacion() };
						artTMod.addRow(rowData);
					}
					this.panel.getLoteTable().revalidate();
					this.cardManager.goToCard("admPan");
					panel.revalidate();
					panel.repaint();
					break;
				}
			}
		} else {
			if (e.getSource() == this.panel.getBotonFiltrado()) {
				String tipo = (String) this.panel.getFiltTipo().getSelectedItem();
				boolean certif = this.panel.getFiltCert().isSelected();
				String temp = this.panel.getFiltFechaMin().getText();
				String expectedPattern = "MM/dd/yyyy";
				Date dateMin = null;
				SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
				try {
					if (!temp.isEmpty()) {
						dateMin = formatter.parse(temp);
					}
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "El formato de la fecha es MM/dd/yyyy");
				}
				temp = this.panel.getFiltPrecMax().getText();
				float precioMax = 0;
				float precioMin = 0;
				if (!temp.isEmpty()) {
					precioMax = Float.valueOf(temp);
				}
				temp = this.panel.getFiltPrecMin().getText();
				if (!temp.isEmpty()) {
					precioMin = Float.valueOf(temp);
				}
				String datacion = this.panel.getDatacion().getText();
				this.listaFiltro = new ArrayList<>();

				this.listaFiltro.addAll(
						this.aplic.filtArt(certif, datacion, Estado.Ambos, dateMin, precioMax, precioMin, tipo));
				this.listaFiltro.addAll(
						this.aplic.filtArt(certif, datacion, Estado.Vendible, dateMin, precioMax, precioMin, tipo));
				this.listaFiltro.addAll(
						this.aplic.filtArt(certif, datacion, Estado.Subastable, dateMin, precioMax, precioMin, tipo));

				while (this.artTMod.getRowCount() != 0) {
					this.artTMod.removeRow(0);
				}
				this.panel.getLoteTable().revalidate();
				SimpleDateFormat tempDate = new SimpleDateFormat("dd/MM/yyyy");
				for (Articulo art : this.listaFiltro) {
					int flag = 0;
					int i = 0;
					while (i < lotMod.getRowCount()) {
						if (((String) lotMod.getValueAt(i, 0)).compareToIgnoreCase(art.getID() + "") == 0) {
							flag = 1;
							break;
						}
						i++;
					}
					if (flag == 0) {
						Object[] rowData = { art.getID(), art.getTipo(), tempDate.format(art.getFechaAdquisicion()),
								art.getFechaDatacion() };
						artTMod.addRow(rowData);
					}
				}
				this.panel.getLoteTable().revalidate();
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		String id;
		JTable tablaOrigen = this.panel.getArticleTable();
		FilterTableModel orgTMod = this.artTMod;
		FilterTableModel destMod = this.lotMod;
		int index = tablaOrigen.getSelectedRow();
		if (index == -1) {
			tablaOrigen = this.panel.getLoteTable();
			index = tablaOrigen.getSelectedRow();
			orgTMod = this.lotMod;
			destMod = this.artTMod;
		}

		Articulo currentArt = listaFiltro.get(index);
		this.panel.getIdArticulo().setText(currentArt.getID() + "");
		this.panel.getTipoArticulo().setText(currentArt.getTipo() + "");
		this.panel.getPrecio().setText(currentArt.getPrecioObjetivo() + "");
		this.panel.getFecha().setText(new SimpleDateFormat("dd/MM/yyyy").format(currentArt.getFechaAdquisicion()) + "");
		this.panel.getAnyoOrigen().setText(currentArt.getFechaDatacion() + "");
		this.panel.getDescripcion().setText(currentArt.getDescripcion() + "");

		if (e.getClickCount() == 2) {
			if (orgTMod.getValueAt(index, 1) instanceof String) {
				id = orgTMod.getValueAt(index, 0) + "";
				Object[] rowData = { id, orgTMod.getValueAt(index, 1), orgTMod.getValueAt(index, 2),
						orgTMod.getValueAt(index, 3) };
				destMod.addRow(rowData);
				orgTMod.removeRow(index);
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// Do nothing
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Do nothing
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Do nothing
	}

	public void setView(NuevoLotePanel view) {
		this.panel = view;
		artTMod = new FilterTableModel();
		view.getArticleTable().setModel(artTMod);
		if (this.aplic != null) {
			this.listaFiltro = (List<Articulo>) this.aplic.filtArt(false, null, Estado.Ambos, null, 0, 0, null);
			this.listaFiltro
					.addAll((List<Articulo>) this.aplic.filtArt(false, null, Estado.Vendible, null, 0, 0, null));
			this.listaFiltro
					.addAll((List<Articulo>) this.aplic.filtArt(false, null, Estado.Subastable, null, 0, 0, null));
			SimpleDateFormat tempDate = new SimpleDateFormat("dd/MM/yyyy");
			for (Articulo art : this.listaFiltro) {
				Object[] rowData = { art.getID(), art.getTipo(), tempDate.format(art.getFechaAdquisicion()),
						art.getFechaDatacion() };
				artTMod.addRow(rowData);
			}
		}
		lotMod = new FilterTableModel();
		view.getLoteTable().setModel(lotMod);
	}

	public void setModel(Aplicacion aplication) {
		this.aplic = aplication;
	}

}
