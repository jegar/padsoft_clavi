package es.uam.clavi.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.ForgeButtonNames;
import es.uam.clavi.view.panel.NewArticlePanel;

public class NewArticleController implements ActionListener, KeyListener {

	private static NewArticleController instance = null;
	private Aplicacion model;
	private NewArticlePanel panel;
	private FrameController cardManager;
	
	public static NewArticleController getInstance (FrameController cardManager) {
		if (instance == null) instance = new NewArticleController(cardManager);
		return instance;
	}
	
	private NewArticleController(FrameController cardManager) {
		this.cardManager = cardManager;
	}
	
	public void setView(NewArticlePanel nuevoArtPanel) {
		panel = nuevoArtPanel;
	}
	
	public void setModel (Aplicacion app) {
		model = app;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox<?>) {
			JComboBox<String> tipos = (JComboBox<String>)e.getSource();
			switch ((String)tipos.getSelectedItem()) {
			case "Arte":
				panel.getTipoObraLabel().setVisible(true);
				panel.getTipoObra().setVisible(true);
				panel.getAutorLabel().setVisible(true);
				panel.getAutor().setVisible(true);
				panel.getCert().setVisible(true);
				panel.getAnchoLabel().setVisible(false);
				panel.getAncho().setVisible(false);
				panel.getFondoLabel().setVisible(false);
				panel.getFondo().setVisible(false);
				panel.getAltoLabel().setVisible(false);
				panel.getAlto().setVisible(false);
				panel.getPesoLabel().setVisible(false);
				panel.getPeso().setVisible(false);
				panel.getDescuentoLabel().setVisible(false);
				panel.getDescuento().setVisible(false);
				break;
			case "Menudencia":
				panel.getTipoObraLabel().setVisible(false);
				panel.getTipoObra().setVisible(false);
				panel.getAutorLabel().setVisible(false);
				panel.getAutor().setVisible(false);
				panel.getCert().setVisible(false);
				panel.getAnchoLabel().setVisible(false);
				panel.getAncho().setVisible(false);
				panel.getFondoLabel().setVisible(false);
				panel.getFondo().setVisible(false);
				panel.getAltoLabel().setVisible(false);
				panel.getAlto().setVisible(false);
				panel.getPesoLabel().setVisible(false);
				panel.getPeso().setVisible(false);
				panel.getDescuentoLabel().setVisible(true);
				panel.getDescuento().setVisible(true);
				break;
			case "Voluminoso":
				panel.getTipoObraLabel().setVisible(false);
				panel.getTipoObra().setVisible(false);
				panel.getAutorLabel().setVisible(false);
				panel.getAutor().setVisible(false);
				panel.getCert().setVisible(false);
				panel.getAnchoLabel().setVisible(true);
				panel.getAncho().setVisible(true);
				panel.getFondoLabel().setVisible(true);
				panel.getFondo().setVisible(true);
				panel.getAltoLabel().setVisible(true);
				panel.getAlto().setVisible(true);
				panel.getPesoLabel().setVisible(true);
				panel.getPeso().setVisible(true);
				panel.getDescuentoLabel().setVisible(false);
				panel.getDescuento().setVisible(false);
				break;
			default:
				return;	
			}
			panel.revalidate();
			panel.repaint();
		} else {
			ClaviButton<ForgeButtonNames> button = (ClaviButton<ForgeButtonNames>) e.getSource();
			if (button.getTag() == ForgeButtonNames.CRAR) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date fAdq;
				try {
					Integer desc, ancho, fondo, alto, peso;
					Float preciom, precioM;
					if (panel.getDescuento().getText().isEmpty()) desc = 0;
					else desc = Integer.parseInt(panel.getDescuento().getText());
					if (panel.getAncho().getText().isEmpty()) ancho = 0;
					else ancho = Integer.parseInt(panel.getAncho().getText());
					if (panel.getFondo().getText().isEmpty()) fondo = 0;
					else fondo = Integer.parseInt(panel.getFondo().getText());
					if (panel.getAlto().getText().isEmpty()) alto = 0;
					else alto = Integer.parseInt(panel.getAlto().getText());
					if (panel.getPeso().getText().isEmpty()) peso = 0;
					else peso = Integer.parseInt(panel.getPeso().getText());
					if (panel.getPrecioAdq().getText().isEmpty()) preciom = (float) 0;
					else preciom = Float.parseFloat(panel.getPrecioAdq().getText());
					if (panel.getPrecioObj().getText().isEmpty()) precioM = (float) 0;
					else precioM = Float.parseFloat(panel.getPrecioObj().getText());
					
					fAdq = sdf.parse(panel.getDd().getText()+"/"+panel.getMm().getText()+"/"+panel.getAa().getText());
					if (model.nuevoArticulo((String)panel.getTipoArt().getSelectedItem(), fAdq, panel.getfDat().getText(),
							panel.getDescripcion().getText(), preciom, precioM, panel.getCert().isSelected(),
							desc, (TipoObra)panel.getTipoObra().getSelectedItem(), ancho, fondo, alto, peso,
							panel.getAutor().getText())) JOptionPane.showMessageDialog(null, "El artículo se creó con éxito");
					else JOptionPane.showMessageDialog(null, "Argumentos de entrada no válidos", "Input inválido", JOptionPane.ERROR_MESSAGE);
				} catch (NumberFormatException e1 ) {
					JOptionPane.showMessageDialog(null, "Dimensiones/Descuento malformado", "Input inválido", JOptionPane.ERROR_MESSAGE);
				} catch (MalformedArtException e1) {
					JOptionPane.showMessageDialog(null, "Argumentos de entrada no válidos", "Input inválido", JOptionPane.ERROR_MESSAGE);
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Precios de adquisición y objetivo inválidos", "Input inválido", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				if (cardManager.getAplic().esAdmin()) cardManager.goToCard("admPan");
				else cardManager.goToCard("usrPan");
				panel.revalidate();
				panel.repaint();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getExtendedKeyCode() != KeyEvent.VK_ENTER) return;
		ClaviButton<ForgeButtonNames> button =
				(ClaviButton<ForgeButtonNames>) e.getComponent();
		actionPerformed(new ActionEvent(button, 0, null));
	}

}
