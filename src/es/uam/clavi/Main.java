package es.uam.clavi;

import es.uam.clavi.controllers.AdminPanelController;
import es.uam.clavi.controllers.ContractPropController;
import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.controllers.FrameController;
import es.uam.clavi.controllers.LoginPanelController;
import es.uam.clavi.controllers.LoteController;
import es.uam.clavi.controllers.NewArticleController;
import es.uam.clavi.controllers.NewClientPanelController;
import es.uam.clavi.controllers.NuevoEmplController;
import es.uam.clavi.controllers.UserPanelController;
import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.general.Aplicacion;
import es.uam.clavi.model.general.InvalidUserException;
import es.uam.clavi.model.gpers.GpersLoadException;
import es.uam.clavi.view.frames.Frame;

public class Main {

	public static void main(String[] args) {
		try {
			/* Creamos los elementos */
			Aplicacion aplication = new Aplicacion();
			Frame mainframe = new Frame();
			FrameController cardManager = FrameController.getInstance();

			/* Enlazamos a FrameController */
			cardManager.setView(mainframe);
			cardManager.setModel(aplication);
			mainframe.setController(cardManager);

			/* Enlazamos AdminPanelController */
			AdminPanelController admPanCont = AdminPanelController.getInstance(cardManager);
			admPanCont.setView(mainframe.getAdminPanel());
			mainframe.getAdminPanel().setController(admPanCont);

			/* Enlazamos FilterPanelController */
			FilterPanelController filtPanCont = FilterPanelController.getInstance(cardManager);
			filtPanCont.setArtP(mainframe.getArtPanel());
			mainframe.getArtPanel().setController(filtPanCont);
			filtPanCont.setAucP(mainframe.getAuctionPanel());
			mainframe.getAuctionPanel().setController(filtPanCont);
			filtPanCont.setCliP(mainframe.getClientPanel());
			mainframe.getClientPanel().setController(filtPanCont);

			/* Enlazamos LoginPanelController */
			LoginPanelController logPanCont = LoginPanelController.getInstance(cardManager);
			logPanCont.setView(mainframe.getLoginPanel());
			logPanCont.setModel(aplication);
			mainframe.getLoginPanel().setController(logPanCont);

			/* Enlazamos UserPanelController */
			UserPanelController usrPanCont = UserPanelController.getInstance(cardManager);
			mainframe.getUserPanel().setController(
					usrPanCont); /*
									 * Este controlador no necesita referencia
									 * ni al modelo ni a la vista
									 */

			/* Enlazamos LoteController */
			LoteController lotCont = new LoteController(cardManager);
			lotCont.setModel(aplication);
			lotCont.setView(mainframe.getNuevoLotePanel());
			mainframe.getNuevoLotePanel().setController(lotCont,
					lotCont); /* Implementa ambos */

			/* Enlazamos NewClController */
			NewClientPanelController newClCont = NewClientPanelController.getInstance(cardManager);
			newClCont.setView(mainframe.getNuevoClientePanel());
			newClCont.setModel(aplication);
			mainframe.getNuevoClientePanel().setController(newClCont);

			/* Enlazamos NewArtController */
			NewArticleController newArtCont = NewArticleController.getInstance(cardManager);
			newArtCont.setView(mainframe.getNuevoArtPanel());
			newArtCont.setModel(aplication);
			mainframe.getNuevoArtPanel().setController(newArtCont);

			/* Enlazamos PropContController */
			ContractPropController propContController = ContractPropController.getInstance(cardManager);
			propContController.setView(mainframe.getContrPropPanel());
			mainframe.getContrPropPanel().setController(propContController);
			propContController.resetPanel();

			/* Enlazamos NuevoEmplController */
			NuevoEmplController nuevEmplContr = new NuevoEmplController(cardManager);
			nuevEmplContr.setView(mainframe.getEmplAttPanel());
			nuevEmplContr.setModel(aplication);
			mainframe.getEmplAttPanel().setController(nuevEmplContr);

			try {
				aplication.validarLogin("admin", "1234");
				aplication.nuevoEmpleado("nombre", "1111", "", "", "", "", "");
			} catch (InvalidUserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// mainframe.getLoginPanel().setController(userPanCont);

			mainframe.pack();
			mainframe.setVisible(true);
		} catch (GclienLoadException | GpersLoadException | GartLoadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
}
