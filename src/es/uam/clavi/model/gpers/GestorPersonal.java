package es.uam.clavi.model.gpers;

import java.io.*;
import java.util.HashMap;

import es.uam.clavi.model.general.InvalidUserException;

public class GestorPersonal{

	private HashMap<String,Personal> personal;
	private String path_to_file;
	
	/**
	 * @param path
	 * @throws GpersLoadException
	 */
	public GestorPersonal(String path) throws GpersLoadException{
		this.path_to_file = path + File.separator+"gpers";
		if (!this.abrirGestor()){
			personal = new HashMap<String,Personal>();
			Gerente gerente = new Gerente("admin","1234");
			personal.put(gerente.getUsername(),gerente);
		}
	}

	/**
	 * 
	 * @param username
	 * @return
	 */
	public Personal get(String username){
			return personal.get(username);
	}
	/**
	 * 
	 * @param username
	 * @param personal
	 */
	public void put(String username, Personal personal){
		this.personal.put(username,personal);
	}
	/** Crea un nuevo empleado y lo incluye en los usuarios
	 * @param username
	 * @param password
	 */
	public void nuevoEmpleado(String usuario, String password, String nombre, String apellido1, String apellido2,
			String tlf, String movil){
			Empleado nuevo = new Empleado(usuario, password, nombre, apellido1, apellido2, tlf, movil);
			personal.put(usuario, nuevo);
	}
	
	/**
	 * Hace las operaciones previas a abrir el gestor de articulos
	 * @param path_to_file
	 * @throws RuntimeException
	 */
	public void cerrarGestor()throws GpersSaveException{
		try{
			File file = new File(this.path_to_file);
			if (!file.isFile()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			FileOutputStream output = new FileOutputStream (this.path_to_file);
			ObjectOutputStream out = new ObjectOutputStream (output);
			guardarPersonal(out);
			out.close();
			output.close();
		} catch (IOException e){
			e.printStackTrace();
			throw new GpersSaveException();
		}
	}

	/**
	 * Guarda el repositorio de Personal
	 * @param path_to_file
	 * @throws IOException
	 */
	private void guardarPersonal(ObjectOutputStream out) throws IOException {
			out.writeObject(this.personal);
	}
	
	/**
	 * Carga a "personal"
	 * @param in
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void cargarPersonal(ObjectInputStream in) 
			throws ClassNotFoundException, IOException, GpersLoadException{
			
		Object object = in.readObject();
		if (object instanceof HashMap<?,?>){
			HashMap<?,?> hsmp = (HashMap<?, ?>) object;
			if ((hsmp.keySet().iterator().next() instanceof String) &&
				(hsmp.values().iterator().next() instanceof Personal)){		
				this.personal = (HashMap<String, Personal>) hsmp;
				return;
			}
		}
		throw new GpersLoadException();
	}
	
	/**
	 * Carga los datos del gestor de personal
	 * @param path
	 * @return 
	 * @throws LoadingError
	 */
	public boolean abrirGestor() throws GpersLoadException{
		if (new File(this.path_to_file).isFile()){
			try{
				FileInputStream input = new FileInputStream (this.path_to_file);
				ObjectInputStream in = new ObjectInputStream(input);
				cargarPersonal(in);
				in.close();
				input.close();
			} catch (IOException | ClassNotFoundException e){
				e.printStackTrace();
				throw new GpersLoadException();
			}
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Modifica un empleado existente
	 * @param empleado
	 * @param usuario
	 * @param password
	 * @param nombre
	 * @param apellido1
	 * @param apellido2
	 * @param tlf
	 * @param movil
	 * @throws InvalidUserException
	 */
	public void modEmpleado(Empleado empleado, String usuario, String password, String nombre, String apellido1,
			String apellido2,String tlf, String movil){
			boolean flag = false;
			if (empleado.getUsername().compareTo(usuario)!=0){
				/*Esto se hace para que el HashMap funcione correctamente*/
				this.personal.remove(empleado.getUsername());
				flag = true;
			}
			empleado.setUsername(usuario);
			empleado.setApellido1(apellido1);
			empleado.setApellido2(apellido2);
			empleado.setPassword(password);
			empleado.setMovil(movil);
			empleado.setTlf(tlf);
			if (flag){
				this.put(empleado.getUsername(), empleado);
			}
		}
}
