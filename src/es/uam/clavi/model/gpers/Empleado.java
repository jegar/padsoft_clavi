package es.uam.clavi.model.gpers;

public class Empleado extends Personal {

	private static final long serialVersionUID = 4965512387052660423L;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String tlf;
	private String movil;

	public Empleado(String usuario, String password, String nombre, String apellido1, String apellido2,
			String tlf, String movil){
		super(usuario,password);
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.tlf = tlf;
		this.movil = movil;
	}
	/**
	 * @return El nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre El nombre para colocar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return El apellido1
	 */
	public String getApellido1() {
		return apellido1;
	}
	/**
	 * @param apellido1 El apellido1 para colocar
	 */
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	/**
	 * @return El apellido2
	 */
	public String getApellido2() {
		return apellido2;
	}
	/**
	 * @param apellido2 El apellido2 para colocar
	 */
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	/**
	 * @return El tlf
	 */
	public String getTlf() {
		return tlf;
	}
	/**
	 * @param tlf El tlf para colocar
	 */
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	/**
	 * @return El movil
	 */
	public String getMovil() {
		return movil;
	}
	/**
	 * @param movil El movil para colocar
	 */
	public void setMovil(String movil) {
		this.movil = movil;
	}
}
