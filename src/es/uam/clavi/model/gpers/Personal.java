package es.uam.clavi.model.gpers;

import java.io.Serializable;
/**
 * Clase abstracta de todo el personal
 * 
 * @author jesu
 *
 */
public abstract class Personal implements Serializable{
	
	private static final long serialVersionUID = -5291320419943335298L;
	private String username;
	private String password;
	/**
	 * Constructor de Personal
	 * @param username Nombre de usuario
	 * @param password Contrasenya de usuario
	 */
	public Personal(String username, String password){
		this.username = username;
		this.password = password;
	}
	/**
	 * 
	 * @return El nombre de usuario
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 
	 * @param username El nombre de usuario para colocar
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @param password La contrasenha para colocar
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Devuelve si la contrasenha introducida es la correcta o no
	 * @param password
	 * @return true si es correcto, false si no
	 */
	public boolean validar(String password){
		return this.password.equals(password);
	}
}
