package es.uam.clavi.model.general;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import es.uam.clavi.model.gart.GartLoadException;
import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.Lote;
import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.ContractNotFoundException;
import es.uam.clavi.model.gclien.GclienException;
import es.uam.clavi.model.gclien.GclienLoadException;
import es.uam.clavi.model.gclien.GclienSaveException;
import es.uam.clavi.model.gclien.GestorClientes;
import es.uam.clavi.model.gpers.Empleado;
import es.uam.clavi.model.gpers.Gerente;
import es.uam.clavi.model.gpers.GestorPersonal;
import es.uam.clavi.model.gpers.GpersLoadException;
import es.uam.clavi.model.gpers.GpersSaveException;
import es.uam.clavi.model.gpers.Personal;
import es.uam.eps.padsof.emailconnection.FailedInternetConnectionException;
import es.uam.eps.padsof.emailconnection.InvalidEmailAddressException;

/**
 * Gestiona los usuarios de la aplicacion, los login y otras funciones
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public final class Aplicacion {

	private Personal usuarioActual;
	private String path_to_file = "save";
	private GestorPersonal gestorUsuarios;
	private GestorArticulos gestorArticulos;
	private GestorClientes gestorClientes;

	/* CONSTRUCTOR Y DE APLICACION */

	/**
	 * Constructor, asigna los gestores y pone el usuario actual a null.
	 * 
	 * @throws GclienLoadException
	 *             Salta si ha habido un error de carga del Gestor de Clientes
	 * @throws GpersLoadException
	 *             Salta si ha habido un error de carga del Gestor de Personal
	 * @throws GartLoadException
	 *             Salta si ha habido un error de carga del Gestor de Articulo
	 */
	public Aplicacion() throws GclienLoadException, GpersLoadException, GartLoadException {
		gestorArticulos = new GestorArticulos(path_to_file);
		gestorClientes = new GestorClientes(path_to_file);
		gestorUsuarios = new GestorPersonal(path_to_file);
		usuarioActual = null;
	}

	/**
	 * Comprueba si el usuario actual es administrador
	 * 
	 * @return true si es administrador, false si no
	 */
	public boolean esAdmin() {
		return (usuarioActual instanceof Gerente);
	}

	public int getDescEnvPrem() {
		return this.gestorClientes.getDescEnvPrem();
	}

	public float getPrecioPrem() {
		return this.gestorClientes.getPrecioPrem();
	}

	public int getInscrGratNorm() {
		return this.gestorClientes.getSubGratisNormal();
	}

	public float getPrecioNorm() {
		return this.gestorClientes.getPrecioNormal();
	}

	public float getGastMenudNorm() {
		return this.gestorClientes.getGastoMenudNormal();
	}

	/**
	 * Comprueba si la combinación usuario / contrasenya es correcta y la asigna
	 * a el usuario acutual si lo es.
	 * 
	 * @param username
	 *            El nombre de usuario
	 * @param password
	 *            La constrasenya a
	 * @return true si se ha hecho cambio, false si no.
	 */
	public boolean validarLogin(String username, String password) {
		Personal temPers;
		if (username == null)
			this.usuarioActual = null;
		temPers = gestorUsuarios.get(username);
		if ((temPers != null) && (temPers.validar(password))) {
			this.usuarioActual = temPers;
			return true;
		}
		return false;
	}

	/* OPERACIONES DEL GERENTE */
	/**
	 * Cierra la aplicacion
	 * 
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public void cerrarAplicacion() throws InvalidUserException {
		if (esAdmin()) {
			try {
				this.gestorArticulos.finDia(path_to_file);
				this.gestorClientes.cerrarGestor();
				this.gestorUsuarios.cerrarGestor();
			} catch (GclienSaveException | GpersSaveException | IOException e) {
				e.printStackTrace();
			}
		} else
			throw new InvalidUserException();
	}

	/**
	 * Modifica contrato normal
	 * 
	 * @param gasto
	 *            Nuevo gasto para recibir el descuento de menudencias
	 * @param desc
	 *            Nuevo % de descuento de envio de voluminosos
	 * @param subs
	 *            Nueva cantidad de subastas gratuitas
	 * @param prec
	 *            Nuevo precio del tipo de contrato
	 * @throws InvalidUserException
	 *             Salta si el usuario actual no tiene permiso para hacer esa
	 *             operacion
	 */
	public void modContrNorm(float gasto, int desc, int subs, float prec) throws InvalidUserException {
		if (esAdmin()) {
			this.gestorClientes.modContrNorm(gasto, desc, subs, prec);
		} else
			throw new InvalidUserException();
	}

	/**
	 * Modifica un cliente
	 * 
	 * @param cliente
	 *            A modificar
	 * @param tlf
	 *            Telefono del cliente OBLIGATORIO.
	 * @param nombre
	 *            Nombre del cliente OBLIGATORIO.
	 * @param primerApellido
	 *            Primer apellido del cliente.
	 * @param segundoApellido
	 *            Segundo apellido del cliente.
	 * @param nif
	 *            NIF del cliente.
	 * @param dnia
	 *            DNI o NIA del cliente.
	 * @param movil
	 *            Telefono movil del cliente.
	 * @param eMail
	 *            Correo del cliente NECESARIO PARA CONTRATOS.
	 * @param direccion
	 *            Direccion de el cliente.
	 * @param ciudad
	 *            Ciudad en la que reside el cliente.
	 * @param provincia
	 *            Provincia en la que vive el cliente.
	 * @param pais
	 *            Pais en el que vive el cliente.
	 * @param cp
	 *            Codigo Postal del cliente.
	 * @param empresa
	 *            Empresa para la que trabaja el cliente (por si pide factura a
	 *            nombre de empresa).
	 * @throws InvalidUserException
	 */
	public int modCliente(Cliente cliente, String tlf, String nombre, String primerApellido, String segundoApellido,
			String nif, String dnia, String movil, String eMail, String direccion, String ciudad, String provincia,
			String pais, String cp, String empresa) throws InvalidUserException {
		if (!esAdmin()) {
			return this.gestorClientes.modCliente(cliente, tlf, nombre, primerApellido, segundoApellido, nif, dnia,
					movil, eMail, direccion, ciudad, provincia, pais, cp, empresa);
		} else {
			throw new InvalidUserException();
		}
	}

	/**
	 * Modifica contrato premium
	 * 
	 * @param gasto
	 *            Nuevo gasto para recibir el descuento de menudencias
	 * @param desc
	 *            Nuevo % de descuento de envio de voluminosos
	 * @param subs
	 *            Nueva cantidad de subastas gratuitas
	 * @param prec
	 *            Nuevo precio del tipo de contrato
	 * @throws InvalidUserException
	 *             Salta si el usuario actual no tiene permiso para hacer esa
	 *             operacion
	 */
	public void modContrPrem(float gasto, int desc, int subs, float prec) throws InvalidUserException {
		if (esAdmin()) {
			this.gestorClientes.modContrPrem(gasto, desc, subs, prec);
		} else
			throw new InvalidUserException();
	}

	/**
	 * Pone a subastar un articulo o lote
	 * 
	 * @param entrada
	 *            Precio que cuesta la entrada a esa subasta.
	 * @param duracion
	 *            Cuantos dias dura la subasta a partir de la primera puja.
	 * @param articulo
	 *            El articulo a subastar.
	 * @param mensaje
	 *            El mensaje que se va a usar en la subasta
	 * @return Lista de clientes en los que ha habido fallo
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public ArrayList<Cliente> nuevaSubasta(float entrada, int duracion, String mensaje, Articulo articulo)
			throws InvalidUserException {
		if (esAdmin())
			try {
				Subasta subasta = this.gestorArticulos.iniciarSubasta(entrada, mensaje, duracion, articulo);
				return this.gestorClientes.notificarSubasta(subasta);
			} catch (NotValidArtException | FailedInternetConnectionException e) {
				e.printStackTrace();
			}
		else
			throw new InvalidUserException();
		return null;
	}

	/**
	 * Devuelve si al cliente hay que notificarlo
	 * 
	 * @param cl
	 *            Cliente a notificar
	 * @return True si se le notifica, false si no
	 */
	public boolean getIfNotify(Cliente cl) {
		return this.gestorClientes.getIfNotify(cl);
	}

	/**
	 * Cambia si hay que notificar o no al cliente
	 * 
	 * @param cl
	 * @param flag
	 * @throws InvalidUserException
	 */
	public void setIfNotify(Cliente cl, boolean flag) throws InvalidUserException {
		if (!esAdmin()) {
			this.gestorClientes.setIfNotify(cl, flag);
		}
	}

	/**
	 * Crea un nuevo empleado
	 * 
	 * @param usuario
	 *            Nombre de usuario de un empleado OBLIGATORIO
	 * @param password
	 *            Contrasenya de usuario de un empleado OBLIGATORI
	 * @param nombre
	 *            Nombre de un empleado
	 * @param apellido1
	 *            Primer apellido de un empleado
	 * @param apellido2
	 *            Segundo apellido de un empleado
	 * @param tlf
	 *            Telefono de un empleado
	 * @param movil
	 *            Telefono movil de un empleado
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public void nuevoEmpleado(String usuario, String password, String nombre, String apellido1, String apellido2,
			String tlf, String movil) throws InvalidUserException {
		if (esAdmin()) {
			this.gestorUsuarios.nuevoEmpleado(usuario, password, nombre, apellido1, apellido2, tlf, movil);
		} else
			throw new InvalidUserException();
	}

	/**
	 * Modifica un empleado existente
	 * 
	 * @param empleado
	 *            Empleado a modificar
	 * @param usuario
	 *            Nombre de usuario de un empleado OBLIGATORIO
	 * @param password
	 *            Contrasenya de usuario de un empleado OBLIGATORI
	 * @param nombre
	 *            Nombre de un empleado
	 * @param apellido1
	 *            Primer apellido de un empleado
	 * @param apellido2
	 *            Segundo apellido de un empleado
	 * @param tlf
	 *            Telefono de un empleado
	 * @param movil
	 *            Telefono movil de un empleado
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public void modEmpleado(Empleado empleado, String usuario, String password, String nombre, String apellido1,
			String apellido2, String tlf, String movil) throws InvalidUserException {
		if (esAdmin()) {
			this.gestorUsuarios.modEmpleado(empleado, usuario, password, nombre, apellido1, apellido2, tlf, movil);
		} else
			throw new InvalidUserException();
	}

	/**
	 * Modifica la constraseña del administrador
	 * 
	 * @param password
	 *            Contrasenya del admin para asegurar que se tenga utoridad de
	 *            cambiar
	 * @param newPassword
	 *            Nueva contrasenya del admin
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public void modPasAdmin(String password, String newPassword) throws InvalidUserException {
		if (esAdmin()) {
			if (this.usuarioActual.validar(password)) {
				this.usuarioActual.setPassword(newPassword);
				return;
			}
		}
		throw new InvalidUserException();
	}

	/**
	 * Busca un empleado en el repositorio por username para mostrar sus datos o
	 * modificarlo
	 * 
	 * @param username
	 *            Nombre de usuario del empleado a buscar
	 * @return El empleado si se ha encontrado, null si no
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public Empleado buscarEmpleado(String username) throws InvalidUserException {
		Personal personal;
		if (esAdmin()) {
			personal = this.gestorUsuarios.get(username);
			if (personal instanceof Empleado)
				return (Empleado) personal;
			else
				return null;
		}
		throw new InvalidUserException();
	}

	/**
	 * Cancela una subasta en la cual no se haya pujado aun
	 * 
	 * @param subasta
	 *            La subasta a cancelar
	 * @return true si se ha cancelado la subasta, false si no
	 */
	public boolean cancelarSubasta(Subasta subasta) {
		return this.gestorArticulos.cancelarSubasta(subasta);
	}

	/* OPERACIONES DE LOS EMPLEADOS */

	/**
	 * Hace una venta de un set de articulos a un cliente.
	 * 
	 * @param cliente
	 *            El cliente al cual se vende el articulo.
	 * @param articulos
	 *            Set de articulos a vender.
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public float nuevaVenta(Cliente cliente, ArrayList<Articulo> articulos) throws InvalidUserException {
		if (!esAdmin())
			try {
				int descuentoVoluminosos = this.gestorClientes.getPorcenEnv(cliente);
				boolean descuentoMenudAplic = this.gestorClientes.getAplicarDescuento(cliente);
				return gestorArticulos.nuevaVenta(cliente, articulos, descuentoMenudAplic, descuentoVoluminosos);
			} catch (NotValidArtException | GclienException e) {
				e.printStackTrace();
			}
		else
			throw new InvalidUserException();
		return -1;
	}

	/**
	 * Hace una venta de un articulo a un cliente.
	 * 
	 * @param cliente
	 *            El cliente al cual se vende el articulo.
	 * @param articulo
	 *            El articulo.
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public float nuevaVenta(Cliente cliente, Articulo articulo) throws InvalidUserException {
		if (!esAdmin())
			try {
				int descuentoVoluminosos = this.gestorClientes.getPorcenEnv(cliente);
				boolean descuentoMenudAplic = this.gestorClientes.getAplicarDescuento(cliente);
				return gestorArticulos.nuevaVenta(cliente, articulo, descuentoMenudAplic, descuentoVoluminosos);
			} catch (NotValidArtException | GclienException e) {
				e.printStackTrace();
			}
		else
			throw new InvalidUserException();
		return -1;
	}

	/**
	 * Anyade una puja nueva a una subasta
	 * 
	 * @param subasta
	 *            Subasta a la que se anyade la puja
	 * @param cliente
	 *            Cliente a nombre del cual se hace la puja
	 * @param cantidad
	 *            Nueva cantidad por la que se puja
	 * @return true si la puja se ha actualizado, false si no
	 */
	public boolean realizarPuja(Subasta subasta, Cliente cliente, float cantidad) throws InvalidUserException {
		boolean retVal = false;
		float precioMinimo;
		if (!esAdmin()) {
			try {
				retVal = this.gestorClientes.pujasGratis(cliente);
				precioMinimo = this.gestorArticulos.peekSubasta(subasta, cliente, retVal);
				if (cantidad > precioMinimo) {
					/* Aqui va la funcion que hace la subasta */
					retVal = this.gestorClientes.gastarSubasta(cliente);
					if (!subasta.anh(cliente, cantidad))
						return false;
				}
			} catch (ContractNotFoundException | InvalidEmailAddressException
					| FailedInternetConnectionException excepcion) {
				return false;
			}
		} else
			throw new InvalidUserException();
		return retVal;
	}

	/**
	 * Crea un nuevo contrato, premium o normal a un cliente
	 * 
	 * @param cliente
	 *            al que se le hace la puja
	 * @param isPremium
	 *            true si el contrato es premium, false si no lo se
	 * @return El precio del contrato
	 */
	public float crearContrato(Cliente cliente, boolean isPremium) {
		this.gestorClientes.nuevoContrato(cliente, isPremium);
		if (isPremium) {
			return this.gestorClientes.getPrecioPrem();
		} else {
			return this.gestorClientes.getPrecioNormal();
		}
	}

	/**
	 * Anhade un nuevo artículo básico (Los lotes no son válidos).
	 * 
	 * @param tipo
	 *            Tipo de artículo
	 * @param fAdq
	 *            Fecha de adquisición
	 * @param fDat
	 *            Fecha de datación del artículo
	 * @param descr
	 *            Descripción del artículo
	 * @param pAdq
	 *            Precio de adquisición
	 * @param pObj
	 *            Precio objetivo
	 * @param cert
	 *            Si posee o no certificado (Arte)
	 * @param descuento
	 *            Descuento aplicable a la venta (Menudencia)
	 * @param tipoObra
	 *            El tipo de obra (Arte)
	 * @param ancho
	 *            Las dimensiones a lo ancho (Voluminoso)
	 * @param fondo
	 *            Las dimensiones a lo largo (Voluminoso)
	 * @param alto
	 *            Las dimensiones a lo alto (Voluminoso)
	 * @param peso
	 *            El peso del artículo (Voluminoso)
	 * @param autor
	 *            Autor de la obra (Arte)
	 * @return true si el artículo se creó con éxito, false si no
	 * @throws MalformedArtException
	 *             en caso de que los argumentos no sean válidos.
	 */
	public boolean nuevoArticulo(String tipo, Date fAdq, String fDat, String descr, float pAdq, float pObj,
			boolean cert, int descuento, TipoObra tipoObra, int ancho, int fondo, int alto, int peso, String autor)
			throws MalformedArtException {
		switch (tipo) {
		case "Arte":
			return (gestorArticulos.anhArte(descr, fAdq, pAdq, pObj, tipoObra, cert, autor, fDat) != null);
		case "Menudencia":
			return (gestorArticulos.anhMenudencia(descr, fAdq, pAdq, pObj, descuento, fDat) != null);
		case "Voluminoso":
			return (gestorArticulos.anhVoluminoso(descr, fAdq, pAdq, pObj, peso, alto, ancho, fondo, fDat) != null);
		default:
			throw new MalformedArtException();
		}
	}

	/**
	 * Añade un nuevo cliente
	 * 
	 * @param tlf
	 *            Telefono del cliente OBLIGATORIO.
	 * @param nombre
	 *            Nombre del cliente OBLIGATORIO.
	 * @param primerApellido
	 *            Primer apellido del cliente.
	 * @param segundoApellido
	 *            Segundo apellido del cliente.
	 * @param nif
	 *            NIF del cliente.
	 * @param dnia
	 *            DNI o NIA del cliente.
	 * @param movil
	 *            Telefono movil del cliente.
	 * @param eMail
	 *            Correo del cliente NECESARIO PARA CONTRATOS.
	 * @param direccion
	 *            Direccion de el cliente.
	 * @param ciudad
	 *            Ciudad en la que reside el cliente.
	 * @param provincia
	 *            Provincia en la que vive el cliente.
	 * @param pais
	 *            Pais en el que vive el cliente.
	 * @param cp
	 *            Codigo Postal del cliente.
	 * @param empresa
	 *            Empresa para la que trabaja el cliente (por si pide factura a
	 *            nombre de empresa).
	 * @return true si el cliente se ha anyadido, false si no
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene permiso para hacer
	 *             esa operacion
	 */
	public boolean nuevoCliente(String tlf, String nombre, String primerApellido, String segundoApellido, String nif,
			String dnia, String movil, String eMail, String direccion, String ciudad, String provincia, String pais,
			String cp, String empresa) throws InvalidUserException {
		if (!esAdmin()) {
			return this.gestorClientes.nuevoCliente(tlf, nombre, primerApellido, segundoApellido, nif, dnia, movil,
					eMail, direccion, ciudad, provincia, pais, cp, empresa);
		} else
			throw new InvalidUserException();
	}

	/* METODOS GENERALES QUE LES VIENEN BIEN A LOS DOS */

	/**
	 * Añade un artículo al repositorio.
	 * 
	 * @param art
	 */
	public void addArt(Articulo art) {
		gestorArticulos.anhArticulo(art);
	}

	public int nuevosArticulos(String path) throws MalformedArtException, IOException, ParseException {
		return gestorArticulos.anhArticulos(path);
	}

	/**
	 * Devuelve el artículo, si se encuentra, para mostrar sus datos en la
	 * interfaz.
	 * 
	 * @param id
	 *            Identificador único del artículo.
	 * @return El artículo del cual se busca el ID.
	 */
	public Articulo buscarArticulo(long id) {
		return gestorArticulos.buscarArticulo(id);
	}

	/**
	 * Devuelve el cliente, si se encuentra para mostrar sus datos en la
	 * interfaz.
	 * 
	 * @param tlf
	 *            El telefono de cada cliente (unico para cada uno)
	 * @return El cliente El cliente del cual se busca el telefono
	 */
	public Cliente buscarCliente(String tlf) {
		return this.gestorClientes.buscarCliente(tlf);
	}

	/**
	 * Devuelve el lote, si de verdad existe, que contiene al artículo cuyo ID
	 * se manda como argumento.
	 * 
	 * @param id
	 *            El IDdel artículo cuyo lote vamos a buscar
	 * @return El lote que contiene al artículo.
	 */
	public Lote buscarLote(long id) {
		return gestorArticulos.buscarLote(id);
	}

	/**
	 * Filtra los clientes segun varios criterios
	 * 
	 * @param conContrato
	 *            true si se quiere buscar clientes que tengan contrato, false
	 *            si no
	 * @param nombre
	 *            Si no esta vacia o vale null, filtra clientes que se llamen
	 *            asi
	 * @param apellido
	 *            Si no esta vacia o vale null, filtra clientes que uno de sus
	 *            apellidos sea ese
	 * @param eMail
	 *            Si no es vacio o null, filtra clientes con ese email
	 * @return Una coleccion de clientes que cumplan los requisitos de filtrado
	 */
	public Collection<Cliente> filtClien(boolean conContrato, String nombre, String apellido, String eMail) {
		return this.gestorClientes.filtrado(conContrato, nombre, apellido, eMail);
	}

	/**
	 * Filtra las subastas por varios criterios
	 * 
	 * @return Una collección de subastas que cumplen con los criterios de
	 *         filtrado.
	 */
	public Collection<Subasta> filtSub(Articulo art, Cliente cl, Date fechaM, Date fecham, float pujaM, float pujam) {
		return this.gestorArticulos.filtrado(art, cl, fechaM, fecham, pujaM, pujam);
	}

	/**
	 * Filtra los articulos por varios criterios
	 * 
	 * @return
	 */
	public Collection<Articulo> filtArt(boolean conCertificado, String fDat, Estado est, Date fadq, float precioM,
			float preciom, String tipo) {
		return this.gestorArticulos.filtrado(conCertificado, fDat, est, fadq, precioM, preciom, tipo);
	}

	/**
	 * Genera el informe mensual
	 * 
	 * @throws InvalidUserException
	 *             Salta si el usuario actual no tiene permiso para hacer esa
	 *             tarea
	 */
	public void generarInforme() throws InvalidUserException {
		if (!esAdmin())
			throw new InvalidUserException();
		try {
			System.out.println(this.gestorArticulos.informeMensual());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crea un lote para un conjunto de artículos pasados como argumento en un
	 * ArrayList.
	 * 
	 * @param descr
	 *            Descripción del artículo.
	 * @param fecha
	 *            Fecha de adquisición del artículo.
	 * @param p1
	 *            Precio de adquisicion del artículo.
	 * @param p2
	 *            Precio objetivo.
	 * @param f
	 *            Fecha de datacion del conjunto de artículos del lote.
	 * @param list
	 *            Lista con los artículos del lote.
	 * @throws InvalidUserException
	 *             Salta cuando el usuario actual no tiene autorizacion para
	 *             usar este metodo
	 */
	public void crearLote(String descr, Date fecha, float p1, float p2, String f, ArrayList<Articulo> list)
			throws InvalidUserException {
		if (!esAdmin())
			throw new InvalidUserException();
		try {
			this.gestorArticulos.anhLote(descr, fecha, p1, p2, f, list);
		} catch (MalformedArtException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Elimina el lote enviado como argumento y reinicia el estado de sus
	 * componenetes.
	 * 
	 * @param lote
	 *            El lote a eliminar y descomponer.
	 * 
	 * @return true en caso de éxito, false si el lote no existe o está vendido.
	 */
	public boolean eliminarLote(Lote lote) {
		return gestorArticulos.elLote(lote);
	}
}