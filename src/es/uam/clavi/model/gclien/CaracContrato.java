package es.uam.clavi.model.gclien;

import java.io.Serializable;

/**
 * Esta clase almacena las caracteristicas editables del
 * contrato.
 *  
 * @author Jesualdo Garcia Lopez
 *
 */
public final class CaracContrato implements Serializable{
	
	private static final long serialVersionUID = 3982063675302377942L;
	private float gastoParaDesMen;
	private int descEnvio;
	private int subastaGratis;
	private float precio;
	
	/**
	 * Constructor por defecto, inicia las variables en el valor por defecto
	 * de las caracteristicas de contrato.
	 */
	public CaracContrato(){
		this.gastoParaDesMen = 200;
		this.descEnvio = 0;
		this.subastaGratis = 5;
		this.precio = 25;
	}
	
	/**
	 * Constructor al que le damos valores estandar
	 * @param gasto Gasto necesairo para aplicar descuento de menudencias
	 * @param desc Descuento sobre el envio de articulos voluminosos
	 * @param subs Subastas gratuitas de el contrato, puede valer -1 si son todas
	 * @param prec Precio del propio contrato
	 */
	public CaracContrato(float gasto, int desc, int subs, float prec){
		this.gastoParaDesMen = gasto;
		this.descEnvio = desc;
		this.subastaGratis = subs;
		this.precio = prec;
	}

	/**
	 * Devuelve el gasto para que haya descuento de menudencias
	 * @return El gastoParaDesMen
	 */
	public float getGastoParaDesMen() {
		return this.gastoParaDesMen;
	}

	/**
	 * Coloca el gasto para el descuento de menudencias
	 * @param gastoParaDesMen El gastoParaDesMen para colocar
	 */
	public void setGastoParaDesMen(float gastoParaDesMen) {
		this.gastoParaDesMen = gastoParaDesMen;
	}

	/**
	 * Devuelve el % de descuento de envio
	 * @return El descEnvio
	 */
	public int getDescEnvio() {
		return descEnvio;
	}

	/**
	 * Coloca un nuevo % de descuento de envio
	 * @param descEnvio El descEnvio para colocar
	 */
	public void setDescEnvio(int descEnvio) {
		this.descEnvio = descEnvio;
	}

	/**
	 * Devuelve el numero de subastas gratuitas
	 * @return El subastaGratis
	 */
	public int getSubastaGratis() {
		return subastaGratis;
	}

	/**
	 * Coloca un nuevo numero de subastas gratuitas
	 * @param subastaGratis El subastaGratis para colocar
	 */
	public void setSubastaGratis(int subastaGratis) {
		this.subastaGratis = subastaGratis;
	}

	/**
	 * Devuelve el precio del contrato
	 * @return El precio del contrato
	 */
	public float getPrecio() {
		return new Float(precio);
	}

	/**
	 * Modifica el precio del contrato
	 * @param precio El precio para colocar
	 */
	public void setPrecio(float precio) {
		this.precio = precio;
	}
}
