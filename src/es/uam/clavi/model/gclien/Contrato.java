package es.uam.clavi.model.gclien;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Clase abstracta que controla la caducidad de un contrato
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public abstract class Contrato implements Serializable {

	private static final long serialVersionUID = -3418636187689495648L;
	private Date fechaCaducidad;
	private boolean notificar;

	/**
	 * Constructor, asigna al objeto una fecha de caducidad a un anho desde que
	 * se creo.
	 */
	protected Contrato() {
		this.fechaCaducidad = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.fechaCaducidad);
		cal.add(Calendar.YEAR, 1);
		this.fechaCaducidad = cal.getTime();
		this.notificar = true;
	}

	/**
	 * Devuelve la fecha de caducidad de un contrato
	 */
	public Date getFechaCaduc() {
		return fechaCaducidad;
	}

	/**
	 * Devuelve si el contrato esta caducado o no
	 */
	public boolean caducado() {
		Date tempDate = new Date();
		return fechaCaducidad.before(tempDate);
	}

	public boolean getNotificar() {
		return notificar;
	}

	public void setNotificar(boolean flag) {
		this.notificar = flag;
	}
}
