package es.uam.clavi.model.gclien;

/**
 * Excepcion de carga del Gestor de Clientes, salta cuando
 * se producen errores de carga.
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public class GclienLoadException extends GclienException {

	private static final long serialVersionUID = 7034719766124013233L;

}
