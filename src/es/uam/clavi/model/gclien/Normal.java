package es.uam.clavi.model.gclien;

/**
 * Clase que contiene las propiedades del contrato normal
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public final class Normal extends Contrato{
	
	private static final long serialVersionUID = 7993921088610129337L;
	private int subUsadas;
	private float gastoVentas;
	
	/**
	 * Constructor, asigna los valores 0 a subastas usadas y dinero gastado en compras.
	 */
	public Normal(){
		super();
		subUsadas = 0;
		gastoVentas = 0;
	}
	
	/**
	 * Devuelve el valor de las subastas usadas
	 * @return El numero de subastas usadas
	 */
	public int getUsadas(){
		return subUsadas;
	}
	/**
	 * Devuelve el dinero gastado en ventas
	 * @return El dinero gastado en ventas
	 */
	public float getGastoVentas(){
		return gastoVentas;
	}
	/**
	 * Aumenta en uno las subastas usadas
	 * @return El numero de subastas usadas post-cambio
	 */
	public int addSubUsadas(){
		subUsadas++;
		return subUsadas;
	}
	/**
	 * Solo util en caso de fallo, cuando se ha anhadido una subasta
	 * usada erróneamente. En principio no debería ser necesario usarlo,
	 * pero se anhade por completitud.
	 * @return El numero de subastas usadas post-cambio.
	 */
	public int subSubUsadas(){
		subUsadas--;
		return subUsadas;
	}
	/**
	 * Anhade un dinero que se ha gastado el cliente en una venta y
	 * que se anhade al contrato por proposito de descuentos
	 * @param gasto Dinero que se ha gastado el dueño del contrato
	 * @return Gasto total actual
	 */
	public float addGastoVentas(float gasto){
		gastoVentas = gastoVentas + gasto;
		return gastoVentas;
	}
	/**
	 * Solo util en caso de error. Para quitar de un cliente el gasto
	 * que haya realizado.
	 * @param gasto Gasto erroneo que queremos quitar
	 * @return El gasto corregido
	 */
	public float subGastoVentas(float gasto){
		gastoVentas = gastoVentas - gasto;
		return gastoVentas;
	}
}
