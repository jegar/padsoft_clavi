package es.uam.clavi.model.gclien;

/**
 * Excepcion generica del Gestor de Clientes
 * @author jesu
 *
 */
public class GclienException extends Exception{

	private static final long serialVersionUID = 3897468549106270104L;

}
