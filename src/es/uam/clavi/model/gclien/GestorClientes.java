package es.uam.clavi.model.gclien;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.eps.padsof.emailconnection.FailedInternetConnectionException;
import es.uam.eps.padsof.emailconnection.InvalidEmailAddressException;

/**
 * Esta clase implementa el Gestor de Clientes. Este gestor almacena y gestiona
 * Clientes y Contratos, ademas de las caracteristicas de los contratos.
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public final class GestorClientes {

	private enum Criterio {
		CONTRATO, NOMBRE, APELLIDOS, EMAIL
	}

	private HashMap<String, Cliente> repoClientes;
	private CaracContrato cNormal;
	private CaracContrato cPremium;
	private String path_to_file;
	private String msg_nueva_subasta = "Nueva subasta disponible";

	/**
	 * Constructor del gestor que inicia su lista
	 * 
	 * @param path
	 *            El directorio en el cual se guarda y carga el archivo
	 * @throws GclienLoadException
	 */
	public GestorClientes(String path) throws GclienLoadException {
		this.path_to_file = path + File.separator + "gclien";
		if (!abrirGestor()) {
			repoClientes = new HashMap<String, Cliente>();
			cNormal = new CaracContrato();
			cPremium = new CaracContrato(0, 50, -1, 100);
		}
	}

	/**
	 * Busca un clientes por su tlf
	 * 
	 * @param tlf
	 *            Telefono del cliente a buscar
	 * @return El cliente, si ha sido encontrado, null si no
	 */
	public Cliente buscarCliente(String tlf) {
		return repoClientes.get(tlf);
	}

	/**
	 * Anyade un cliente al repositiorio de Clientes
	 * 
	 * @param cliente
	 *            El cliente a anyadir
	 */
	private void anyadirCliente(Cliente cliente) {
		repoClientes.put(cliente.getTlf(), cliente);
	}

	/**
	 * Crea un nuevo cliente y lo mete en el repositorio.
	 * 
	 * @param tlf
	 *            Telefono del cliente OBLIGATORIO.
	 * @param nombre
	 *            Nombre del cliente OBLIGATORIO.
	 * @param primerApellido
	 *            Primer apellido del cliente.
	 * @param segundoApellido
	 *            Segundo apellido del cliente.
	 * @param nif
	 *            NIF del cliente.
	 * @param dnia
	 *            DNI o NIA del cliente.
	 * @param movil
	 *            Telefono movil del cliente.
	 * @param eMail
	 *            Correo del cliente NECESARIO PARA CONTRATOS.
	 * @param direccion
	 *            Direccion de el cliente.
	 * @param ciudad
	 *            Ciudad en la que reside el cliente.
	 * @param provincia
	 *            Provincia en la que vive el cliente.
	 * @param pais
	 *            Pais en el que vive el cliente.
	 * @param cp
	 *            Codigo Postal del cliente.
	 * @param empresa
	 *            Empresa para la que trabaja el cliente (por si pide factura a
	 *            nombre de empresa).
	 * @return true si se ha anyadido el cliente sin problema, false si no
	 */
	public boolean nuevoCliente(String tlf, String nombre, String primerApellido, String segundoApellido, String nif,
			String dnia, String movil, String eMail, String direccion, String ciudad, String provincia, String pais,
			String cp, String empresa) {

		if ((tlf == null) || (nombre == null))
			return false;
		Cliente nuevoCliente = new Cliente(tlf, nombre, primerApellido, segundoApellido, nif, dnia, movil, eMail,
				direccion, ciudad, provincia, pais, cp, empresa);
		anyadirCliente(nuevoCliente);
		return true;
	}

	/**
	 * Esta funcion devuelve los clientes que tienen contrato
	 * 
	 * @return Clientes con contrato
	 */
	public ArrayList<Cliente> clientConContr() {
		Collection<Cliente> coleccion = this.repoClientes.values();
		return clientConContr(coleccion);
	}

	/**
	 * Esta funcion devuelve los clientes que tienen contrato de una coleccion
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar
	 * @return Lista de clientes con contrato
	 */
	public ArrayList<Cliente> clientConContr(Collection<Cliente> coleccion) {
		return filtClientes(coleccion, Criterio.CONTRATO, null);
	}

	/**
	 * Esta funcion filtra el repoClientes por nombre.
	 * 
	 * @param nombre
	 *            Por el que buscar
	 * @return Clientes que se llaman asi.
	 */
	public ArrayList<Cliente> clientNombre(String nombre) {
		Collection<Cliente> coleccion = this.repoClientes.values();
		return clientNombre(coleccion, nombre);
	}

	/**
	 * Esta funcion filtra el repoClientes por nombre sobre una coleccion dada.
	 * 
	 * @param coleccion
	 *            Sobre la que buscar.
	 * @param nombre
	 *            Por el que buscar
	 * @return Clientes que se llaman asi.
	 */
	public ArrayList<Cliente> clientNombre(Collection<Cliente> coleccion, String nombre) {
		return filtClientes(coleccion, Criterio.NOMBRE, nombre);
	}

	/**
	 * Filtra los clientes por su apellido
	 * 
	 * @param apellido
	 *            Apellido por el que buscar
	 * @return Lista de clientes con ese apellido
	 */
	public ArrayList<Cliente> clientApellido(String apellido) {
		Collection<Cliente> coleccion = this.repoClientes.values();
		return clientApellido(coleccion, apellido);
	}

	/**
	 * Filtra los clientes por su apellido de una coleccion
	 * 
	 * @param apellido
	 *            Apellido por el que buscar
	 * @param coleccion
	 *            Coleccion en la que buscar
	 * @return Lista de clientes con ese apellido
	 */
	public ArrayList<Cliente> clientApellido(Collection<Cliente> coleccion, String apellido) {
		return filtClientes(coleccion, Criterio.APELLIDOS, apellido);
	}

	/**
	 * Filtra los clientes por su correo electronico
	 * 
	 * @param eMail
	 *            Correo por el que buscar
	 * @return Lista de clientes con ese correo
	 */
	public ArrayList<Cliente> clientEmail(String eMail) {
		Collection<Cliente> coleccion = this.repoClientes.values();
		return clientEmail(coleccion, eMail);
	}

	/**
	 * Filtra los clientes por su eMail de una coleccion
	 * 
	 * @param apellido
	 *            eMail por el que buscar
	 * @param coleccion
	 *            Coleccion en la que buscar
	 * @return Lista de clientes con ese correo
	 */
	public ArrayList<Cliente> clientEmail(Collection<Cliente> coleccion, String eMail) {
		return filtClientes(coleccion, Criterio.EMAIL, eMail);
	}

	/**
	 * Esta funcion filtra el repoClientes a una lista, dada con
	 * 
	 * @param criterio
	 *            El criterio por el que se va a filtrar
	 * @param comp
	 *            Cadena con la que se va a comparar, si aplicable
	 * @return La lista de los elementos que cumplen la condicion
	 */
	private ArrayList<Cliente> filtClientes(Collection<Cliente> coleccion, Criterio criterio, String comp) {
		ArrayList<Cliente> tempList = new ArrayList<Cliente>();
		for (Cliente actual : coleccion) {
			if (eleCriterio(actual, criterio, comp)) {
				tempList.add(actual);
			}
		}
		return tempList;
	}

	/**
	 * Funcion privada que elige el criterio por el que se va a filtrar
	 * 
	 * @param actual
	 *            El elemento siendo filtrado
	 * @param criterio
	 *            El criterio por el cual se va a filtrar
	 * @param comp
	 *            En caso de necesitarlo, con lo que se va a comparar
	 * @return true si pasa el filtro, false si no
	 */
	private boolean eleCriterio(Cliente actual, Criterio criterio, String comp) {
		switch (criterio) {
		case CONTRATO:
			return actual.poseeContrato();
		case NOMBRE:
			return comp.equals(actual.getNombre());
		case APELLIDOS:
			return (comp.equals(actual.getPrimerApellido()) || comp.equals(actual.getSegundoApellido()));
		case EMAIL:
			return comp.equals(actual.geteMail());
		default:
			return false;
		}
	}

	/**
	 * Crea un nuevo contrato para un cliente y lo anhade a la lista de
	 * contratos y al cliente
	 * 
	 * @param duenho
	 *            El comprador del contrato
	 * @param isPremium
	 *            true si es un contrato premium, false si no.
	 * @return true si se ha creado un nuevo contrato con exito, false si no
	 */
	public boolean nuevoContrato(Cliente duenho, boolean isPremium) {
		Contrato contrato;
		if (es.uam.eps.padsof.emailconnection.EmailSystem.isValidEmailAddr(duenho.geteMail())) {
			if (isPremium) {
				contrato = new Premium();
			} else {
				contrato = new Normal();
			}
			duenho.setContrato(contrato);
			return true;
		} else
			return false;
	}

	/**
	 * Modifica el contrato normal
	 * 
	 * @param gasto
	 *            Gasto para conseguir descuento en menudencias
	 * @param desc
	 *            Descuentos de envios voluminosos
	 * @param subs
	 *            Subastas Gratuitas
	 * @param prec
	 *            Precio del contrato
	 * @return Numero de propiedades cambiadas
	 */
	public int modContrNorm(float gasto, int desc, int subs, float prec) {
		int counter = 0;
		if (cNormal.getDescEnvio() != desc) {
			cNormal.setDescEnvio(desc);
			counter++;
		}
		if (cNormal.getGastoParaDesMen() != gasto) {
			cNormal.setGastoParaDesMen(gasto);
			counter++;
		}
		if (cNormal.getPrecio() != prec) {
			cNormal.setPrecio(prec);
			counter++;
		}
		if (cNormal.getSubastaGratis() != subs) {
			cNormal.setSubastaGratis(subs);
			counter++;
		}
		return counter;
	}

	/**
	 * Modifica el contrato premium
	 * 
	 * @param gasto
	 *            Gasto para conseguir descuento en menudencias
	 * @param desc
	 *            Descuentos de envios voluminosos
	 * @param subs
	 *            Subastas Gratuitas
	 * @param prec
	 *            Precio del contrato
	 * @return Numero de propiedades cambiadas
	 */
	public int modContrPrem(float gasto, int desc, int subs, float prec) {
		int counter = 0;
		if (cPremium.getDescEnvio() != desc) {
			cPremium.setDescEnvio(desc);
			counter++;
		}
		if (cPremium.getGastoParaDesMen() != gasto) {
			cPremium.setGastoParaDesMen(gasto);
			counter++;
		}
		if (cPremium.getPrecio() != prec) {
			cPremium.setPrecio(prec);
			counter++;
		}
		if (cPremium.getSubastaGratis() != subs) {
			cPremium.setSubastaGratis(subs);
			counter++;
		}
		return counter;
	}

	/**
	 * Notifica a todos los clientes que tengan contrato
	 * 
	 * @param subasta
	 * @return Lista de fallos de email, null si no ha habido fallos.
	 */
	public ArrayList<Cliente> notificarSubasta(Subasta subasta) throws FailedInternetConnectionException {
		ArrayList<Cliente> errores = new ArrayList<Cliente>();
		for (Cliente cl : this.clientConContr()) {
			try {
				if (cl.getContrato().getNotificar())
					es.uam.eps.padsof.emailconnection.EmailSystem.send(cl.geteMail(), msg_nueva_subasta,
							subasta.getMensaje());
			} catch (InvalidEmailAddressException e) {
				/*
				 * Cuando algunos no han podido enviarse, se meten en una lista
				 */
				errores.add(cl);
			}
		}
		return errores;
	}

	/**
	 * Dice si hay que notificar a un cliente en concreto
	 * 
	 * @param cliente
	 *            Cliente a notificar o no
	 * @return true si se notifica, false si no.
	 */
	public boolean getIfNotify(Cliente cliente) {
		if (cliente.getContrato() != null) {
			return cliente.getContrato().getNotificar();
		}
		return false;
	}

	/**
	 * Cambia si hay que notificar al cliente o no
	 * 
	 * @param cliente
	 *            Cliente a notificar
	 * @param flag
	 *            True si hay que notificarlo, false si no
	 */
	public void setIfNotify(Cliente cliente, boolean flag) {
		if (cliente.poseeContrato()) {
			cliente.getContrato().setNotificar(flag);
		}
	}

	/**
	 * Comprueba si le quedan pujas gratis al cliente
	 * 
	 * @param cliente
	 *            El cliente de el que queremos saber las pujas
	 * @return true si le quedan subastas gratis, false si no
	 * @throws ContractNotFoundException
	 */
	public boolean pujasGratis(Cliente cliente) throws ContractNotFoundException {

		if (cliente.poseeContrato()) {
			if ((cliente.getContrato() instanceof Premium)) {
				return true;
			} else if (cliente.getContrato() instanceof Normal) {
				Normal contrato = (Normal) cliente.getContrato();
				if ((contrato.getUsadas() < cNormal.getSubastaGratis()) || (cNormal.getSubastaGratis() == -1)) {
					return true;
				}
			}
		} else {
			throw new ContractNotFoundException();
		}
		return false;
	}

	/**
	 * Gastamos una subasta gratuita de un cliente
	 * 
	 * @param cliente
	 *            Cliente de el que se gasta la subastas
	 * @return true si se ha gastado la subasta correctamente, false si no
	 * @throws ContractNotFoundException
	 */
	public boolean gastarSubasta(Cliente cliente) throws ContractNotFoundException {
		if (!cliente.poseeContrato()) {
			throw new ContractNotFoundException();
		} else if (cliente.getContrato() instanceof Normal) {
			if (this.pujasGratis(cliente)) {
				Normal contrato = (Normal) cliente.getContrato();
				contrato.addSubUsadas();
				return true;
			}
		} else if (cliente.getContrato() instanceof Premium) {
			return true;
		}
		return false;
	}

	/**
	 * Guarda los clientes
	 * 
	 * @param out
	 *            Stream de objetos para guardar los clientes
	 * @throws IOException
	 *             Excepcion de entrada-salida
	 */
	private void guardarClientes(ObjectOutputStream out) throws IOException {
		out.writeObject(this.repoClientes);
	}

	/**
	 * Guarda las caracteristicas de los contratos
	 * 
	 * @param out
	 *            Stream de objetos para guardar los caracteristicas
	 * @throws IOException
	 *             Excepcion de entrada-salida
	 */
	private void guardarCaract(ObjectOutputStream out) throws IOException {
		out.writeObject(this.cNormal);
		out.writeObject(this.cPremium);
	}

	/**
	 * Realiza las operaciones necesarias para guardar los elementos del gestor
	 */
	public void cerrarGestor() throws GclienSaveException {
		try {
			File file = new File(this.path_to_file);
			if (!file.isFile()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			FileOutputStream output = new FileOutputStream(file, false);
			ObjectOutputStream out = new ObjectOutputStream(output);
			guardarClientes(out);
			guardarCaract(out);
			out.close();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new GclienSaveException();
		}
	}

	/**
	 * Carga el gestor si hay un archivo de guardado, si no devuelve false
	 * 
	 * @return false si no hay archivo desde el que cargar, true si se carga
	 * @throws GclienSaveException
	 *             Salta cuando ocurre un error de guardado del gestor de
	 *             clientes
	 */
	public boolean abrirGestor() throws GclienLoadException {
		if (new File(path_to_file).isFile()) {
			try {
				FileInputStream input = new FileInputStream(path_to_file);
				ObjectInputStream in = new ObjectInputStream(input);
				cargarClientes(in);
				cargarCaract(in);
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
				throw new GclienLoadException();
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Carga las caracteristicas del contrato
	 * 
	 * @param in
	 *            Stream de objetos en el que cargar
	 * @throws IOException
	 *             Excepcion de entrada-salida
	 * @throws ClassNotFoundException
	 *             Excepcion que salta cuando no se encuentra una clase
	 */
	private void cargarCaract(ObjectInputStream in) throws ClassNotFoundException, IOException {
		this.cNormal = (CaracContrato) in.readObject();
		this.cPremium = (CaracContrato) in.readObject();
	}

	/**
	 * Carga los clientes desde el input stream
	 * 
	 * @param in
	 *            Stream de objetos en el que cargar
	 * @throws IOException
	 *             Excepcion de entrada-salida
	 * @throws ClassNotFoundException
	 *             Excepcion que salta cuando no se encuentra una clase
	 */
	@SuppressWarnings("unchecked")
	private void cargarClientes(ObjectInputStream in) throws ClassNotFoundException, IOException {
		this.repoClientes = (HashMap<String, Cliente>) in.readObject();
	}

	/**
	 * Devuelve el precio de un contrato Normal
	 * 
	 * @return El precio del contrato normal
	 */
	public float getPrecioNormal() {
		return this.cNormal.getPrecio();
	}

	/**
	 * Devuelve las subastas gratis del contrato normal
	 * 
	 * @return El numero de subastas del contrato normal
	 */
	public int getSubGratisNormal() {
		return this.cNormal.getSubastaGratis();
	}

	/**
	 * Devuelve el limite inferior para que haga descuento de menudencias
	 * 
	 * @return El limite sobre el que se hace el descuento de menudencias
	 */
	public float getLimInfDescuento() {
		return this.cNormal.getGastoParaDesMen();
	}

	/**
	 * Devuelve el precio de un contrato Premium
	 * 
	 * @return El precio del Premium
	 */
	public float getPrecioPrem() {
		return this.cPremium.getPrecio();
	}

	/**
	 * Filtrado general según uno, ninguno, o varios atributos.
	 * 
	 * @param conContrato
	 *            true si queremos que se filtre solo los clientes con contrato
	 * @param nombre
	 *            Si no esta vacia o a null, busca por ese nombre.
	 * @param apellido
	 *            Si no esta vacia o null, busca por esos apellidos.
	 * @param eMail
	 *            Si no esta vacia o es null, busca por ese email
	 * @return Una coleccion de los resultados del filtrado
	 */
	public Collection<Cliente> filtrado(boolean conContrato, String nombre, String apellido, String eMail) {
		Collection<Cliente> clientes = this.repoClientes.values();
		if (conContrato) {
			clientes = this.clientConContr(clientes);
		}
		if (nombre != null && !nombre.isEmpty()) {
			clientes = this.clientNombre(clientes, nombre);
		}
		if (apellido != null && !apellido.isEmpty()) {
			clientes = this.clientApellido(clientes, apellido);
		}
		if (eMail != null && !eMail.isEmpty()) {
			clientes = this.clientEmail(clientes, eMail);
		}
		return clientes;
	}

	/**
	 * Devuelve el gasto necesario para conseguir descuento de menudencias en
	 * contrato normal
	 * 
	 * @return El gasto que hay que realizar
	 */
	public float getGastoMenudNormal() {
		return this.cNormal.getGastoParaDesMen();
	}

	/**
	 * Devuelve el descuento de envío que el contrato premium recibe
	 * 
	 * @return El descuento de envio
	 */
	public int getDescEnvPrem() {
		return this.cPremium.getDescEnvio();
	}

	/**
	 * Modifica un cliente
	 * 
	 * @param cliente
	 *            A modificar
	 * @param tlf
	 *            Telefono del cliente OBLIGATORIO.
	 * @param nombre
	 *            Nombre del cliente OBLIGATORIO.
	 * @param primerApellido
	 *            Primer apellido del cliente.
	 * @param segundoApellido
	 *            Segundo apellido del cliente.
	 * @param nif
	 *            NIF del cliente.
	 * @param dnia
	 *            DNI o NIA del cliente.
	 * @param movil
	 *            Telefono movil del cliente.
	 * @param eMail
	 *            Correo del cliente NECESARIO PARA CONTRATOS.
	 * @param direccion
	 *            Direccion de el cliente.
	 * @param ciudad
	 *            Ciudad en la que reside el cliente.
	 * @param provincia
	 *            Provincia en la que vive el cliente.
	 * @param pais
	 *            Pais en el que vive el cliente.
	 * @param cp
	 *            Codigo Postal del cliente.
	 * @param empresa
	 *            Empresa para la que trabaja el cliente (por si pide factura a
	 *            nombre de empresa).
	 * @return
	 */
	public int modCliente(Cliente cliente, String tlf, String nombre, String primerApellido, String segundoApellido,
			String nif, String dnia, String movil, String eMail, String direccion, String ciudad, String provincia,
			String pais, String cp, String empresa) {
		int contador = 0;
		if (tlf != null && !tlf.isEmpty()) {
			this.repoClientes.remove(cliente.getTlf());
			cliente.setTlf(tlf);
			this.repoClientes.put(cliente.getTlf(), cliente);
			contador++;
		}
		if (nombre != null && !nombre.isEmpty()) {
			cliente.setNombre(nombre);
			contador++;
		}
		if (primerApellido != null && !primerApellido.isEmpty()) {
			cliente.setPrimerApellido(primerApellido);
			contador++;
		}
		if (segundoApellido != null && !segundoApellido.isEmpty()) {
			cliente.setSegundoApellido(segundoApellido);
			contador++;
		}
		if (nif != null && !nif.isEmpty()) {
			cliente.setNif(nif);
			contador++;
		}
		if (dnia != null && !dnia.isEmpty()) {
			cliente.setDnia(dnia);
			contador++;
		}
		if (movil != null && !movil.isEmpty()) {
			cliente.setMovil(movil);
			contador++;
		}
		if (direccion != null && !direccion.isEmpty()) {
			cliente.setDireccion(direccion);
			contador++;
		}
		if (pais != null && !pais.isEmpty()) {
			cliente.setPais(pais);
			contador++;
		}
		if (eMail != null && !eMail.isEmpty()) {
			cliente.seteMail(eMail);
			contador++;
		}
		if (ciudad != null && !ciudad.isEmpty()) {
			cliente.setCiudad(ciudad);
			contador++;
		}
		if (cp != null && !cp.isEmpty()) {
			cliente.setCp(cp);
			contador++;
		}
		if (empresa != null && !empresa.isEmpty()) {
			cliente.setEmpresa(empresa);
			contador++;
		}
		if (provincia != null && !provincia.isEmpty()) {
			cliente.setProvincia(provincia);
			contador++;
		}
		return contador;
	}

	/**
	 * Devuelve el descuento que se aplica a articulos voluminosos en caso para
	 * un cliente determinado.
	 * 
	 * @param cliente
	 *            Cliente del que se quiere saber el descuento.
	 * @return El descuento sobre envio de voluminosos aplicable.
	 */
	public int getPorcenEnv(Cliente cliente) {
		if (cliente.poseeContrato()) {
			if (cliente.getContrato() instanceof Premium) {
				return this.cPremium.getDescEnvio();
			} else if (cliente.getContrato() instanceof Normal) {
				return this.cNormal.getDescEnvio();
			}
		}
		return 0;
	}

	/**
	 * Devuelve si hay que aplicar el descuento sobre menudencias
	 * 
	 * @param cliente
	 *            Cliente sobre el que queremos comprobar si hay que aplicar el
	 *            descuento
	 * @return true si hay que aplicarlo, false si no
	 * @throws GclienException
	 */
	public boolean getAplicarDescuento(Cliente cliente) throws GclienException {
		CaracContrato caract = null;
		Contrato contract = cliente.getContrato();
		if (cliente.poseeContrato()) {
			if (cliente.getContrato() instanceof Normal) {
				caract = this.cNormal;
				contract = (Normal) cliente.getContrato();
			} else if (contract instanceof Premium) {
				return true;
			}
			if (caract == null)
				throw new GclienException();
			if (caract.getGastoParaDesMen() == -1) {
				return true;
			} else if (caract.getGastoParaDesMen() >= ((Normal) contract).getGastoVentas()) {
				return true;
			}
		}
		return false;
	}
}