package es.uam.clavi.model.gclien;

import java.io.Serializable;

/**
 * Clase que guarda la informacion del cliente
 * @author Jesualdo Garcia Lopez
 */
public final class Cliente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2509457799626437082L;
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String nif;
	private String dnia;
	private String movil;
	private String tlf;
	private String eMail;
	private String direccion;
	private String ciudad;
	private String provincia;
	private String pais;
	private String cp;
	private String empresa;
	private Contrato contrato;

	/**
	 * Constructor del cliente con los datos minimos
	 * @param tlf Telefono del nuevo cliente
	 * @param nombre Nombre del nuevo cliente
	 */
	public Cliente(String tlf, String nombre){
		this.nombre = nombre;
		this.tlf = tlf;
	}
	/**
	 * Crea un objeto Cliente con todos los datos
	 * @param tlf El telefono del cliente NECESARIO
	 * @param nombre El nombre del cliente NECESARIO
	 * @param primerApellido El primer apellido del cliente
	 * @param segundoApellido El segundo apellido del cliente
	 * @param nif El NIF de la empresa del cliente
	 * @param dnia El DNI o NIA del cliente
	 * @param movil El tlf movil del cliente
	 * @param eMail El correo electronico del cliente NECESARIO PARA CONTRATO
	 * @param direccion La direccion de domicilio del cliente
	 * @param ciudad La ciudad de residencia del cliente
	 * @param provincia La provincia donde vive el cliente
	 * @param pais El pais del cliente
	 * @param cp El Codigo Postal del cliente
	 * @param empresa La empresa del cliente
	 */
	public Cliente(String tlf, String nombre, String primerApellido, String segundoApellido,
			String nif,	String dnia, String movil, String eMail, String direccion,String ciudad,
			String provincia, String pais, String cp, String empresa){
		this(tlf, nombre);
		this.setPrimerApellido(primerApellido);
		this.setSegundoApellido(segundoApellido);
		this.setNif(nif);
		this.setDnia(dnia);
		this.setMovil(movil);
		this.seteMail(eMail);
		this.setDireccion(direccion);
		this.setCiudad(ciudad);
		this.setProvincia(provincia);
		this.setPais(pais);
		this.setCp(cp);
		this.setEmpresa(empresa);
	}
	
	/**
	 * Devuelve el nombre del cliente
	 * @return El nombre del cliente
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Cambia el nombre del cliente
	 * @param nombre El nuevo nombre del cliente
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Devuelve el primer apellido del cliente
	 * @return El primer apellido del cliente
	 */
	public String getPrimerApellido() {
		return primerApellido;
	}
	
	
	/**
	 * Coloca un nuevo primer apellido de el cliente
	 * @param primerApellido El primerApellido a colocar
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	
	/**
	 * Devuelve el segundo apellido del cliente
	 * @return El segundo apellido del cliente
	 */
	public String getSegundoApellido() {
		return segundoApellido;
	}
	
	/**
	 * Coloca un nuevo segundo apellido en el cliente
	 * @param segundoApellido El segundo apellido a colocar
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	
	/**
	 * Devuelve el NIF del cliente
	 * @return El nif del cliente
	 */
	public String getNif() {
		return nif;
	}
	/**
	 * Coloca un nuevo NIF en el cliente
	 * @param nif El nif a colocar
	 */
	public void setNif(String nif) {
		this.nif = nif;
	}
	/**
	 * Devuelve el DNI o NIA del cliente
	 * @return el DNI o NIA
	 */
	public String getDnia() {
		return dnia;
	}
	/**
	 * Coloca un nuevo DNI o NIA en el cliente
	 * @param dnia El DNI o NIA a colocar
	 */
	public void setDnia(String dnia) {
		this.dnia = dnia;
	}
	/**
	 * Devuelve el numero telefono movil del cliente
	 * @return El numero de movil
	 */
	public String getMovil() {
		return movil;
	}
	/**
	 * Coloca un nuevo tlf de movil en el cliente
	 * @param movil el movil a colocar
	 */
	public void setMovil(String movil) {
		this.movil = movil;
	}
	/**
	 * Devuelve el tlf estandar del cliente
	 * @return el tlf
	 */
	public String getTlf() {
		return tlf;
	}
	/**
	 * Coloca un nuevo tlf
	 * @param tlf el tlf a colocar
	 */
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	/**
	 * Devuelve el eMail del cliente
	 * @return el eMail
	 */
	public String geteMail() {
		return eMail;
	}
	/**
	 * Coloca un nuevo eMail en el cliente
	 * @param eMail el eMail a colocar
	 */
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	/**
	 * Devuelve la direccion del cliente
	 * @return la direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * Coloca una nueva direccion de cliente
	 * @param direccion la direccion a colocar
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * Devuelve la ciudad del cliente
	 * @return la ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * Coloca una nueva ciudad del cliente
	 * @param ciudad la ciudad a colocar
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * Devuelve la provincia del cliente
	 * @return la provincia
	 */
	public String getProvincia() {
		return provincia;
	}
	/**
	 * Coloca una nueva provincia en el cliente
	 * @param provincia la provincia a colocar
	 */
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	/**
	 * Devuelve el pais de el cliente
	 * @return el pais de el cliente
	 */
	public String getPais() {
		return pais;
	}
	/**
	 * Coloca el nuevo pais del cliente
	 * @param pais el pais a colocar
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	/**
	 * Devuelve el codigo postal del cliente
	 * @return el CP
	 */
	public String getCp() {
		return cp;
	}
	/**
	 * Coloca un nuevo CP en el cliente
	 * @param cp el CP a colocar
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}
	/**
	 * Devuelve la empresa de un cliente
	 * @return la empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	/**
	 * Coloca la nueva empresa de un cliente
	 * @param empresa la empresa a colocar
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	/**
	 * Devuelve el contrato de un cliente, o null si no tiene
	 * @return El contrato
	 */
	public Contrato getContrato() {
		return contrato;
	}
	/**
	 * Anyade un nuevo contrato a un cliente SIEMPRE QUE ESTE TENGA eMail
	 * @param contrato El contrato del cliente
	 */
	public void setContrato(Contrato contrato) {
		if (!this.eMail.isEmpty())
			this.contrato = contrato;
	}
	/**
	 * Devuelve si el cliente posee contrato o no
	 * @return true si posee contrato, false si no
	 */
	public boolean poseeContrato(){
		if ((this.contrato != null) && (!this.contrato.caducado())){
			return true;
		}else {
			return false;
		}
	}
}
