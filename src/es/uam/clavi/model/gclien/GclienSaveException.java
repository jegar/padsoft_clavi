package es.uam.clavi.model.gclien;

/**
 * Excepcion de guardado del Gestor de Clientes.
 * Salta cuando se producen errores de guardado.
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public class GclienSaveException extends GclienException {

	private static final long serialVersionUID = -1835098401259278882L;

}
