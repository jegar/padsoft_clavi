package es.uam.clavi.model.gclien;

/**
 * Salta cuando el contrato no se encuentra y deberia estar
 * 
 * @author Jesualdo Garcia Lopez
 *
 */
public class ContractNotFoundException extends Exception{
	private static final long serialVersionUID = -6348512396796046680L;
}
