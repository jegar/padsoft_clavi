package es.uam.clavi.model.gart.trans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gclien.Cliente;

/**
 * Clase que engloba todas las características comunes de los dos tipos de transacciones
 * del gestor. La clase no cumple ningún papel específico aparte de encapsularlas.
 * @author Miguel Laseca
 * @version 2.0
 */
public abstract class Transaccion implements Serializable {

	private static final long serialVersionUID = -4047820492921929389L;
	protected Date fechaCreacion;
	protected GestorArticulos gart;
	private long id;
	
	/**
	 * Constructor, asigna al objeto la fecha de su creacion
	 * y una ID.
	 * @param id El ID que asignaremos.
	 */
	protected Transaccion (long id, GestorArticulos gestor) {
		fechaCreacion = Calendar.getInstance().getTime();
		this.id = id;
		this.gart = gestor;
	}
	
	/**
	 * @return El cliente de una venta o el ultimo pujante de una subasta.
	 */
	abstract Cliente getCl();
	
	/**
	 * @return La fecha en la que se creó la transacción.
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @return El ID del objeto.
	 */
	public long getId(){
		return id;
	}

	/**
	 * @param fechaCreacion La fecha de creación a asignar.
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * Le asigna una nueva ID al objeto.
	 * @param newId Nueva ID que se le asigna al objeto.
	 */
	public void setId(long newId){
		id = newId;
	}
}
