package es.uam.clavi.model.gart.trans;

/**
 * Se lanza siempre que una transacción o lote no tiene acceso a un artículo concreto.
 * @author Miguel Laseca.
 * @version 1.0
 */
public class NotValidArtException extends Exception {
	private static final long serialVersionUID = 3534477778037449208L;
}
