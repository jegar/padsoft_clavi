package es.uam.clavi.model.gart.trans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.*;
import es.uam.clavi.model.gclien.Cliente;

/**
 * 
 * @author Miguel Laseca
 * @version 1.4
 */
public class Venta extends Transaccion {
	
	private static final long serialVersionUID = -406565039717908294L;
	private ArrayList<Articulo> lista;
	private Cliente cl;
	
	/**
	 * Constructor de las ventas. Inicializa el cliente y el ID 
	 * y crea una lista vacía de la compra.
	 * @param id El ID de la venta.
	 * @param cl El cliente al que se le realiza la venta.
	 */
	public Venta (long id, Cliente cl, GestorArticulos gestor) {

		super (id, gestor);
		lista = new ArrayList<Articulo> ();
		this.cl = cl;
	}
	
	/**
	 * Añade un artículo a la lista de una venta.
	 * @param art El artículo a insertar.
	 * @return true en caso de exito, false si no.
	 */
	public boolean anhArticulo (Articulo art) throws NotValidArtException {
		
		if (art.getEst() != Estado.Vendible && art.getEst() != Estado.Ambos) throw new NotValidArtException ();
		art.setEst(Estado.EnVenta);
		boolean ret = lista.add (art);
		if (ret == false) {
			
			Estado e = (art instanceof Arte) ? Estado.Ambos : Estado.Vendible;
			art.setEst(e);
		}
		if (gart.actArticulo(art) == null) return false;
		
		return ret;
	}
	
	/**
	 * Elimina un artículo concreto de la lista de la compra.
	 * @param index La posición que ocupa el artículo a eliminar.
	 * @return true en caso de éxito, false si no.
	 * @throws SoldArtException En caso de que el artículo esté vendido, 
	 * es decir, que ya se cerró la compra.
	 */
	public boolean eliArticulo (Articulo art) throws SoldArtException {
		
		if (art == null) throw new NullPointerException ();
		if (art.getEst() != Estado.EnVenta) throw new SoldArtException ();
		boolean ret = (lista.remove(art));
		Estado e = (art instanceof Arte) ? Estado.Ambos : Estado.Vendible;
		art.setEst(e);
		gart.actArticulo(art);
		
		return ret;
	}
	
	/**
	 * Elimina un artículo por su posición en la lista de la compra.
	 * @param index La posición que ocupa el artículo a eliminar.
	 * @return true en caso de exito, false si no.
	 * @throws SoldArtException En caso de que el artículo esté vendido, 
	 * es decir, que ya se cerró la compra.
	 */
	public boolean eliArticulo (int index) throws SoldArtException {
		
		return this.eliArticulo(lista.get(index));
	}

	/**
	 * @return La lista de artículos de la venta.
	 */
	public ArrayList<Articulo> getLista() {
		return lista;
	}

	/**
	 * @return El cliente que realiza la compra.
	 */
	public Cliente getCl() {
		return cl;
	}

	/**
	 * @param lista La lista de artículos a asignar.
	 */
	public void setLista(ArrayList<Articulo> lista) {
		this.lista = lista;
	}
	
	public String toString () {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return ("V;" + super.getId() + ";" + this.cl + ";" + this.lista + ";" + sdf.format(super.getFechaCreacion()));
	}

	/**
	 * @param cl El cliente a asignar.
	 */
	public void setCl(Cliente cl) {
		this.cl = cl;
	}
}
