package es.uam.clavi.model.gart.trans;

/**
 * Se lanza cuando se intenta modificar un artículo ya vendido.
 * @author Miguel Laseca
 * @version 1.0
 */
public class SoldArtException extends Exception {
	private static final long serialVersionUID = 7691741693175443331L;

}
