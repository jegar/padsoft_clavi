package es.uam.clavi.model.gart.trans;

import java.io.Serializable;

import es.uam.clavi.model.gclien.Cliente;

/**
 * Las pujas de cada subasta.
 * @author Miguel Laseca
 * @version 1.2
 */
public class Puja implements Serializable {
	
	private static final long serialVersionUID = -410620799068258937L;
	private float cantidad;
	private Cliente cl;
	
	public Puja (float cantidad, Cliente cl) {
		
		this.cantidad = cantidad;
		this.cl = cl;
	}
	
	/**
	 * @return La cantidad de una puja.
	 */
	public float getCantidad() {
		return cantidad;
	}

	/**
	 * @return El pujante.
	 */
	public Cliente getCl() {
		return cl;
	}

	/**
	 * @param cantidad La oferta de la puja.
	 */
	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @param cl El cliente a asignar.
	 */
	public void setCl(Cliente cl) {
		this.cl = cl;
	}
	
	@Override
	public String toString () {
		return this.cantidad + ";" + this.cl;
	}
	
	/**
	 * Valida una puja para asignarse a una subasta.
	 * @param cantidad La cantidad a comprobar.
	 * @return true si la puja es válida, false si no.
	 */
	public boolean validarPuja (float cantidad) {
		
		return ((float)(this.cantidad + 0.05) <= cantidad);
	}
}
