package es.uam.clavi.model.gart.trans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.*;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.eps.padsof.emailconnection.*;

/**
 * 
 * @author Miguel Laseca
 * @version 2.3
 */
public class Subasta extends Transaccion {

	private static final long serialVersionUID = 2017111579250063316L;
	private Puja puja;
	private float entrada;
	private Date fechaFin;
	private String mensaje;
	private int duracion;
	private HashMap<String, Cliente> clientes;
	private Articulo art;
	private boolean iniciada;
	
	/**
	 * Constructor de las subastas.
	 * @param id Identificador de la subasta.
	 * @param entrada Valor del precio de entrada.
	 * @param mensaje Mensaje para las notificaciones.
	 * @param duracion Duración en días de la subasta una vez
	 * se haya pujado por primera vez.
	 * @param art Artículo a subastar.
	 */
	public Subasta (long id, float entrada, String mensaje, int duracion, Articulo art, GestorArticulos gestor) {
		
		super (id, gestor);
		this.puja = new Puja(art.getPrecioObjetivo(), null);
		this.entrada = entrada;
		this.mensaje = mensaje;
		this.duracion = duracion;
		this.clientes = new HashMap<String, Cliente> ();
		this.art = art;
		this.art.setEst(Estado.EnSubasta);
		this.iniciada = false;
	}

	/**
	 * Añade una nueva puja a la subasta, siempre cuando sea válida.
	 * @param cl El nuevo pujante.
	 * @param cantidad La cantidad de su oferta.
	 * @return true si la puja ha sido aceptada, false si no.
	 * @throws FailedInternetConnectionException 
	 * @throws InvalidEmailAddressException 
	 */
	public boolean anh (Cliente cl, float cantidad) throws InvalidEmailAddressException, FailedInternetConnectionException {
		
		if (puja.validarPuja(cantidad)) {
			
			this.puja.setCantidad(cantidad);
			this.puja.setCl(cl);
			if (this.clientes.containsKey(cl.getTlf()) == false) {
				
				this.clientes.put(cl.getTlf(), cl);
			}
			if (this.iniciada == false) {
				
				this.iniciada = true;
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, this.duracion);
				fechaFin = c.getTime();
			}
			if (this.ultimoDia() == false) return notificar();
			return true;
		}
		
		return false;
	}

	/**
	 * Añade un cliente a la suscripción de la subasta.
	 * @param cl El cliente a añadir.
	 * @return true si el mapa contiene al cliente, ya sea porque ya estaba o porque lo añadió
	 * exitosamente; false en caso de que no se pudo insertar.
	 */
	public boolean anhSuscripcion (Cliente cl) {
		
		if (this.clientes.containsKey(cl.getTlf()) == false) {
			
			return (this.clientes.put(cl.getTlf(), cl) != null);
		}
		
		return true;
	}

	/**
	 * Elimina una suscripción a una subasta.
	 * @param cl Cliente a desuscribir.
	 * @return true si el cliente estaba en el mapa, false en caso contrario.
	 */
	public boolean eliSuscripcion (Cliente cl) {
		
		return (clientes.remove(cl.getTlf()) != null);
	}

	/**
	 * @return El artículo subastado.
	 */
	public Articulo getArt () {
		return art;
	}
	
	@Override
	public Cliente getCl() {
		return puja.getCl();
	}

	/**
	 * @return Los clientes suscritos a las notificaciones de una subasta.
	 */
	public HashMap<String, Cliente> getClientes() {
		return clientes;
	}
	
	/**
	 * Devuelve la cantidad mínima necesaria para que la puja sea válida.
	 * @return 
	 */
	public float getMinimo () {
		return (float) (puja.getCantidad() + 0.05);
	}

	/**
	 * @return La duración de la subasta.
	 */
	public int getDuracion() {
		return duracion;
	}
	
	/**
	 * @return El precio de la entrada a la subasta.
	 */
	public float getEntrada() {
		return entrada;
	}
	
	/**
	 * @return La fecha de finalización de la subasta.
	 */
	public Date getFechaFin() {
		return fechaFin;
	}

	/**
	 * @return El mensaje de notificación de la subasta.
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @return La última puja realizada.
	 */
	public Puja getPuja() {
		return puja;
	}

	/**
	 * @return El estado de la subasta, por si se puede o no cancelar.
	 */
	public boolean isIniciada() {
		return iniciada;
	}

	/**
	 * Realiza las notificaciones, falta implementar.
	 * @return true si se pudo emviar correctamente a todas las direcciones,
	 * false en cualquier otro caso.
	 * @throws FailedInternetConnectionException 
	 * @throws InvalidEmailAddressException 
	 */
	public boolean notificar () throws InvalidEmailAddressException, FailedInternetConnectionException {
		
		ArrayList<Cliente> list = new ArrayList<Cliente> (this.clientes.values());
		for (Cliente cl : list) {
			
			if (EmailSystem.isValidEmailAddr (cl.geteMail()) == false) throw new InvalidEmailAddressException ("El email no es correcto.");
			EmailSystem.send(cl.geteMail(),
						"Subasta nº " + super.getId() +
						" del día "+ super.getFechaCreacion() + ".\n",
						this.mensaje, true);
		}
		
		return true;
	}
	
	/**
	 * Notifica a una lista de clientes pasada como argumento.
	 * @param list Lista de los clientes.
	 * @return true si se pudo notificar a TODOS los clientes, false en caso contrario.
	 * @throws InvalidEmailAddressException
	 * @throws FailedInternetConnectionException
	 */
	public boolean notificar (ArrayList<Cliente> list) throws InvalidEmailAddressException, FailedInternetConnectionException {
		for (Cliente cl : list) {
			if (!EmailSystem.isValidEmailAddr (cl.geteMail())) throw new InvalidEmailAddressException ("El email no es correcto.");
			EmailSystem.send(cl.geteMail(),
						"Subasta nº " + super.getId() +
						" del día "+ super.getFechaCreacion() + ".\n",
						this.mensaje, true);
		}
		return true;
	}
	
	/**
	 * @param art El artículo a asignar.
	 */
	public boolean setArt (Articulo art) {
		
		if (art == null) return false;
		if (art.getEst() != Estado.Subastable && art.getEst() != Estado.Ambos) return false;
		art.setEst(Estado.EnSubasta);
		this.art = art;
		
		return (gart.actArticulo(art) != null);
	}
	
	/**
	 * @param clientes El mapa de clientes a asignar.
	 */
	public void setClientes(HashMap<String, Cliente> clientes) {
		this.clientes = clientes;
	}
	
	/**
	 * @param duracion La duración a asignar.
	 */
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	/**
	 * @param entrada El precio de entrada a asignar.
	 */
	public void setEntrada(float entrada) {
		this.entrada = entrada;
	}
	
	/**
	 * @param mensaje El mensaje a asignar.
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String toString () {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		String s = "S;" + super.getId() + ";" + this.mensaje + ";" + this.art + ";" + 
			this.duracion + ";" + String.format (java.util.Locale.US,"%.2f", this.entrada)
			+ ";" + ((this.iniciada == true) ? "S":"N" + ";" + 
			this.clientes + ";" + sdf.format(super.getFechaCreacion()));
		if (this.iniciada) return s.concat(";" + sdf.format(this.fechaFin) + ";" + this.puja);
		return s;
	}
	
	/**
	 * Devuelve si es el ultimo dia de una subasta. Se considera que
	 * es el último día si la fecha dd/mm/yyyy coincide con la de hoy.
	 * @return true si es el último día de la subasta, false si no lo es.
	 */
	public boolean ultimoDia () {
		
		DateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
		
		if (isIniciada() == false) return false; 
		return sdf.format(fechaFin).equals(sdf.format(Calendar.getInstance().getTime()));
	}
}
