package es.uam.clavi.model.gart;

/**
 * Excepción para los cargados fallidos del gestor de artículos, sólo se llamará desde
 * el constructor de objetos.
 * @author Miguel Laseca
 * @version 1.0
 */
public class GartLoadException extends Exception {
	private static final long serialVersionUID = 3145575655018550030L;
}
