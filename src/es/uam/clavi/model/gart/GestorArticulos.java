package es.uam.clavi.model.gart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import es.uam.clavi.model.gart.art.Arte;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.model.gart.art.Lote;
import es.uam.clavi.model.gart.art.MalformedArtException;
import es.uam.clavi.model.gart.art.Menudencia;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.model.gart.art.Voluminoso;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gart.trans.Transaccion;
import es.uam.clavi.model.gart.trans.Venta;
import es.uam.clavi.model.gclien.Cliente;

/**
 * Gestor general de artículos y transacciones.
 * 
 * @author Miguel Laseca
 * @version 3.5
 */
public class GestorArticulos implements Serializable {

	private static final long serialVersionUID = -4585071262368526182L;

	public static enum Criterios {
		DATACION, FADQ, PRECIOm, PRECIOM, CERTIFICADO, TIPO, ESTADO
	}

	private static HashMap<Long, Subasta> repoSubastas;
	private static HashMap<Long, Venta> repoVentas;

	private static HashMap<Long, Transaccion> repoFin;
	private static HashMap<Long, Articulo> repoArticulos;
	private static String newArtPath;
	private static String artF = "art.clvi";
	private static String finF = "fin.clvi";
	private static String subF = "sub.clvi";
	private static String venF = "ven.clvi";
	private static String path_to_file;

	/**
	 * Constructor del gestor para dar la opción de usarlo como un objeto
	 * cualquiera.
	 * 
	 * @param path
	 *            Dirección de dónde cargar todos los archivos.
	 * @throws GartLoadException
	 *             En caso de que el cargado falle.
	 */
	public GestorArticulos(String path) throws GartLoadException {
		path_to_file = path + File.separator + "gart" + File.separator;
		try {
			if (!iniGestor(path_to_file)) {
				repoSubastas = new HashMap<Long, Subasta>();
				repoVentas = new HashMap<Long, Venta>();
				repoFin = new HashMap<Long, Transaccion>();
				repoArticulos = new HashMap<Long, Articulo>();
				anhArticulos("art_nuevos.txt");
			}
		} catch (ClassNotFoundException | IOException | MalformedArtException | ParseException e) {
			e.printStackTrace();
			throw new GartLoadException();
		}
	}

	/**
	 * Actualiza un artículo en el mapa, para garantizar que no se pierdan sus
	 * cambios.
	 * 
	 * @param art
	 *            El artículo a insertar.
	 * @return El artículo insertado, null si es la primera vez que se inserta
	 *         en el mapa o si surge un error.
	 */
	public Articulo actArticulo(Articulo art) {

		if (repoArticulos.put(art.getID(), art) == null)
			return null;

		return art;
	}

	/**
	 * Anhade una obra de arte al repositorio de articulos a traves de sus
	 * atributos. El ID se asigna
	 * 
	 * @param descr
	 *            Descripcion del articulo
	 * @param fecha
	 *            Fecha de adquisicion del articulo
	 * @param p1
	 *            Precio de adquisicion del articulo
	 * @param p2
	 *            Precio objetivo
	 * @param tipoObra
	 *            Tipo de obra (Pintura, escultura, grabado,...)
	 * @param c
	 *            Posesion o no de certificado
	 * @param autor
	 *            Autor de la obra
	 * @param f
	 *            Fecha de datacion de la obra
	 * @return la obra en formato de articulo, una vez dentro del hashmap
	 * @throws MalformedArtException
	 */
	public Articulo anhArte(String descr, Date fecha, float p1, float p2, TipoObra tipoObra, boolean c, String autor,
			String f) throws MalformedArtException {

		Articulo art = new Arte(repoArticulos.size() + 1, descr, fecha, p1, p2, tipoObra, c, autor, f, this);

		repoArticulos.put(art.getID(), art);
		
		return art;
	}

	/**
	 * Añade un artículo previamente creado a la repo, reasignándole el ID.
	 * 
	 * @param art
	 *            el articulo a añadir.
	 * @return el objeto artículo dentro del hashmap, null en caso de error.
	 * @throws NullPointerException
	 *             cuando art == null.
	 */
	public Articulo anhArticulo(Articulo art) throws NullPointerException {

		if (art == null)
			throw (new NullPointerException());
		art.setID(repoArticulos.size() + 1);
		return repoArticulos.put(art.getID(), art);
	}

	/**
	 * Anhade a la repo una coleccion de articulos leido desde un fichero de
	 * texto. No acepta lotes
	 * 
	 * @param path
	 *            el fichero del que vamos a leer
	 * @return el numero de elementos que actualmente tiene la repo
	 * @throws MalformedArtException
	 *             cuando los atos están corruptos.
	 * @throws IOException
	 *             ante cualquier error del fichero.
	 * @throws ParseException
	 *             cuando alguna fecha está mal introducida en el fichero.
	 */
	public int anhArticulos(String path) throws MalformedArtException, IOException, ParseException {

		newArtPath = path;
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		archivo = new File(path);
		fr = new FileReader(archivo);

		br = new BufferedReader(fr);

		// Lectura del fichero
		String linea;
		while ((linea = br.readLine()) != null) {

			Long ID = (long) (repoArticulos.size() + 1); // Asigno el ID de 1 en
															// 1 empezando por
															// el 1
			StringTokenizer st = new StringTokenizer(linea, ";");
			char tipo = st.nextToken().charAt(0);
			String descripcion = st.nextToken();
			String fechaDatacion = st.nextToken();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaAdquisicion = format.parse(st.nextToken());
			float precioAdquisicion = Float.parseFloat(st.nextToken());
			float precioObjetivo = Float.parseFloat(st.nextToken());

			switch (tipo) {
			case 'M':
				int descuento = Integer.parseInt(st.nextToken());
				repoArticulos.put(ID, new Menudencia(ID, descripcion, fechaAdquisicion, precioAdquisicion,
						precioObjetivo, descuento, fechaDatacion, this));
				break;
			case 'V':
				int peso = Integer.parseInt(st.nextToken());
				int alto = Integer.parseInt(st.nextToken());
				int ancho = Integer.parseInt(st.nextToken());
				int largo = Integer.parseInt(st.nextToken());
				repoArticulos.put(ID, new Voluminoso(ID, descripcion, fechaAdquisicion, precioAdquisicion,
						precioObjetivo, peso, alto, ancho, largo, fechaDatacion, this));
				break;
			default:
				String autor = st.nextToken();
				repoArticulos.put(ID,
						new Arte(ID, descripcion, fechaAdquisicion, precioAdquisicion, precioObjetivo,
								TipoObra.valueOf(st.nextToken()), (st.nextToken().charAt(0) == 'S'), autor,
								fechaDatacion, this));
			}
		}
		br.close();

		return repoArticulos.size();
	}

	/**
	 * Añade un lote al repositorio de artículos a través de sus atributos. El
	 * ID se asigna.
	 * 
	 * @param descr
	 *            Descripción del artículo.
	 * @param fecha
	 *            Fecha de adquisición del artículo.
	 * @param p1
	 *            Precio de adquisicion del artículo.
	 * @param p2
	 *            Precio objetivo.
	 * @param f
	 *            Fecha de datacion del conjunto de artículos del lote.
	 * @return el lote en formato de artículo, una vez dentro del hashmap.
	 * @throws MalformedArtException
	 */
	public Articulo anhLote(String descr, Date fecha, float p1, float p2, String f) throws MalformedArtException {

		Articulo art = new Lote(repoArticulos.size() + 1, descr, fecha, p1, p2, f, this);
		repoArticulos.put(art.getID(), art);
		return art;
	}

	/**
	 * Crea un lote para un conjunto de artículos pasados como argumento en un
	 * ArrayList.
	 * 
	 * @param descr
	 *            Descripción del artículo.
	 * @param fecha
	 *            Fecha de adquisición del artículo.
	 * @param p1
	 *            Precio de adquisicion del artículo.
	 * @param p2
	 *            Precio objetivo.
	 * @param f
	 *            Fecha de datacion del conjunto de artículos del lote.
	 * @param list
	 *            Lista con los artículos del lote.
	 * @return el lote en formato de artículo, una vez dentro del hashmap.
	 * @throws MalformedArtException
	 */
	public Articulo anhLote(String descr, Date fecha, float p1, float p2, String f,
			ArrayList<es.uam.clavi.model.gart.art.Articulo> list) throws MalformedArtException {

		Articulo art = new Lote(repoArticulos.size() + 1, descr, fecha, p1, p2, f, list, this);
		repoArticulos.put(art.getID(), art);
		return art;
	}

	/**
	 * Anhade una menudencia al repositorio de articulos a traves de sus
	 * atributos. El ID se asigna
	 * 
	 * @param descr
	 *            Descripcion del articulo
	 * @param fecha
	 *            Fecha de adquisicion del articulo
	 * @param p1
	 *            Precio de adquisicion del articulo
	 * @param p2
	 *            Precio objetivo
	 * @param descuento
	 *            descuento base porcentual del articulo
	 * @param f
	 *            Fecha de datacion del articulo
	 * @return la menudencia en formato de articulo, una vez dentro del hashmap
	 * @throws MalformedArtException
	 */
	public Articulo anhMenudencia(String descr, Date fecha, float p1, float p2, int descuento, String f)
			throws MalformedArtException {

		es.uam.clavi.model.gart.art.Articulo art = new Menudencia(repoArticulos.size() + 1, descr, fecha, p1, p2,
				descuento, f, this);
		repoArticulos.put(art.getID(), art);
		return art;
	}

	/**
	 * Anhade un articulo voluminoso al repositorio de articulos a traves de sus
	 * atributos. El ID se asigna
	 * 
	 * @param descr
	 *            Descripcion del articulo
	 * @param fecha
	 *            Fecha de adquisicion del articulo
	 * @param p1
	 *            Precio de adquisicion del articulo
	 * @param p2
	 *            Precio objetivo
	 * @param peso
	 *            Peso del articulo
	 * @param alto
	 *            La altura del articulo
	 * @param ancho
	 *            La anchura del articulo
	 * @param largo
	 *            La longitud del articulo
	 * @param f
	 *            Fecha de datacion del articulo
	 * @return el voluminoso en formato de articulo, una vez dentro del hashmap
	 * @throws MalformedArtException
	 */
	public Articulo anhVoluminoso(String descr, Date fecha, float p1, float p2, int peso, int alto, int ancho,
			int largo, String f) throws MalformedArtException {

		Articulo art = new Voluminoso(repoArticulos.size() + 1, descr, fecha, p1, p2, peso, alto, ancho, largo, f,
				this);
		repoArticulos.put(art.getID(), art);
		return art;
	}

	/**
	 * Devuelve los articulos que tienen certificado de autenticidad
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar
	 * @return Lista de articulos con certificado
	 */
	public ArrayList<Articulo> artCert(Collection<Articulo> coleccion) {
		return filtArticulos(coleccion, Criterios.CERTIFICADO, null);
	}

	/**
	 * Devuelve los articulos que corresponden con una datación
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar
	 * @param fecha
	 *            Una cadena con el criterio de búsqueda
	 * @return Lista de artículos de dicha datación
	 */
	public ArrayList<Articulo> artDat(Collection<Articulo> coleccion, String fecha) {
		return filtArticulos(coleccion, Criterios.DATACION, fecha);
	}

	/**
	 * Filtra los articulos por un estado concreto dado.
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar.
	 * @param est
	 *            Estado por el que filtrar.
	 * @return Lista de articulos en ese estado.
	 */
	public ArrayList<Articulo> artEst(Collection<Articulo> coleccion, es.uam.clavi.model.gart.art.Estado est) {
		return filtArticulos(coleccion, Criterios.ESTADO, est.toString());
	}

	/**
	 * Devuelve los articulos adquiridos en una fecha concreta.
	 * 
	 * @param coleccion
	 *            La colección en la que buscar.
	 * @param fadq
	 *            Fecha de adquisición que filtrar.
	 * @return Lista con los artículos filtrados.
	 */
	public ArrayList<Articulo> artFadq(Collection<Articulo> coleccion, Date fadq) {
		return filtArticulos(coleccion, Criterios.FADQ, (new SimpleDateFormat("dd/MM/yyyy")).format(fadq));
	}

	/**
	 * Devuelve los artículos cuyo precio es igual o menor que uno dado.
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar.
	 * @param pr
	 *            El precio.
	 * @return Lista con los artículos filtrados.
	 */
	public ArrayList<Articulo> artPreciom(Collection<Articulo> coleccion, float pr) {
		return filtArticulos(coleccion, Criterios.PRECIOm, String.valueOf(pr));
	}

	/**
	 * Devuelve los artículos cuyo precio es igual o mayor que uno dado.
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar.
	 * @param pr
	 *            El precio.
	 * @return Lista con los artículos filtrados.
	 */
	public ArrayList<Articulo> artPrecioM(Collection<Articulo> coleccion, float pr) {
		return filtArticulos(coleccion, Criterios.PRECIOM, String.valueOf(pr));
	}

	/**
	 * Lista los últimos n artículos según se indique en entrada.
	 * 
	 * @param nart
	 *            Número de artículos que tendrá la lista.
	 * @return La lista organizada.
	 */
	public static ArrayList<Articulo> artRecien(int nart) {

		ArrayList<Transaccion> values = new ArrayList<Transaccion>(repoFin.values());
		ArrayList<Articulo> list = new ArrayList<Articulo>();
		for (int i = repoFin.size() - 1; i > -1 && list.size() < nart; i--) {

			if (values.get(i) instanceof Venta) {

				list.addAll(((Venta) (values.get(i))).getLista());
				while (list.size() > nart)
					list.remove(list.size() - 1);
			} else {

				list.add(((Subasta) values.get(i)).getArt());
			}
		}

		return list;
	}

	/**
	 * Filtra los artículos de una colección por su tipo.
	 * 
	 * @param coleccion
	 *            La coleccion en la que buscar.
	 * @param type
	 *            String con el tipo.
	 * @return Lista con los artículos filtrados.
	 */
	public ArrayList<Articulo> artTipo(Collection<Articulo> coleccion, String type) {
		return filtArticulos(coleccion, Criterios.TIPO, type);
	}

	/**
	 * Busca un articulo especifico a traves de su clave
	 * 
	 * @param id
	 *            el ID del articulo, esta es la clave para los articulos
	 * @return
	 */
	public Articulo buscarArticulo(Long id) {
		return repoArticulos.get(id);
	}

	/**
	 * Busca el lote que contiene a un artículo específico sabiendo la clave de
	 * éste
	 * 
	 * @param id
	 *            el ID del artículo
	 * @return el lote que contiene al artículo cuyo ID sea id
	 */
	public Lote buscarLote(Long id) {
		ArrayList<Articulo> list = filtrarArtTipo("Lote");
		Articulo a = buscarArticulo(id);
		for (Articulo art : list) {
			Lote lote = (Lote) art;
			if (lote.getList().contains(a))
				return lote;
		}

		return null;
	}

	/**
	 * Cancela una subasta pasada como argumento. Sólo cancelará subastas de
	 * artículos contenidos en repoArticulos.
	 * 
	 * @param s
	 *            Subasta a cancelar.
	 * @return true en caso de que se pudiese cancelar, false en otro caso.
	 */
	public boolean cancelarSubasta(Subasta s) {

		if (!repoSubastas.containsValue(s))
			return false;
		if (s.isIniciada())
			return false;
		if (!repoArticulos.containsValue(s.getArt()))
			return false;

		if (s.getArt() instanceof Arte)
			s.getArt().setEst(Estado.Ambos); // Es arte
		else
			s.getArt().setEst(Estado.Subastable); // Es lote
		repoSubastas.remove(s.getId());

		return (actArticulo(s.getArt()) != null);
	}

	/**
	 * Carga el repositorio de artículos de un fichero binario.
	 * 
	 * @return true en caso de que se pudo abrir el fichero, false en caso
	 *         contrario.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static boolean cargarArt(String path) throws IOException, ClassNotFoundException {
		if (new File(path).isFile()) {
			FileInputStream input = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(input);
			repoArticulos = (HashMap<Long, Articulo>) in.readObject();
			in.close();
			input.close();
			return true;
		} else
			return false;
	}

	/**
	 * Carga el repositorio de transacciones finalizadas de un fichero binario.
	 * 
	 * @return true en caso de que se pudo abrir el fichero, false en caso
	 *         contrario.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static boolean cargarFin(String path) throws IOException, ClassNotFoundException {
		if (new File(path).isFile()) {
			FileInputStream input = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(input);
			repoFin = (HashMap<Long, Transaccion>) in.readObject();
			in.close();
			input.close();
			return true;
		} else
			return false;
	}

	/**
	 * Carga el repositorio de subastas de un fichero binario.
	 * 
	 * @return true en caso de que se pudo abrir el fichero, false en caso
	 *         contrario.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static boolean cargarSub(String path) throws IOException, ClassNotFoundException {
		if (new File(path).isFile()) {
			FileInputStream input = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(input);
			repoSubastas = (HashMap<Long, Subasta>) in.readObject();
			in.close();
			input.close();
			return true;
		} else
			return false;
	}

	/**
	 * Carga el repositorio de ventas de un fichero binario.
	 * 
	 * @return true en caso de que se pudo abrir el fichero, false en caso
	 *         contrario.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static boolean cargarVen(String path) throws IOException, ClassNotFoundException {
		if (new File(path).isFile()) {
			FileInputStream input = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(input);
			repoVentas = (HashMap<Long, Venta>) in.readObject();
			in.close();
			input.close();
			return true;
		} else
			return false;
	}

	/**
	 * Funcion privada que elige el criterio por el que se va a filtrar.
	 * 
	 * @param actual
	 *            El elemento a comprobar.
	 * @param criterio
	 *            El criterio por el cual se va a clasificar.
	 * @param comp
	 *            De necesitarlo, aquello con lo que se va a comparar en formato
	 *            String.
	 * @return true si pasa el filtro, false si no.
	 */
	private static boolean eleCriterio(es.uam.clavi.model.gart.art.Articulo actual, Criterios criterio, String comp) {
		switch (criterio) {
		case CERTIFICADO:
			return actual.isCertAutentific();
		case DATACION:
			return comp.equals(actual.getFechaDatacion());
		case ESTADO:
			return comp.equals(actual.getEst().toString());
		case FADQ:
			return (new SimpleDateFormat("dd/MM/yyyy")).format(actual.getFechaAdquisicion()).equals(comp);
		case PRECIOm:
			return Float.parseFloat(comp) >= actual.getPrecioObjetivo();
		case PRECIOM:
			return Float.parseFloat(comp) <= actual.getPrecioObjetivo();
		case TIPO:
			return comp.equals(actual.getTipo());
		default:
			return false;
		}
	}

	/**
	 * Elimina un lote de artículos y reinicializa los estados de sus
	 * componentes.
	 * 
	 * @param l
	 *            Lote
	 * @return true en caso de éxito, false en caso de que el lote fuera
	 *         vendido, en cuyo caso no se modifica.
	 */
	public boolean elLote(es.uam.clavi.model.gart.art.Lote l) {

		if (l.getEst() == Estado.Vendido)
			return false;
		for (Articulo art : l.getList()) {

			switch (art.getTipo()) {

			case "Arte":
				art.setEst(es.uam.clavi.model.gart.art.Estado.Ambos);
				break;
			case "Lote":
				art.setEst(es.uam.clavi.model.gart.art.Estado.Subastable);
			default: // Menudencia y voluminoso
				art.setEst(es.uam.clavi.model.gart.art.Estado.Vendible);
			}
			actArticulo(art);
		}
		l.getList().clear();
		repoArticulos.remove(l.getID());

		return true;
	}

	/**
	 * Filtra el repoArticulos según un criterio.
	 * 
	 * @param criterio
	 *            El criterio por el que se va a filtrar.
	 * @param comp
	 *            Cadena con la que se va a comparar, si aplicable.
	 * @return La lista de los elementos que cumplen la condicion.
	 */
	private static ArrayList<Articulo> filtArticulos(Collection<es.uam.clavi.model.gart.art.Articulo> coleccion,
			Criterios criterio, String comp) {
		ArrayList<es.uam.clavi.model.gart.art.Articulo> tempList = new ArrayList<es.uam.clavi.model.gart.art.Articulo>();
		for (es.uam.clavi.model.gart.art.Articulo actual : coleccion) {
			if (eleCriterio(actual, criterio, comp)) {
				tempList.add(actual);
			}
		}

		return tempList;
	}

	/**
	 * Esta funcion devuelve los articulos que tienen certificado.
	 * 
	 * @return Artículos certificados.
	 */
	public ArrayList<Articulo> filtrarArtCert() {
		return artCert(repoArticulos.values());
	}

	/**
	 * Devuelve los artículos de una misma datación.
	 * 
	 * @param fecha
	 *            String de la época/fecha.
	 * @return Artículos de la datación.
	 */
	public ArrayList<Articulo> filtrarArtDat(String fecha) {
		return artDat(repoArticulos.values(), fecha);
	}

	/**
	 * Devuelve los artículos que se encuentran en un estado pasado como
	 * argumento.
	 * 
	 * @param est
	 *            Estado por el que filtrar.
	 * @return Artículos en dicho estado.
	 */
	public ArrayList<Articulo> filtrarArtEst(Estado est) {
		return artEst(repoArticulos.values(), est);
	}

	/**
	 * Devuelve los artículos adquiridos en un día concreto.
	 * 
	 * @param fadq
	 *            Fecha de adquisición.
	 * @return Artículos de dicha fecha.
	 */
	public ArrayList<Articulo> filtrarArtFadq(Date fadq) {
		return artFadq(repoArticulos.values(), fadq);
	}

	/**
	 * Devuelve los artículos con precio menor o igual a una cifra dada.
	 * 
	 * @param pr
	 *            Precio a filtrar.
	 * @return Artículos con precio que cumpla la condición.
	 */
	public ArrayList<Articulo> filtrarArtPreciom(float pr) {
		return artPreciom(repoArticulos.values(), pr);
	}

	/**
	 * Devuelve los artículos con precio mayor o igual a una cifra dada.
	 * 
	 * @param pr
	 *            Precio a filtrar.
	 * @return Artículos con precio que cumpla la condición.
	 */
	public ArrayList<Articulo> filtrarArtPrecioM(float pr) {
		return artPrecioM(repoArticulos.values(), pr);
	}

	/**
	 * Devuelve los artículos de una misma clase.
	 * 
	 * @param type
	 *            Tipo de artículo.
	 * @return Artículos de la clase.
	 */
	public ArrayList<Articulo> filtrarArtTipo(String type) {
		return artTipo(repoArticulos.values(), type);
	}

	/**
	 * Devuelve las transacciones finalizadas de este gestor en función de la
	 * fecha en la que se cerraron.
	 * 
	 * @param fecha
	 *            Fecha a comparar a los milisegundos.
	 * @param mayor
	 *            Su valor varía según se quiera comparar las fechas: si es
	 *            true, se filtrarán las transacciones finalizadas en el mismo
	 *            día o después del día indicado en la fecha.
	 * @return
	 */
	public ArrayList<Transaccion> filtrarFinFecha(long fecha, boolean mayor) {
		ArrayList<Transaccion> list = new ArrayList<Transaccion>();
		for (Long key : repoFin.keySet()) {
			if (mayor) {
				if (key >= fecha)
					list.add(repoFin.get(key));
			} else {
				if (key <= fecha)
					list.add(repoFin.get(key));
				else
					break;
			}
		}
		return list;
	}

	/**
	 * Busca la venta en la que se lista un artículo dentro de repoVentas.
	 * 
	 * @param art
	 *            El artículo a buscar en la repoVentas.
	 * @return La venta que lo contenga. Null en caso de error.
	 */
	public Venta filtrarVenArt(Articulo art) {

		return venArt(repoVentas.values(), art);
	}

	/**
	 * Filtra las ventas dentro de un conjunto realizadas a un cliente concreto.
	 * 
	 * @param cl
	 *            El cliente a filtrar.
	 * @return Las ventas realizadas a ese cliente.
	 */
	public ArrayList<Venta> filtrarVenCl(Cliente cl) {
		return venCl(repoVentas.values(), cl);
	}

	/**
	 * Filtra el repoVentas por su fecha de creación.
	 * 
	 * @param f
	 *            La fecha para filtrar.
	 * @param mayor
	 *            Indica si se filtrará por posterioridad o anterioridad.
	 * @return Lista de ventas que cumplan el criterio.
	 */
	public ArrayList<Venta> filtrarVenFecha(Date f, boolean mayor) {
		return venFecha(repoVentas.values(), f, mayor);
	}

	/**
	 * Busca las subastas que terminan ese día y las finaliza, guardándolas en
	 * la repo de subastas finalizadas. Cambia el estado de los artículos
	 * subastados a "Vendido".
	 */
	public void finalizarSubastas() {

		ArrayList<Subasta> list = new ArrayList<Subasta>(repoSubastas.values());
		for (Subasta s : list) {

			if (s.ultimoDia()) {

				s.getArt().setEst(Estado.Vendido);
				actArticulo(s.getArt());
				repoFin.put(Calendar.getInstance().getTimeInMillis(), repoSubastas.remove(s.getId()));
			}
		}
	}

	/**
	 * Mueve una venta al mapa de ventas finalizadas una vez se haya ultimado la
	 * compra.
	 * 
	 * @param v
	 *            La venta a finalizar.
	 */
	public float finalizarVenta(Venta v, int descuento, boolean aplica) {
		// Revisar validez de este método
		float total = 0;
		for (Articulo art : v.getLista()) {

			art.setEst(Estado.Vendido);
			total += art.getPrecioObjetivo();
			if (art instanceof Menudencia) {
				if (aplica)
					total -= art.getPrecioObjetivo() * ((Menudencia) art).getDescuento() / 100;
			}
			if (art instanceof Voluminoso) {
				total += ((Voluminoso) art).clacularGastosEnv();
				if (aplica)
					total -= ((Voluminoso) art).clacularGastosEnv() * descuento / 100;
			}
			actArticulo(art);
		}
		repoFin.put(Calendar.getInstance().getTimeInMillis(), v);

		return total;
	}

	/**
	 * Finaliza el día, guardando en ficheros binarios dentro de un directorio
	 * pasado como argumento.
	 * 
	 * @param path
	 *            Directorio dónde guardar los ficheros.
	 * @throws IOException
	 */
	public void finDia(String path) throws IOException {
		path = path.concat(File.separator + "gart" + File.separator);
		String s1 = path.concat(artF);
		String s2 = path.concat(finF);
		String s3 = path.concat(subF);
		String s4 = path.concat(venF);
		finDia(s1, s2, s3, s4);
	}

	/**
	 * Finaliza el día, guardando en ficheros pasados como argumento el
	 * contenido de los repos.
	 * 
	 * @param artp
	 *            Fichero de los artículos
	 * @param finp
	 *            Fichero de las transacciones finalizadas.
	 * @param subp
	 *            Fichero de las subastas.
	 * @param venp
	 *            Fichero de las ventas activas.
	 * @throws IOException
	 */
	public void finDia(String artp, String finp, String subp, String venp) throws IOException {

		finalizarSubastas();
		guardarArt(artp);
		guardarFin(finp);
		guardarSub(subp);
		guardarVen(venp);
	}

	/**
	 * @return La ruta del fichero que contiene los últimos artículos añadidos.
	 */
	public String getNewArtPath() {
		return newArtPath;
	}

	/**
	 * Devuelve el HashMap de artículos. Sólo para las pruebas de JUnit.
	 * 
	 * @return el repositorio de artículos.
	 */
	public HashMap<Long, Articulo> getRepoArticulos() {
		return repoArticulos;
	}

	/**
	 * Devuelve el HashMap de transacciones finalizadas.Sólo para las pruebas de
	 * JUnit.
	 * 
	 * @return el repositorio de transacciones finalizadas.
	 */
	public HashMap<Long, Transaccion> getRepoFin() {
		return repoFin;
	}

	/**
	 * Devuelve el HashMap de subastas activas. Sólo para las pruebas de JUnit.
	 * 
	 * @return el repositorio de subastas activas.
	 */
	public HashMap<Long, Subasta> getRepoSubastas() {
		return repoSubastas;
	}

	/**
	 * Guarda el repositorio de artículos en un fichero binario.
	 * 
	 * @throws IOException
	 */
	public void guardarArt(String path) throws IOException {

		File file = new File(path);
		if (!file.isFile() || !file.canWrite()) {
			file.getParentFile().mkdir();
			file.createNewFile();
		}
		FileOutputStream output = new FileOutputStream(file);
		ObjectOutputStream out = new ObjectOutputStream(output);
		out.writeObject(repoArticulos);
		out.close();
		output.close();
	}

	/**
	 * Guarda el repositorio de transacciones finalizadas en un fichero binario.
	 * 
	 * @throws IOException
	 */
	public void guardarFin(String path) throws IOException {

		File file = new File(path);
		if (!file.isFile() || !file.canWrite()) {
			file.getParentFile().mkdir();
			file.createNewFile();
		}
		FileOutputStream output = new FileOutputStream(file);
		ObjectOutputStream out = new ObjectOutputStream(output);
		out.writeObject(repoFin);
		out.close();
		output.close();
	}

	/**
	 * Guarda el repositorio de subastas en un fichero binario.
	 */
	public void guardarSub(String path) throws RuntimeException {

		try {
			File file = new File(path);
			if (!file.isFile() || !file.canWrite()) {
				file.getParentFile().mkdir();
				file.createNewFile();
			}
			FileOutputStream output = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(output);
			out.writeObject(repoSubastas);
			out.close();
			output.close();
		} catch (IOException | SecurityException | NullPointerException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Guarda el repositorio de ventas en un fichero binario.
	 */
	public void guardarVen(String path) throws RuntimeException {

		try {
			File file = new File(path);
			if (!file.isFile() || !file.canWrite()) {
				file.getParentFile().mkdir();
				file.createNewFile();
			}
			FileOutputStream output = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(output);
			out.writeObject(repoVentas);
			out.close();
			output.close();
		} catch (IOException | SecurityException | NullPointerException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Genera un informe con todas las transacciones cerradas en el último mes.
	 * 
	 * @return
	 * @throws ParseException
	 */
	public ArrayList<Transaccion> informeMensual() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		return filtrarFinFecha(sdf.parse(sdf.format(c.getTime())).getTime(), true);
	}

	/**
	 * Inicia una subasta.
	 * 
	 * @param entrada
	 *            Precio de entrada. Siempre que se añade una suscripción se
	 *            comprueba.
	 * @param mensaje
	 *            Mensaje por defecto para las notificaciones.
	 * @param duracion
	 *            Duración de la subasta en días.
	 * @param art
	 *            Artículo a subastar.
	 * @return true si la subasta se creó correctamente.
	 * @throws NotValidArtException
	 *             si el artículo no puede subastarse.
	 */
	public Subasta iniciarSubasta(float entrada, String mensaje, int duracion, Articulo art)
			throws NotValidArtException {

		if (art.getEst() != Estado.Subastable && art.getEst() != Estado.Ambos)
			throw new NotValidArtException();
		Subasta sub = new Subasta((long) repoSubastas.size() + 1, entrada, mensaje, duracion, art, this);
		repoSubastas.put(sub.getId(), sub);

		return sub;
	}

	/**
	 * Inicializa una venta vacía, sin finalizarla. Esta función serviría para
	 * realizar compras sin confirmar o reservas de una serie de artículos.
	 * 
	 * @param c
	 *            el cliente al que se realiza la venta.
	 * @return la venta.
	 */
	public Venta iniciarVenta(Cliente c) {

		HashMap<Long, Venta> repo = repoVentas;
		Venta v = new Venta((long) repo.size() + 1, c, this);
		repoVentas.put(v.getId(), v);

		return v;
	}

	/**
	 * Constructor alternativo del gestor. En vez de los nombres de los ficheros
	 * se utiliza nombres de ficheros binarios por defecto, los cuales se cargan
	 * en el directorio recibido por argumento.
	 * 
	 * @param path
	 *            La dirección relativa del directorio de los ficheros, el cual
	 *            deberá contener los binarios llamados art.clvi, fin.clvi,
	 *            sub.clvi, ven.clvi.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 * @throws MalformedArtException
	 */
	public boolean iniGestor(String path)
			throws ClassNotFoundException, IOException, MalformedArtException, ParseException {
		if (new File(path).isDirectory()) {
			String s1 = path.concat(artF);
			String s2 = path.concat(finF);
			String s3 = path.concat(subF);
			String s4 = path.concat(venF);
			return iniGestor(s1, s2, s3, s4);
		}

		return false;
	}

	/**
	 * Constructor del gestor de artículos. Inicializa sus repositorios con los
	 * valores ubicados en los ficheros enviados como argumentos.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 * @throws MalformedArtException
	 */
	public boolean iniGestor(String artp, String finp, String subp, String venp)
			throws ClassNotFoundException, IOException, MalformedArtException, ParseException {

		repoArticulos = new HashMap<Long, Articulo>();
		repoSubastas = new HashMap<Long, Subasta>();
		repoVentas = new HashMap<Long, Venta>();
		repoFin = new HashMap<Long, Transaccion>();
		newArtPath = null;

		if (!cargarArt(artp) || !cargarFin(finp) || !cargarSub(subp) || !cargarVen(venp)) return false;
		File f = new File ("input"+File.separator);
		File[] matchingFiles = f.listFiles();
		if (matchingFiles == null) return true;
		for (int i = 0; i < matchingFiles.length; i++) {
			anhArticulos(matchingFiles[i].toString());
		}
		
		return true;
	}

	/**
	 * Filtra los artículos del gestor según una serie de criterios pasados como
	 * argumento.
	 * 
	 * @param conCertificado
	 *            Indica si se quieren filtrar artículos con certificado o no,
	 *            de ser así su valor será true; si no se quiere filtrar, su
	 *            valor será false.
	 * @param fDat
	 *            Indica la filtración por fecha de datación. Si vale null o
	 *            está vacía no se activa el filtrado.
	 * @param est
	 *            Activa el filtrado por estado. Si vale null no se filtrará.
	 * @param fadq
	 *            Activa el filtrado de fecha de adquisición. Si vale null no se
	 *            filtrará.
	 * @param precioM
	 *            Se aplica para filtrar los artículos con precio mayor o igual
	 *            a precioM. Sólo se filtra si precioM vale más que 0.
	 * @param preciom
	 *            Lo mismo que precioM, pero para artículos con precio menor o
	 *            igual a preciom.
	 * @param tipo
	 *            Aplica el filtrado por tipo de artículo. Si es null o está
	 *            vacía no se aplicará el filtrado.
	 * @return El conjunto de artículos que cumplen los criterios de filtrado.
	 */
	public Collection<Articulo> filtrado(boolean conCertificado, String fDat, es.uam.clavi.model.gart.art.Estado est,
			Date fadq, float precioM, float preciom, String tipo) {
		Collection<Articulo> c = new ArrayList<Articulo>(repoArticulos.values());
		if (conCertificado) {
			c = artCert(c);
		}
		if (fDat != null && !fDat.isEmpty()) {
			c = artDat(c, fDat);
		}
		if (est != null) {
			c = artEst(c, est);
		}
		if (fadq != null) {
			c = artFadq(c, fadq);
		}
		if (precioM > 0) {
			c = artPrecioM(c, precioM);
		}
		if (preciom > 0) {
			c = artPreciom(c, preciom);
		}
		if (tipo != null && !tipo.isEmpty()) {
			c = artTipo(c, tipo);
		}

		return c;
	}

	/**
	 * Filtra las subastas del repositorio de forma anidada a partir de un
	 * conjunto de parámetros que definen cada uno de los filtrados.
	 * 
	 * @param art
	 *            Artículo para realizar la filtración por artículos, para
	 *            evitarla hay que enviar null en este argumento.
	 * @param cl
	 *            Cliente para filtrar por cliente. Funciona igual que art.
	 * @param fechaM
	 *            Fecha para filtrar por mayor en la fecha de creación. Se elude
	 *            si es null.
	 * @param fecham
	 *            Fecha para filtrar por menor o igual en la fecha de creación.
	 *            Se elude si es null.
	 * @param pujaM
	 *            Cifra tipo float para filtrar por pujas actuales mayores o
	 *            iguales a ella. Sólo se filtra si es mayor que 0.
	 * @param pujam
	 *            Lo mismo que pujaM, salvo que se filtrará por pujas menores o
	 *            iguales a pujam, siempre cuando sea mayor que 0.
	 * @return El conjunto de subastas que cumplen con los criterios
	 *         especificados.
	 */
	public Collection<Subasta> filtrado(Articulo art, Cliente cl, Date fechaM, Date fecham, float pujaM, float pujam) {
		Collection<Subasta> c = new ArrayList<Subasta>(repoSubastas.values());
		if (art != null) {
			Subasta s = subArt(c, art);
			c.clear();
			c.add(s);
		}
		if (cl != null && cl.poseeContrato()) {
			c = subCl(c, cl);
		}
		if (fechaM != null) {
			c = subFecha(c, fechaM, true);
		}
		if (fecham != null) {
			c = subFecha(c, fecham, false);
		}
		if (pujaM > 0) {
			c = subPuja(c, pujaM, true);
		}
		if (pujam > 0) {
			c = subPuja(c, pujam, false);
		}

		return c;
	}

	/**
	 * Concatena todas las transacciones activas y las devuelve en un listado.
	 * 
	 * @return Las ventas no finalizadas y las subastas activas.
	 */
	public ArrayList<Transaccion> mostrarTransacciones() {
		// ¿Convierto a List o devuelvo el HashMap como tal?
		ArrayList<Transaccion> list = new ArrayList<Transaccion>(repoVentas.values());
		list.addAll(repoSubastas.values());

		return list;
	}

	/**
	 * Realiza la venta de un conjunto de artículos de forma instantánea.
	 * 
	 * @param cliente
	 *            El cliente al que se le realiza la venta.
	 * @param list,
	 *            boolean aplica, int descuento La cesta de la compra del
	 *            cliente.
	 * @return la cuenta total de la venta.
	 * @throws NotValidArtException
	 *             en caso de que algún artículo de la lista no se pueda vender.
	 */
	public float nuevaVenta(Cliente cliente, ArrayList<Articulo> list, boolean aplica, int descuento)
			throws NotValidArtException {

		Venta ventaActual = iniciarVenta(cliente);

		for (Articulo art : list)
			ventaActual.anhArticulo(art);
		return finalizarVenta(ventaActual, descuento, aplica);
	}

	/**
	 * Realiza la venta de un artículo de forma instantánea.
	 * 
	 * @param cliente
	 *            El cliente al que se le realiza la venta.
	 * @param articulo
	 *            La cesta de la compra del cliente.
	 * @param aplica
	 *            Comprueba si se aplica o no descuento.
	 * @param descuento
	 *            Descuento a aplicar.
	 * @return la cuenta total de la venta.
	 * @throws NotValidArtException
	 *             en caso de que el artículo no se pueda vender.
	 */
	public float nuevaVenta(Cliente cliente, Articulo articulo, boolean aplica, int descuento)
			throws NotValidArtException {

		Venta ventaActual = iniciarVenta(cliente);

		ventaActual.anhArticulo(articulo);
		return finalizarVenta(ventaActual, descuento, aplica);
	}

	/**
	 * Devuelve el mínimo total que un usuario debería pagar para pujar.
	 * 
	 * @param s
	 *            La subasta en la que se quiere pujar.
	 * @param cl
	 *            Nuevo pujante.
	 * @param gratis
	 *            Indica si el pujante tiene o no que pagar entrada.
	 * @return El mínimo a pagar para que una puja de cl en s sea validada.
	 */
	public float peekSubasta(Subasta s, Cliente cl, boolean gratis) {
		if (gratis == true || s.getClientes().containsValue(cl)) {
			return (float) (s.getPuja().getCantidad() + 0.05);
		} else
			return (float) (s.getPuja().getCantidad() + 0.05 + s.getEntrada());
	}

	/**
	 * @param newArtPath
	 *            el newArtPath a inicializar.
	 */
	public void setNewArtPath(String newArtPath) {
		GestorArticulos.newArtPath = newArtPath;
	}

	/**
	 * Devuelve la subasta de dentro de una colección que contenga un artículo
	 * específico.
	 * 
	 * @param subastas
	 *            La colección a filtrar.
	 * @param art
	 *            El artículo a buscar.
	 * @return La subasta de art, null en caso de error o de no encontrarse.
	 */
	public Subasta subArt(Collection<Subasta> subastas, es.uam.clavi.model.gart.art.Articulo art) {

		if (subastas == null || art == null)
			return null;
		if (art.getEst() != es.uam.clavi.model.gart.art.Estado.EnSubasta
				&& art.getEst() != es.uam.clavi.model.gart.art.Estado.Vendido)
			return null;
		for (Subasta s : subastas)
			if (s.getArt().equals(art))
				return s;

		return null;
	}

	/**
	 * Devuelve las ventas realizadas a un cliente específico.
	 * 
	 * @param ventas
	 *            El conjunto de ventas de las que se quiere filtrar.
	 * @param cl
	 *            El cliente que queremos buscar.
	 * @return La lista de ventas realizadas a ese cliente.
	 */
	public ArrayList<Subasta> subCl(Collection<Subasta> subastas, Cliente cl) {

		ArrayList<Subasta> list = new ArrayList<Subasta>();

		for (Subasta s : subastas)
			if (s.getCl().equals(cl))
				list.add(s);

		return list;
	}

	/**
	 * Filtra las subastas por su fecha de creación.
	 * 
	 * @param subastas
	 *            Conjunto de ventas a filtrar.
	 * @param f
	 *            Fecha a partir de la cual realizar la comparación.
	 * @param mayor
	 *            Si mayor == true, devuelve las ventas posteriores a f, si no
	 *            las de ese día más las anteriores.
	 * @return La lista de subastas resultado del filrado, o null en caso de
	 *         error.
	 */
	public ArrayList<Subasta> subFecha(Collection<Subasta> subastas, Date f, boolean mayor) {

		ArrayList<Subasta> list = new ArrayList<Subasta>();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yy");

		if (subastas == null)
			return null;
		for (Subasta s : subastas) {

			if (sdf.format(s.getFechaCreacion()).compareTo(sdf.format(f)) > 0) {

				if (mayor == true)
					list.add(s);
			} else if (mayor == false)
				list.add(s);
		}

		return list;
	}

	/**
	 * Filtra un conjunto de subastas por sus pujas actuales.
	 * 
	 * @param subastas
	 *            Conjunto de subastas a filtrar.
	 * @param puja
	 *            Cantidad a usar para el filtrado.
	 * @param mayor
	 *            Designa si las subastas se filtrarán por si son >= o <= que
	 *            puja.
	 * @return Listado con las subastas que cumplen con el criterio.
	 */
	public ArrayList<Subasta> subPuja(Collection<Subasta> subastas, float puja, boolean mayor) {

		ArrayList<Subasta> list = new ArrayList<Subasta>();

		if (subastas == null)
			return null;
		for (Subasta s : subastas) {

			if (s.getPuja().getCantidad() > puja) {
				if (mayor)
					list.add(s);
			} else if (!mayor)
				list.add(s);
		}

		return list;
	}

	/**
	 * Devuelve la venta de dentro de una colección que contenga un artículo
	 * específico.
	 * 
	 * @param ventas
	 *            La colección a filtrar.
	 * @param art
	 *            Artículo a buscar.
	 * @return La venta que contenga a art.
	 */
	public Venta venArt(Collection<Venta> ventas, es.uam.clavi.model.gart.art.Articulo art) {

		if (ventas == null || art == null)
			return null;
		if (art.getEst() != es.uam.clavi.model.gart.art.Estado.EnVenta
				&& art.getEst() != es.uam.clavi.model.gart.art.Estado.Vendido)
			return null;
		for (Venta v : ventas)
			if (v.getLista().contains(art))
				return v;

		return null;
	}

	/**
	 * Devuelve las ventas realizadas a un cliente específico.
	 * 
	 * @param ventas
	 *            El conjunto de ventas de las que se quiere filtrar.
	 * @param cl
	 *            El cliente que queremos buscar.
	 * @return La lista de ventas realizadas a ese cliente.
	 */
	public ArrayList<Venta> venCl(Collection<Venta> ventas, Cliente cl) {

		ArrayList<Venta> list = new ArrayList<Venta>();

		for (Venta v : ventas)
			if (v.getCl().equals(cl))
				list.add(v);

		return list;
	}

	/**
	 * Filtra las ventas por su fecha de creación.
	 * 
	 * @param ventas
	 *            Conjunto de ventas a filtrar.
	 * @param f
	 *            Fecha que usar para las comparaciones.
	 * @param mayor
	 *            Si mayor == true, devuelve las ventas posteriores a f, si no
	 *            las de ese día más las anteriores.
	 * @return La lista de ventas resultado del filrado, o null en caso de
	 *         error.
	 */
	public ArrayList<Venta> venFecha(Collection<Venta> ventas, Date f, boolean mayor) {

		ArrayList<Venta> list = new ArrayList<Venta>();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yy");

		if (ventas == null)
			return null;
		for (Venta v : ventas) {

			if (sdf.format(v.getFechaCreacion()).compareTo(sdf.format(f)) > 0) {

				if (mayor == true)
					list.add(v);
			} else if (mayor == false)
				list.add(v);
		}

		return list;
	}
}