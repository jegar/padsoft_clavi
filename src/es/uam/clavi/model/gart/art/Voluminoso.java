package es.uam.clavi.model.gart.art;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.uam.clavi.model.gart.GestorArticulos;

/**
 * 
 * @author Miguel Laseca
 * @version 1.1
 */
public class Voluminoso extends Articulo {

	private static final long serialVersionUID = 3358317637170006726L;
	private int peso;
	private int alto;
	private int ancho;
	private int largo;
	
	/**
	 * Constructor de los artículos voluminosos.
	 * @param id ID único.
	 * @param descr Descipción.
	 * @param fecha Fecha de adquisición.
	 * @param p1 Precio de adquisición.
	 * @param p2 Precio objetivo.
	 * @param peso Peso.
	 * @param alto Alto.
	 * @param ancho Ancho.
	 * @param fondo Fondo.
	 * @param f Fecha de datación.
	 * @throws MalformedArtException en caso de parámetros inválidos (e.g. peso o dimensión nulos).
	 */
	public Voluminoso (long id, String descr, Date fecha, float p1, float p2, int peso, int alto, int ancho,
			int largo, String f, GestorArticulos gestor) throws MalformedArtException {
		
		if (peso == 0 || alto == 0 || ancho == 0 || largo == 0) throw new MalformedArtException ();
		this.peso = peso;
		this.alto = alto;
		this.ancho = ancho;
		this.largo = largo;
		super.crearArt (id, descr, fecha, p1, p2, f, gestor);
		super.setEst(Estado.Vendible);
	}
	
	@Override
	public boolean equals (Object art) {
		
		if ((art instanceof Voluminoso) == false) return false;
		Voluminoso v = (Voluminoso) art;
		return (super.equals (v) && this.alto == v.alto &&
				this.ancho == v.ancho && this.largo == v.largo &&
				this.peso == v.peso);
	}
	
	/**
	 * Devuelve los gastos de envío de un voluminoso sin descuento.
	 * @return los gastos de porteo del artículo.
	 */
	public float clacularGastosEnv () {
		
		if (alto > 200 || ancho > 200 || largo > 200) return 50;
		else if (peso > 15) {
			
			return 20 + 2*(peso/5);
		} else {

			return 20;
		}
	}

	/**
	 * @return El alto del artículo.
	 */
	public int getAlto() {
		return alto;
	}

	/**
	 * @return El ancho del artículo.
	 */
	public int getAncho() {
		return ancho;
	}

	/**
	 * @return El largo del artículo.
	 */
	public int getLargo() {
		return largo;
	}

	/**
	 * @return El peso del artículo.
	 */
	public float getPeso() {
		return peso;
	}

	@Override
	public String getTipo() {
		
		return "Voluminoso";
	}

	/**
	 * @param alto La altura a asignar.
	 */
	public void setAlto(int alto) {
		this.alto = alto;
	}

	/**
	 * @param ancho La anchura a asignar.
	 */
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	/**
	 * @param largo La longitud a asignar.
	 */
	public void setFondo(int largo) {
		this.largo = largo;
	}
	
	/**
	 * @param peso El peso a asignar.
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}

	@Override
	public String toString () {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		return "V;"+ super.getDescripcion() + ";" + super.getFechaDatacion() + ";"
				+ sdf.format(super.getFechaAdquisicion()) + ";" + String.format (java.util.Locale.US,"%.2f", super.getPrecioAdquisicion()) + ";"
				+ String.format (java.util.Locale.US,"%.2f", super.getPrecioObjetivo()) + ";" + this.peso + ";" + this.alto + ";" + this.ancho +
				";" + this.largo;
	}
}
