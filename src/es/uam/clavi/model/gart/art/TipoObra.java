package es.uam.clavi.model.gart.art;

/**
 * Define los distintos tipos de obras de arte: Pintura, 
 * Escultura, Grabado, Fotografia y Especial (misc.).
 * @author Miguel Laseca
 * @version 1.0
 */
public enum TipoObra {
	Pintura, Escultura, Grabado, Fotografia, Especial
}
