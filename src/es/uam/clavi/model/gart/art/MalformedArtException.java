package es.uam.clavi.model.gart.art;

/**
 * Excepción para los argumentos erróneos a la hora de construir artículos.
 * @author Miguel Laseca
 * @version 1.0
 */
public class MalformedArtException extends Exception {
	private static final long serialVersionUID = 4720534211530667908L;
}
