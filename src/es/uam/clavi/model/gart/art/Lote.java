package es.uam.clavi.model.gart.art;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.trans.NotValidArtException;
import es.uam.clavi.model.gart.trans.SoldArtException;

import java.util.ArrayList;

/**
 * Clase para definir los lotes de varios artículos.
 * @author Miguel Laseca
 * @version 1.2
 */
public class Lote extends Articulo {

	private static final long serialVersionUID = 2832249400296748158L;
	private List<Articulo> list;
	
	
	/**
	 * Constructor. Crea un lote vacío.
	 * @param id El ID único.
	 * @param descr Descipción del lote.
	 * @param fecha Fecha de adquisición del lote.
	 * @param p1 Precio de adquisición del lote.
	 * @param p2 Precio objetivo del lote.
	 * @param f Fecha de datación del conjunto de los artículos que conforman el lote.
	 * @throws MalformedArtException en caso de que los argumentos estén mal.
	 */
	public Lote (long id, String descr, Date fecha, float p1, float p2, String f, GestorArticulos gestor) throws MalformedArtException {
		
		list = new ArrayList<Articulo> ();
		super.crearArt(id, descr, fecha, p1, p2, f, gestor);
		super.setEst(Estado.Subastable);
	}
	
	/**
	 * Constructor. Crea un lote y lo llena con un lstado de artículos.
	 * @param id El ID único.
	 * @param descr Descipción del lote.
	 * @param fecha Fecha de adquisición del lote.
	 * @param p1 Precio de adquisición del lote.
	 * @param p2 Precio objetivo del lote.
	 * @param f Fecha de datación del conjunto de los artículos que conforman el lote.
	 * @param set Los artículos iniciales del lote.
	 * @throws MalformedArtException en caso de que los argumentos estén mal.
	 */
	public Lote (long id, String descr, Date fecha, float p1, float p2, String f,
			ArrayList<Articulo> set, GestorArticulos gestor) throws MalformedArtException {
		

		super.crearArt(id, descr, fecha, p1, p2, f, gestor);
		super.setEst(Estado.Subastable);
		if (set != null) {
			
			list = set;
			for (Articulo art : list) {
				art.setEst(Estado.EnLote);
				gart.actArticulo(art);
			}
		} else list = new ArrayList<Articulo> ();
	}
	
	@Override
	public boolean equals (Object art) {
		
		if ((art instanceof Lote) == false) return false;
		Lote l = (Lote) art;
		return (super.equals (l) && this.list.equals(l.list));
	}
	
	/**
	 * @return La lista de artículos de un lote.
	 */
	public List<Articulo> getList() {
		return list;
	}

	/**
	 * @param list La lista a asignar.
	 */
	public void setList(List<Articulo> list) {
		this.list = list;
	}

	/**
	 * Añade un elemento a la lista de un lote.
	 * @param art El artículo a añadir.
	 * @return true si se pudo insertar, false en caso de error.
	 * @throws NotValidArtException Si el artículo no está disponible para un lote.
	 * @throws SoldArtException Si el artículo está vendido.
	 */
	public boolean anh (Articulo art) throws NotValidArtException, SoldArtException {
		
		if (art.getEst() == Estado.EnLote || art.getEst() == Estado.EnSubasta
				|| art.getEst() == Estado.EnVenta) throw new NotValidArtException ();
		if (art.getEst() == Estado.Vendido)throw new SoldArtException ();
		list.add (art);
		art.setEst(Estado.EnLote);
		gart.actArticulo(art);
		
		return true;
	}
	
	@Override
	public String getTipo() {
		
		return "Lote";
	}

	@Override
	public String toString () {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		String s = "L;" + super.getDescripcion() + ";" + super.getFechaDatacion() + ";"
				+ sdf.format(super.getFechaAdquisicion()) + ";" + String.format (java.util.Locale.US,"%.2f", super.getPrecioAdquisicion()) + ";"
				+ String.format (java.util.Locale.US,"%.2f", super.getPrecioObjetivo());
		for (Articulo art : list) {
			
			s = s.concat(";"+String.valueOf(art.getID()));
		}
		
		return s;
	}
}
