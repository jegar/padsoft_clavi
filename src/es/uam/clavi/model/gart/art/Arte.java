package es.uam.clavi.model.gart.art;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.uam.clavi.model.gart.GestorArticulos;

/**
 * @author Miguel Laseca
 * @version 1.0
 */
public class Arte extends Articulo {

	private static final long serialVersionUID = 3783858200232146821L;
	private TipoObra tipoObra; 
	private String autor;
	private boolean certAutentific;
	
	/**
	 * Constructor de las obras de arte.
	 * @param id ID único.
	 * @param descr Descripción del artículo.
	 * @param fecha Fecha de adquisición.
	 * @param p1 Precio por el que se obtuvo.
	 * @param p2 Precio objetivo.
	 * @param c Indica si posee un certificado de autenticidad.
	 * @param tipoObra Tipo de obra. Véase @see TipoObra.
	 * @param autor Autor de la obra.
	 * @param f Fecha de datación de la obra.
	 * @throws MalformedArtException En caso de que los argumentos sean erróneos.
	 */
	public Arte (long id, String descr, Date fecha, float p1, float p2, TipoObra tipoObra,
			boolean c, String autor, String f, GestorArticulos gestor) throws MalformedArtException {
		
		this.tipoObra = tipoObra;
		this.autor = autor;
		this.certAutentific = c;
		super.crearArt (id, descr, fecha, p1, p2, f, gestor);
		super.setEst(Estado.Ambos);
	}

	@Override
	public String toString() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		return "A;" + super.getDescripcion() + ";" + super.getFechaDatacion() + ";"
				+ sdf.format(super.getFechaAdquisicion()) + ";" + String.format (java.util.Locale.US,"%.2f", super.getPrecioAdquisicion()) + ";"
				+ String.format (java.util.Locale.US,"%.2f", super.getPrecioObjetivo()) + ";" + this.autor + ";" + this.tipoObra.toString() + ";"
				+ ((this.certAutentific == true) ? "S":"N");
		}

	/**
	 * @return el tipo de obra de arte (Pintura, escultura, fotografía, grabado o miscelánea).
	 */
	public TipoObra getTipoObra() {
		return tipoObra;
	}

	/**
	 * Asigna un tipo de obra al artículo.
	 * @param tipoObra El tipo de obra a asignar.
	 */
	public void setTipoObra(TipoObra tipoObra) {
		this.tipoObra = tipoObra;
	}

	/**
	 * @return El autor de la obra.
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * Asigna un autor a la obra.
	 * @param autor El autor a asignar.
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return El certificado de autenticidad.
	 */
	public boolean isCertAutentific() {
		return certAutentific;
	}

	/**
	 * @param certAutentific El certificado a asignar.
	 */
	public void setCertAutentific(boolean certAutentific) {
		this.certAutentific = certAutentific;
	}

	@Override
	public String getTipo() {
		
		return "Arte";
	}
	
	@Override
	public boolean equals (Object art) {
		
		if ((art instanceof Arte) == false) return false;
		Arte a = (Arte) art;
		return (super.equals (a) && this.tipoObra == a.tipoObra &&
				this.certAutentific == a.certAutentific && this.autor.equals(a.autor));
	}
}
