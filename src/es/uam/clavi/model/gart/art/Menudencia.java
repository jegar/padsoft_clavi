package es.uam.clavi.model.gart.art;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.uam.clavi.model.gart.GestorArticulos;

/**
 * 
 * @author Miguel
 * @version 1.1
 */
public class Menudencia extends Articulo {

	private static final long serialVersionUID = 2496895333498763952L;
	private int descuento;

	/**
	 * Constructor de las menudencias.
	 * @param id El ID único.
	 * @param descr Descipción de la menudencia.
	 * @param fecha Fecha de adquisición de la menudencia.
	 * @param p1 Precio de adquisición de la menudencia.
	 * @param p2 Precio objetivo de la menudencia.
	 * @param descuento Descuento aplicable a la menudencia.
	 * @param f La fecha de datación de la menudencia.
	 * @throws MalformedArtException si los argumentos son erróneos (e.g. descuento > 100).
	 */
	public Menudencia (long id, String descr, Date fecha, float p1, float p2, int descuento, String f,
			GestorArticulos gestor) throws MalformedArtException {
		
		if (descuento > 100) throw new MalformedArtException ();
		super.crearArt (id, descr, fecha, p1, p2, f, gestor);
		super.setEst(Estado.Vendible);
		this.descuento = descuento;
	}
	
	@Override
	public boolean equals (Object art) {
		
		if ((art instanceof Menudencia) == false) return false;
		Menudencia m = (Menudencia) art;
		return (super.equals (m) && this.descuento == m.descuento);
	}
	
	/**
	 * @return El descuento.
	 */
	public int getDescuento() {
		return descuento;
	}

	@Override
	public String getTipo() {
		
		return "Menudencia";
	}

	/**
	 * @param descuento El descuento a asignar.
	 */
	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	@Override
	public String toString () {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		return "M;"+ super.getDescripcion() + ";" + super.getFechaDatacion() + ";"
				+ sdf.format(super.getFechaAdquisicion()) + ";" + String.format (java.util.Locale.US,"%.2f", super.getPrecioAdquisicion()) + ";"
				+ String.format (java.util.Locale.US,"%.2f", super.getPrecioObjetivo()) + ";" + this.descuento;
	}
}
