package es.uam.clavi.model.gart.art;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.uam.clavi.model.gart.GestorArticulos;

/**
 * Clase abstracta de la que descienden todos los artículos.
 * @author Miguel Laseca
 * @version 1.0
 */
public abstract class Articulo implements Serializable {
	
	private static final long serialVersionUID = 4134121263713377396L;
	private long ID;
	private String descripcion;
	private Date fechaAdquisicion;
	private float precioAdquisicion;
	private float precioObjetivo;
	private Estado est;
	private String fechaDatacion;
	protected GestorArticulos gart;
	
	/**
	 * Constructor común a todos los artículos.
	 * @param id El ID del artículo.
	 * @param descr Descripción del artículo.
	 * @param fecha Fecha de adquisición del artículo.
	 * @param p1 Precio de adquisición.
	 * @param p2 Precio objetivo.
	 * @param fechaDatacion Fecha de datación del artículo.
	 * @throws MalformedArtException En caso de que p1 > p2.
	 */
	protected void crearArt (long id, String descr, Date fecha, float p1, float p2, String fechaDatacion, GestorArticulos gestor) throws MalformedArtException {
		
		this.ID = id;
		this.descripcion = descr;
		this.fechaAdquisicion = fecha;
		if (p1 >= p2) throw (new MalformedArtException());
		this.precioAdquisicion = p1;
		this.precioObjetivo = p2;
		this.fechaDatacion = fechaDatacion;
		this.gart = gestor;
	}

	@Override
	public abstract String toString();

	/**
	 * @return La fecha de datación.
	 */
	public String getFechaDatacion() {
		return fechaDatacion;
	}

	/**
	 * @param fechaDatacion La fecha de datación a asignar.
	 */
	public void setFechaDatacion(String fechaDatacion) {
		this.fechaDatacion = fechaDatacion;
	}

	/**
	 * @return El tipo de artículo deun artículo.
	 */
	public abstract String getTipo();

	/**
	 * @return El ID de artículo.
	 */
	public long getID() {
		return ID;
	}

	/**
	 * @param id El ID a asignar.
	 */
	public void setID(long id) {
		ID = id;
	}

	/**
	 * @return La descripción del artículo.
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion La descripción a asignar.
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return la fecha de adquisición del artículo.
	 */
	public Date getFechaAdquisicion() {
		return fechaAdquisicion;
	}

	/**
	 * @param fechaAdquisicion La fecha de adquisición a asignar.
	 */
	public void setFechaAdquisicion(Date fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}

	/**
	 * @return El precio de adquisiciń.
	 */
	public float getPrecioAdquisicion() {
		return precioAdquisicion;
	}

	/**
	 * @param precioAdquisicion El precio de adquisición a asignar.
	 */
	public void setPrecioAdquisicion(float precioAdquisicion) {
		this.precioAdquisicion = precioAdquisicion;
	}

	/**
	 * @return El precio objetivo del artículo.
	 */
	public float getPrecioObjetivo() {
		return precioObjetivo;
	}

	/**
	 * @param precioObjetivo El precio objetivo a asignar.
	 */
	public void setPrecioObjetivo(float precioObjetivo) {
		this.precioObjetivo = precioObjetivo;
	}

	/**
	 * Devuelve el estado de un articulo.
	 * @return el estado.
	 */
	public Estado getEst() {
		return est;
	}
	
	/**
	 * Devuelve si un artículo está certificado.
	 * @return false para todos los artículos que no sean obras de arte.
	 * Para las obras de arte devuelve si tiene o no certificado.
	 */
	public boolean isCertAutentific() {
		
		return false;
	}

	/**
	 * @param est El estado a asignar.
	 */
	public void setEst(Estado est) {
		this.est = est;
	}

	@Override
	/**
	 * Comprueba si el artículo es igual a otro dado. Sólo se usará para pruebas de JUnit.
	 * @param a Artículo a comparar.
	 * @return La comprobación de igualdad entre los artículos.
	 */
	public boolean equals (Object art) {
		SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
		Articulo a = (Articulo) art;
		return (this.ID == a.ID && this.descripcion.equals(a.descripcion) &&
				this.est == a.est && sdf.format(this.fechaAdquisicion).equals(sdf.format(a.fechaAdquisicion)) &&
				this.fechaDatacion.equals(a.fechaDatacion) && this.precioAdquisicion ==
				a.precioAdquisicion && this.precioObjetivo == a.precioObjetivo);
	}
}
