package es.uam.clavi.model.gart.art;

/**
 * 
 * @author miguel
 * @version 1.2
 * Ultima adicion: eliminado el estado SinEstado, puesto que no era utilizado
 */
public enum Estado {
	Vendido, Subastable, Vendible, Ambos, EnLote, EnVenta, EnSubasta
}
