package es.uam.clavi.view.frames;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;

import es.uam.clavi.controllers.FrameController;
import es.uam.clavi.view.panel.AdminPanel;
import es.uam.clavi.view.panel.ContractPropScreen;
import es.uam.clavi.view.panel.EmplAttPanel;
import es.uam.clavi.view.panel.LoginPanel;
import es.uam.clavi.view.panel.NewArticlePanel;
import es.uam.clavi.view.panel.NewClientPanel;
import es.uam.clavi.view.panel.NuevoLotePanel;
import es.uam.clavi.view.panel.UserPanel;
import es.uam.clavi.view.panel.filter.ArticlePanel;
import es.uam.clavi.view.panel.filter.AuctionPanel;
import es.uam.clavi.view.panel.filter.ClientPanel;

/**
 * Esta clase gestiona el frame principal
 * 
 * @author jesu
 *
 */
public class Frame extends JFrame {

	private static final long serialVersionUID = 4015927889303529317L;
	@SuppressWarnings("unused")
	private FrameController controller;

	private CardLayout cards;
	private Container deck;
	private AdminPanel adminPanel;
	private ArticlePanel artPanel;
	private AuctionPanel auctionPanel;
	private ClientPanel clientPanel;
	private LoginPanel loginPanel;
	private UserPanel userPanel;
	private NewArticlePanel nuevoArtPanel;
	private NuevoLotePanel nuevoLotePanel;
	private NewClientPanel nuevoClientePanel;
	private ContractPropScreen contrPropPanel;
	private EmplAttPanel emplAttPanel;

	/**
	 * Constructor
	 */
	public Frame() {
		super("Clavichord");
		adminPanel = new AdminPanel();
		artPanel = new ArticlePanel();
		auctionPanel = new AuctionPanel();
		clientPanel = new ClientPanel();
		loginPanel = new LoginPanel();
		userPanel = new UserPanel();
		nuevoLotePanel = new NuevoLotePanel();
		nuevoClientePanel = new NewClientPanel();
		contrPropPanel = new ContractPropScreen();
		nuevoArtPanel = new NewArticlePanel();
		emplAttPanel = new EmplAttPanel();

		this.setMinimumSize(new Dimension(800, 600));
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setUndecorated(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setMinimumSize(new Dimension(800, 600));

		cards = new CardLayout();
		deck = this.getContentPane();
		this.setLayout(cards);
		deck.add(loginPanel, "logPan");
		deck.add(adminPanel, "admPan");
		deck.add(artPanel, "artPan");
		deck.add(auctionPanel, "aucPan");
		deck.add(clientPanel, "cliPan");
		deck.add(userPanel, "usrPan");
		deck.add(nuevoLotePanel, "lotePan");
		deck.add(nuevoClientePanel, "clientePan");
		deck.add(contrPropPanel, "ctrPropPan");
		deck.add(nuevoArtPanel, "nuevoArtPan");
		deck.add(emplAttPanel, "emplPan");

		this.pack();
		this.setVisible(true);
	}

	/**
	 * @return El adminPanel
	 */
	public AdminPanel getAdminPanel() {
		return adminPanel;
	}

	/**
	 * @return the artPanel
	 */
	public ArticlePanel getArtPanel() {
		return artPanel;
	}

	/**
	 * @return the auctionPanel
	 */
	public AuctionPanel getAuctionPanel() {
		return auctionPanel;
	}

	/**
	 * @return the clientPanel
	 */
	public ClientPanel getClientPanel() {
		return clientPanel;
	}

	/**
	 * @param adminPanel
	 *            El adminPanel para colocar
	 */
	public void setAdminPanel(AdminPanel adminPanel) {
		this.adminPanel = adminPanel;
	}

	/**
	 * @return El loginPanel
	 */
	public LoginPanel getLoginPanel() {
		return loginPanel;
	}

	public ContractPropScreen getContrPropPanel() {
		return contrPropPanel;
	}

	/**
	 * @param loginPanel
	 *            El loginPanel para colocar
	 */
	public void setLoginPanel(LoginPanel loginPanel) {
		this.loginPanel = loginPanel;
	}

	/**
	 * Devuelve el layout de cartas
	 * 
	 * @return
	 */
	public CardLayout getCards() {
		return cards;
	}

	/**
	 * Devuelve el contenedor del layout de cartas
	 * 
	 * @return
	 */
	public Container getDeck() {
		return deck;
	}

	/**
	 * Cololca el controlador en el marco
	 * 
	 * @param controller
	 */
	public void setController(FrameController controller) {
		this.controller = controller;
	}

	public UserPanel getUserPanel() {
		return this.userPanel;
	}

	public NuevoLotePanel getNuevoLotePanel() {
		return this.nuevoLotePanel;
	}

	/**
	 * @return the nuevoClientePanel
	 */
	public NewClientPanel getNuevoClientePanel() {
		return nuevoClientePanel;
	}

	/**
	 * @return the nuevoArtPanel
	 */
	public NewArticlePanel getNuevoArtPanel() {
		return nuevoArtPanel;
	}

	/**
	 * 
	 * @return el panel de nuevos empleados
	 */
	public EmplAttPanel getEmplAttPanel() {
		return this.emplAttPanel;
	}
}
