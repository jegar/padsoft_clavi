package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.ContractPropController;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.ForgeButtonNames;

public class ContractPropScreen extends JPanel {

	private static final long serialVersionUID = 1L;

	private ClaviButton<ForgeButtonNames> aceptar;
	private ClaviButton<ForgeButtonNames> cancelar;
	private JTextField descMenud;
	private JTextField descMenudNorm;
	private JTextField aplicPasados;
	private JTextField gastEnvio;
	private JComboBox<String> nSubasPrem;
	private JComboBox<String> nSubasNorm;
	private JTextField precioPrem;
	private JTextField precioNorm;

	public ContractPropScreen() {
		super();

		int separator = 5;
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		this.setBackground(new Color(192, 203, 208));

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.5;
		c.weighty = 0.5;
		c.insets = new Insets(separator, separator, separator, separator);
		aceptar = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.SAVE);
		cancelar = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.ATRS);

		JPanel propiedades = new JPanel();
		GridBagLayout propLayout = new GridBagLayout();
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.BOTH;
		c2.weightx = 0.5;
		c2.weighty = 0.5;
		propiedades.setLayout(propLayout);

		JPanel propTipo = new JPanel();
		propTipo.setBackground(new Color(192, 203, 208));
		propTipo.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Tipo de Contrato")),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		ImageIcon pre_icon = new ImageIcon("res/premium1-297x3001-1.png");
		Icon icon = new ImageIcon(pre_icon.getImage().getScaledInstance(85, 85, Image.SCALE_SMOOTH));
		propTipo.add(new JLabel(icon));
		propTipo.setMinimumSize(new Dimension(85, 85));
		c2.gridx = 0;
		c2.gridy = 0;
		c2.weightx = 0.5;
		propiedades.add(propTipo, c2);

		JPanel descProd = new JPanel();
		GridLayout descLay = new GridLayout(2, 4, 5, 5);
		descProd.setLayout(descLay);
		descProd.setBackground(new Color(192, 203, 208));
		descProd.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Descuento en Productos")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		descProd.add(new JLabel("Descuento Menudencias"));
		descMenud = new JTextField(3);
		descMenud.setBackground(new Color(192, 203, 208));
		descMenud.setEditable(false);
		descProd.add(descMenud);

		gastEnvio = new JTextField();
		descProd.add(new JLabel("Descuento Gastos Envio (%)"));
		descProd.add(gastEnvio);

		descProd.add(new JLabel("Inscripciones Gratuitas"));
		nSubasPrem = new JComboBox<String>();
		nSubasPrem.addItem("Todas");
		int i = 0;
		while (i < 50) {
			nSubasPrem.addItem("Las primeras " + i + " son gratuitas");
			i++;
		}
		descProd.add(nSubasPrem);
		nSubasPrem.setEditable(false);
		descProd.add(nSubasPrem);

		descProd.add(new JLabel("Precio"));
		this.precioPrem = new JTextField();
		descProd.setBackground(new Color(192, 203, 208));
		descProd.add(precioPrem);
		c2.gridx = 1;
		c2.gridy = 0;
		c2.weightx = 0.2;
		propiedades.add(descProd, c2);

		JPanel propTipoNorm = new FillerPanel();
		propTipoNorm.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Tipo de Contrato")),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		propTipoNorm.setMinimumSize(new Dimension(85, 85));
		c2.gridx = 0;
		c2.gridy = 1;
		c2.weightx = 0.5;
		propiedades.add(propTipoNorm, c2);

		JPanel descProdNorm = new JPanel();
		GridLayout descLayNorm = new GridLayout(2, 4);
		descProdNorm.setLayout(descLayNorm);
		descProdNorm.setBackground(new Color(192, 203, 208));
		descProdNorm.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Descuento en Productos")),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		descProdNorm.add(new JLabel("Descuento Menudencias"));
		descMenudNorm = new JTextField("Aplicado con cantidad");
		descMenudNorm.setEditable(false);
		descMenudNorm.setBackground(new Color(192, 203, 208));
		descProdNorm.add(descMenudNorm);

		aplicPasados = new JTextField();
		descProdNorm.add(new JLabel("Aplicar Pasados (€)"));
		descProdNorm.add(aplicPasados);

		descProdNorm.add(new JLabel("Inscripciones Gratuitas"));
		nSubasNorm = new JComboBox<String>();
		nSubasNorm.addItem("Todas");
		i = 0;
		while (i < 50) {
			nSubasNorm.addItem("A partir de la  " + i);
			i++;
		}
		descProdNorm.add(nSubasNorm);

		descProdNorm.add(new JLabel("Precio"));
		descProdNorm.setBackground(new Color(192, 203, 208));
		this.precioNorm = new JTextField();
		descProdNorm.add(precioNorm);

		c.gridwidth = 4;
		c.gridheight = 3;
		c.gridx = 0;
		c.gridy = 1;
		c2.gridx = 1;
		c2.gridy = 1;
		c2.weightx = 0.2;
		propiedades.add(descProdNorm, c2);
		this.add(propiedades, c);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.gridheight = 1;
		this.add(new TitleLogo("titulo2.png", 260, 130), c);

		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridy = 4;
		c.gridx = 0;
		this.add(aceptar, c);
		c.gridx = 1;
		this.add(new FillerPanel(), c);
		c.gridx = 2;
		this.add(new FillerPanel(), c);
		c.gridx = 3;
		this.add(cancelar, c);

	}

	/**
	 * @return the aplicPasados
	 */
	public JTextField getAplicPasados() {
		return aplicPasados;
	}

	/**
	 * @return the gastEnvio
	 */
	public JTextField getGastEnvio() {
		return gastEnvio;
	}

	/**
	 * @return the nSubasNorm
	 */
	public JComboBox<String> getnSubasNorm() {
		return nSubasNorm;
	}

	/**
	 * @return the precioPrem
	 */
	public JTextField getPrecioPrem() {
		return precioPrem;
	}

	/**
	 * @return the precioNorm
	 */
	public JTextField getPrecioNorm() {
		return precioNorm;
	}

	public JComboBox<String> getnSubasPrem() {
		return this.nSubasPrem;
	}

	public void setController(ContractPropController c) {
		aceptar.addActionListener(c);
		aceptar.addKeyListener(c);
		cancelar.addActionListener(c);
		cancelar.addKeyListener(c);
	}
}
