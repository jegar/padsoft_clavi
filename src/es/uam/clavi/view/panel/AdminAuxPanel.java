package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

public class AdminAuxPanel extends JPanel {

	private static final long serialVersionUID = 4746245762500125636L;

	public AdminAuxPanel(int separator, JButton... buttons) {
		super();
		// this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		this.setBackground(new Color(237, 233, 227));
		this.setMinimumSize(new Dimension(140, 420));
		this.setMaximumSize(new Dimension(340, 520));
		this.setPreferredSize(new Dimension(160, 460));
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 4;
		c.gridy = 1;
		c.insets = new Insets(separator, separator, separator, separator);
		c.ipadx = 0;
		c.ipady = 0;
		c.weightx = 1;
		c.weighty = 1;
		for (JButton button : buttons) {
			this.add(button, c);
			c.gridy++;
		}
		this.setVisible(true);
	}
}
