package es.uam.clavi.view.panel.filter;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.model.gart.GestorArticulos;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.art.Estado;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.FilterButtonNames;

public class ArticlePanel extends AbstractFilterPanel<Articulo> {
	
	private static final long serialVersionUID = -132691766371842821L;
	private JComboBox<String> tipo;
	private JCheckBox cert;
	private JTextField fDat;
	private JComboBox<Estado> est;
	private JTextField dd;
	private JTextField mm;
	private JTextField aa;
	private JTextField precioM;
	private JTextField preciom;
	private ClaviButton<FilterButtonNames> sellButton;
	private ClaviButton<FilterButtonNames> batchButton;
	private ClaviButton<FilterButtonNames> aucButton;
	private ClaviButton<FilterButtonNames> newAucButton;
	private ClaviButton<FilterButtonNames> unmButton;
	
	public ArticlePanel () {
		
		super ();
		
		cols = 6;
		String[] auxt = {"ID", "Descripción", "Fecha de adquisición",
				"Precio/Puja actual", "Estado", "Fecha de datación"};
		titles = auxt;
		Integer[] auxs = {20, 400, 140, 140, 75, 120};
		sizes = auxs;
		JPanel panel = new JPanel ();
		panel.setLayout(new BoxLayout (panel, BoxLayout.Y_AXIS));
		
		JPanel flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(titleLogo);
		panel.add(flow);
		
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		cert = new JCheckBox("Certificado");
		cert.setBackground(new Color(192, 203, 208));

		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(cert);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelFDat = new JLabel("Fecha de datación");
		fDat = new JTextField(5);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelFDat);
		panel.add(flow);
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(fDat);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelEst = new JLabel ("Estado");
		Estado[] e = new Estado[8];
		e[0] = null;
		int i = 1;
		for (Estado aux : Estado.values ()) {
			e[i] = aux;
			i++;
		}
		est = new JComboBox<Estado>(e);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelEst);
		panel.add(flow);
	
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(est);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelFAdq = new JLabel ("Fecha de adquisición");
		JPanel fAdq = new JPanel (new FlowLayout());
		fAdq.setBackground(new Color(192, 203, 208));
		dd = new JTextField ("DD", 2);
		mm = new JTextField ("MM", 2);
		aa = new JTextField ("AAAA", 4);
		fAdq.add(dd);
		fAdq.add(mm);
		fAdq.add(aa);
		flow = new JPanel(new FlowLayout());
		flow.add(labelFAdq);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		panel.add(fAdq);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelPreciom = new JLabel ("Precio mínimo");
		preciom = new JTextField("0", 10);
		flow = new JPanel(new FlowLayout());
		flow.add(labelPreciom);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.add(preciom);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelPrecioM = new JLabel ("Precio máximo");
		precioM = new JTextField("0", 10);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelPrecioM);
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(precioM);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelTipo = new JLabel("Tipo de artículo");
		String[] st = {null,"Arte","Lote","Menudencia","Voluminoso"};
		tipo = new JComboBox<String>(st);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelTipo);
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(tipo);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		setModel(GestorArticulos.artRecien(10));
		
		fButton = new ClaviButton<>(FilterButtonNames.FILTERA);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(fButton);
		panel.add(flow);
		
		panel.setBackground(new Color(192, 203, 208));
		add(panel, BorderLayout.WEST);
		
		JPanel aux = new JPanel();
		aux.setLayout(new BoxLayout(aux, BoxLayout.Y_AXIS));
		
		JPanel grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(sellButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.SELL));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(batchButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.LINK));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(aucButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.LINKS));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(newAucButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.NAUC));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(unmButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.SPARE));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		bPanel.add(aux, BorderLayout.NORTH);
	}

	/**
	 * @return the tipo
	 */
	public JComboBox<String> getTipo() {
		return tipo;
	}

	/**
	 * @return the cert
	 */
	public JCheckBox getCert() {
		return cert;
	}

	/**
	 * @return the fDat
	 */
	public JTextField getfDat() {
		return fDat;
	}

	/**
	 * @return the est
	 */
	public JComboBox<Estado> getEst() {
		return est;
	}

	/**
	 * @return the dd
	 */
	public JTextField getDd() {
		return dd;
	}

	/**
	 * @return the mm
	 */
	public JTextField getMm() {
		return mm;
	}

	/**
	 * @return the aa
	 */
	public JTextField getAa() {
		return aa;
	}

	/**
	 * @return the precioM
	 */
	public JTextField getPrecioM() {
		return precioM;
	}

	/**
	 * @return the preciom
	 */
	public JTextField getPreciom() {
		return preciom;
	}

	public void setController (FilterPanelController c) {
		super.setController(c);
		sellButton.addActionListener(c);
		batchButton.addActionListener(c);
		unmButton.addActionListener(c);
		aucButton.addActionListener(c);
		newAucButton.addActionListener(c);
	}
	
	public void setButtonsDisplay (byte values) {
		
		List<ClaviButton<FilterButtonNames>> sel = new ArrayList<>();
		// nauct | sell | batch | auct | unm
		if ((values&1) != 0) sel.add(unmButton);
		if ((values&2) != 0) sel.add(aucButton);
		if ((values&4) != 0) sel.add(batchButton);
		if ((values&8) != 0) sel.add(sellButton);
		if ((values&16) != 0) sel.add(newAucButton);
		
		JPanel panel1, panel2;
		for (int i = 0; i < ((JPanel)bPanel.getComponent(1)).getComponentCount(); i++){
			panel1 = (JPanel)((JPanel)bPanel.getComponent(1)).getComponent(i);
			panel2 = (JPanel)panel1.getComponent(0);
			if (sel.contains(panel2.getComponent(0))) {
				panel2.setVisible(true);
			} else panel2.setVisible(false);
			panel2.revalidate();
			panel2.repaint();
			panel1.revalidate();
			panel1.repaint();
		}
		bPanel.revalidate();
		bPanel.repaint();
		revalidate();
		repaint();
	}
}
