package es.uam.clavi.view.panel.filter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.FilterButtonNames;

public class ClientPanel extends AbstractFilterPanel<Cliente> {
	
	private static final long serialVersionUID = -8905878308753366918L;
	private JCheckBox contrato;
	private JTextField nombre;
	private JTextField pApellido;
	private JTextField eMail;
	private ClaviButton<FilterButtonNames> renoveButton;
	private ClaviButton<FilterButtonNames> modifyButton;
	/*private ClaviButton<FilterButtonNames> subscrButton;
	private ClaviButton<FilterButtonNames> unsubscrButton;*/
	
	public ClientPanel () {
		
		super ();
		
		cols = 8;	// Tlf, nombre, apellidos, email, contrato (tipo), empresa
		String[] auxt = {"Teléfono", "Nombre", "Primer apellido", "Segundo apellido",
				"email", "DNI/NIA", "Contrato", "Empresa"};
		titles = auxt;
		Integer[] auxs = {20, 20, 20, 20, 20, 20, 20, 20};
		sizes = auxs;
		JPanel panel = new JPanel ();
		panel.setLayout(new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(192, 203, 208));;
		
		JPanel flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(titleLogo);
		panel.add(flow);
		
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		contrato = new JCheckBox("¿Tiene contrato?");
		contrato.setBackground(new Color(192, 203, 208));
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(contrato);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelNombre = new JLabel("Nombre");
		nombre = new JTextField(20);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelNombre);
		panel.add(flow);
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(nombre);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelApellido = new JLabel("Primer apellido");
		pApellido = new JTextField(20);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelApellido);
		panel.add(flow);
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(pApellido);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelEmail = new JLabel("Email");
		eMail = new JTextField(20);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelEmail);
		panel.add(flow);
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(eMail);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		setModel(new ArrayList<Cliente>());
		
		fButton = new ClaviButton<>(FilterButtonNames.FILTERC);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(fButton);
		panel.add(flow);
		
		add (panel, BorderLayout.WEST);
		
		JPanel aux = new JPanel();
		aux.setLayout(new BoxLayout(aux, BoxLayout.Y_AXIS));
		
		JPanel grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(renoveButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.RENOVE));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(modifyButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.MODIFY));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		/*grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(subscrButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.SUSALL));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(unsubscrButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.UNSUSALL));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);*/
		
		bPanel.add(aux, BorderLayout.NORTH);
	}
	
	/**
	 * @return the contrato
	 */
	public JCheckBox getContrato() {
		return contrato;
	}

	/**
	 * @return the nombre
	 */
	public JTextField getNombre() {
		return nombre;
	}

	/**
	 * @return the pApellido
	 */
	public JTextField getpApellido() {
		return pApellido;
	}

	/**
	 * @return the eMail
	 */
	public JTextField geteMail() {
		return eMail;
	}

	public void setController (FilterPanelController c) {
		super.setController(c);
		renoveButton.addActionListener(c);
		modifyButton.addActionListener(c);
		renoveButton.addKeyListener(c);
		modifyButton.addKeyListener(c);
		/*subscrButton.addActionListener(c);
		unsubscrButton.addActionListener(c);*/
	}
	
	public void setButtonsDisplay (byte values) {
		
		List<ClaviButton<FilterButtonNames>> sel = new ArrayList<>();
		// unsubscribe | subscribe | renove
		if ((values&1) != 0) sel.add(renoveButton);
		/*if ((values&2) != 0) sel.add(subscrButton);
		if ((values&4) != 0) sel.add(unsubscrButton);*/
		
		JPanel panel1, panel2;
		for (int i = 0; i < ((JPanel)bPanel.getComponent(1)).getComponentCount(); i++){
			panel1 = (JPanel)((JPanel)bPanel.getComponent(1)).getComponent(i);
			panel2 = (JPanel)panel1.getComponent(0);
			if (sel.contains(panel2.getComponent(0))) {
				panel2.setVisible(true);
			} else panel2.setVisible(false);
			panel2.revalidate();
			panel2.repaint();
			panel1.revalidate();
			panel1.repaint();
		}
		bPanel.revalidate();
		bPanel.repaint();
		revalidate();
		repaint();
	}
}
