package es.uam.clavi.view.panel.filter;

import javax.swing.table.DefaultTableModel;

public class FilterTableModel extends DefaultTableModel {
	private static final long serialVersionUID = -56942170651050062L;
	public FilterTableModel(Object[][] objects, String[] titles) {
		super (objects, titles);
	}
	public FilterTableModel(Object[] columnNames, int rowCount) {
		super (columnNames, rowCount);
	} 
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
