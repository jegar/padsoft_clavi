package es.uam.clavi.view.panel.filter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.enums.FilterButtonNames;

public class AuctionPanel extends AbstractFilterPanel<Subasta>{
	
	private static final long serialVersionUID = 4604709354200274452L;
	private JTextField art;	// Filtra por el artículo dado su ID.
	private JTextField cl;	// Filtra por el cliente dado su teléfono.
	private JTextField dd;
	private JTextField mm;
	private JTextField aa;
	private JTextField DD;
	private JTextField MM;
	private JTextField AA;
	private JTextField precioM;
	private JTextField preciom;
	private ClaviButton<FilterButtonNames> bidButton;
	private ClaviButton<FilterButtonNames> aButton;
	private ClaviButton<FilterButtonNames> cButton;
	private ClaviButton<FilterButtonNames> sButton;
	private ClaviButton<FilterButtonNames> unsButton;
	
	public AuctionPanel () {
		
		super ();
		
		cols = 6;
		String[] auxt = {"ID", "Artículo subastado", "Entrada",
				"Iniciada", "Finaliza en", "Puja actual"};
		titles = auxt;
		Integer[] auxs = {20, 400, 140, 140, 75, 120};
		sizes = auxs;
		
		JPanel panel = new JPanel ();
		panel.setLayout(new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(192, 203, 208));
		JPanel flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(titleLogo);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		JLabel artLabel = new JLabel("ID del artículo");
		flow.add(artLabel);
		panel.add(flow);
		art = new JTextField("0",15);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(art);
		panel.add(flow);
		
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		JLabel clLabel = new JLabel("Teléfono del cliente");
		flow.add(clLabel);
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		cl = new JTextField(15);
		flow.add(cl);
		panel.add(flow);
		
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelfm = new JLabel ("Fecha de adquisición");
		JPanel fm = new JPanel (new FlowLayout());
		fm.setBackground(new Color(192, 203, 208));
		dd = new JTextField ("DD", 2);
		mm = new JTextField ("MM", 2);
		aa = new JTextField ("AAAA", 4);
		fm.add(dd);
		fm.add(mm);
		fm.add(aa);
		flow = new JPanel(new FlowLayout());
		flow.add(labelfm);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		panel.add(fm);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelFM = new JLabel ("Fecha de adquisición");
		JPanel fM = new JPanel (new FlowLayout());
		fM.setBackground(new Color(192, 203, 208));
		DD = new JTextField ("DD", 2);
		MM = new JTextField ("MM", 2);
		AA = new JTextField ("AAAA", 4);
		fM.add(DD);
		fM.add(MM);
		fM.add(AA);
		flow = new JPanel(new FlowLayout());
		flow.add(labelFM);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		panel.add(fM);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelPreciom = new JLabel ("Precio mínimo");
		preciom = new JTextField("0", 10);
		flow = new JPanel(new FlowLayout());
		flow.add(labelPreciom);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.add(preciom);
		flow.setBackground(new Color(192, 203, 208));
		panel.add(flow);
		panel.add (Box.createVerticalStrut(LINE_SPACE_SEPARATION));
		
		JLabel labelPrecioM = new JLabel ("Precio máximo");
		precioM = new JTextField("0", 10);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(labelPrecioM);
		panel.add(flow);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(precioM);
		panel.add(flow);
		panel.add (Box.createVerticalStrut(2*LINE_SPACE_SEPARATION));
		
		setModel(new ArrayList<Subasta>());
		
		fButton = new ClaviButton<>(FilterButtonNames.FILTERAU);
		flow = new JPanel(new FlowLayout());
		flow.setBackground(new Color(192, 203, 208));
		flow.add(fButton);
		panel.add(flow);
		
		add (panel, BorderLayout.WEST);

		JPanel aux = new JPanel();
		aux.setLayout(new BoxLayout(aux, BoxLayout.Y_AXIS));
		
		JPanel grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(bidButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.BID));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(aButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.LINKA));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(cButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.CANCEL));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(sButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.SUSCRIBE));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		grid = new JPanel(new GridBagLayout());
		flow = new JPanel (new FlowLayout());
		flow.add(unsButton = new ClaviButton<FilterButtonNames>(FilterButtonNames.UNSUSCRIBE));
		flow.setBackground(new Color(192, 203, 208));
		flow.setVisible(false);
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		aux.add(grid);
		
		bPanel.add(aux, BorderLayout.NORTH);
	}
	
/**
	 * @return the art
	 */
	public JTextField getArt() {
		return art;
	}

	/**
	 * @return the cl
	 */
	public JTextField getCl() {
		return cl;
	}

	/**
	 * @return the dd
	 */
	public JTextField getDd() {
		return dd;
	}

	/**
	 * @return the mm
	 */
	public JTextField getMm() {
		return mm;
	}

	/**
	 * @return the aa
	 */
	public JTextField getAa() {
		return aa;
	}

	/**
	 * @return the dD
	 */
	public JTextField getDD() {
		return DD;
	}

	/**
	 * @return the mM
	 */
	public JTextField getMM() {
		return MM;
	}

	/**
	 * @return the aA
	 */
	public JTextField getAA() {
		return AA;
	}

	/**
	 * @return the precioM
	 */
	public JTextField getPrecioM() {
		return precioM;
	}

	/**
	 * @return the preciom
	 */
	public JTextField getPreciom() {
		return preciom;
	}

	public void setController (FilterPanelController c) {
		super.setController(c);
		bidButton.addActionListener(c);
		aButton.addActionListener(c);
		cButton.addActionListener(c);
		sButton.addActionListener(c);
		unsButton.addActionListener(c);
	}
	
	public void setButtonsDisplay (byte values) {
		
		List<ClaviButton<FilterButtonNames>> sel = new ArrayList<>();
		// unsus | sus | cancel | art | bid
		if ((values&1) != 0) sel.add(bidButton);
		if ((values&2) != 0) sel.add(aButton);
		if ((values&4) != 0) sel.add(cButton);
		if ((values&8) != 0) sel.add(sButton);
		if ((values&16) != 0) sel.add(unsButton);
		
		JPanel panel1, panel2;
		for (int i = 0; i < ((JPanel)bPanel.getComponent(1)).getComponentCount(); i++){
			panel1 = (JPanel)((JPanel)bPanel.getComponent(1)).getComponent(i);
			panel2 = (JPanel)panel1.getComponent(0);
			if (sel.contains(panel2.getComponent(0))) {
				panel2.setVisible(true);
			} else panel2.setVisible(false);
			panel2.revalidate();
			panel2.repaint();
			panel1.revalidate();
			panel1.repaint();
		}
		bPanel.revalidate();
		bPanel.repaint();
		revalidate();
		repaint();
	}
}
