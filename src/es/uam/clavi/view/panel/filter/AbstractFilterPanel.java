package es.uam.clavi.view.panel.filter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

import es.uam.clavi.controllers.FilterPanelController;
import es.uam.clavi.model.gart.art.Articulo;
import es.uam.clavi.model.gart.trans.Subasta;
import es.uam.clavi.model.gclien.Cliente;
import es.uam.clavi.model.gclien.Premium;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.FilterButtonNames;

public abstract class AbstractFilterPanel<T> extends JPanel {
	
	private static final long serialVersionUID = -7778100768072320444L;
	protected final int LINE_SPACE_SEPARATION = 9;
	protected JTable resultado;
	protected DefaultTableModel model;
	protected Integer[] sizes;
	protected String[] titles;
	protected Object[][] objects;
	protected JScrollPane scroll;
	protected JPanel bPanel;
	protected ClaviButton<FilterButtonNames> fButton;
	protected ClaviButton<FilterButtonNames> bButton;
	protected TitleLogo titleLogo;
	protected int cols;
	
	protected AbstractFilterPanel () {
		setLayout(new BorderLayout (0, 0));
		
		titleLogo = new TitleLogo("titulo2.png", 260, 90);
		bPanel = new JPanel(new BorderLayout(10,10));
		bPanel.setBackground(new Color(192, 203, 208));

		bButton = new ClaviButton<>(FilterButtonNames.BACK);
		
		JPanel grid = new JPanel(new GridBagLayout());
		JPanel flow = new JPanel (new FlowLayout());
		flow.add(bButton);
		flow.setBackground(new Color(192, 203, 208));
		grid.add(flow);
		grid.setBackground(new Color(192, 203, 208));
		bPanel.add(grid, BorderLayout.SOUTH);
		add (bPanel, BorderLayout.EAST);
	}
	
	public void setModel (List<T> elems) {
		objects = new Object[elems.size()][cols];
		if (model == null) model = new FilterTableModel(titles, 0);
		if (resultado == null) {
			resultado = new JTable(model);
		}
		resultado.setAlignmentX(JTable.CENTER_ALIGNMENT);
		if (scroll == null) {
			scroll = new JScrollPane(resultado);
			add (scroll);
		}
		scroll.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		
		for (int i = 0; i < elems.size();i++)
			for (int j = 0; j < cols; j++)
				objects[i][j] = getElemAt(elems, i, j);
		
		model.setDataVector(objects, titles);
		for (int i = 0; i < resultado.getColumnCount(); i++)
			resultado.getColumnModel().getColumn(i).setPreferredWidth(sizes[i]);
	}
	
	@SuppressWarnings("unchecked")
	public Object getElemAt (List<T> elems, int i, int j) {
		if (this instanceof ArticlePanel) {
			switch (j) {
				case 0:
					return ((List<Articulo>)elems).get(i).getID();
				case 1:
					return ((List<Articulo>)elems).get(i).getDescripcion();
				case 2:
					return (new SimpleDateFormat("dd/MM/yyyy")).format(((List<Articulo>)elems).get(i).getFechaAdquisicion());
				case 3:
					return ((List<Articulo>)elems).get(i).getPrecioObjetivo();
				case 4:
					return ((List<Articulo>)elems).get(i).getEst();
				case 5:
					return ((List<Articulo>)elems).get(i).getFechaDatacion();
				default:
					return null;
			}
		}else if (this instanceof AuctionPanel) {
			switch (j) {
				case 0:
					return ((List<Subasta>)elems).get(i).getId();
				case 1:
					return ((List<Subasta>)elems).get(i).getArt();
				case 2:
					return ((List<Subasta>)elems).get(i).getEntrada();
				case 3:
					return ((List<Subasta>)elems).get(i).isIniciada() ? "S":"N";
				case 4:
					return ((List<Subasta>)elems).get(i).getFechaFin();
				case 5:
					return ((List<Subasta>)elems).get(i).getPuja().getCantidad();
				default:
					return null;
			}
		} else if (this instanceof ClientPanel) {
			switch (j) {
				case 0:
					return ((List<Cliente>)elems).get(i).getTlf();
				case 1:
					return ((List<Cliente>)elems).get(i).getNombre();
				case 2:
					return ((List<Cliente>)elems).get(i).getPrimerApellido();
				case 3:
					return ((List<Cliente>)elems).get(i).getSegundoApellido();
				case 4:
					return ((List<Cliente>)elems).get(i).geteMail();
				case 5:
					return ((List<Cliente>)elems).get(i).getDnia();
				case 6:
					if (!((List<Cliente>)elems).get(i).poseeContrato()) return "";
					return (((List<Cliente>)elems).get(i).getContrato() instanceof Premium) ? "Premium":"Normal";
				case 7:
					return ((List<Cliente>)elems).get(i).getEmpresa();
			}
		}
		
		return null;
	}
	
	/**
	 * @return El modelo de las tablas de filtrados.
	 */
	public DefaultTableModel getModel() {
		return model;
	}
	
	/**
	 * @return La tabla del panel con los resultados de los filtrados.
	 */
	public JTable getTable () {
		return resultado;
	}
	
	public void setController (FilterPanelController c) {
		fButton.addActionListener(c);
		fButton.addKeyListener(c);
		bButton.addActionListener(c);
		bButton.addKeyListener(c);
		resultado.addKeyListener(c);
		resultado.addMouseListener(c);
		resultado.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}
}
