package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.ForgeButtonNames;

public class EmplAttPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private JTextField username;
	private JTextField password;
	private JTextField apellidoField;
	private JTextField nomField;
	private JTextField telefono;
	private JTextField movil;
	private ClaviButton<ForgeButtonNames> botEnviar;
	private ClaviButton<ForgeButtonNames> botAtras;
	private Component titleLogo;

	public EmplAttPanel() {
		int separator = 5;

		setLayout(new GridBagLayout());
		setBackground(new Color(192, 203, 208));
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();

		panel.setBackground(new Color(192, 203, 208));
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Datos de Login")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		c2.gridwidth = 1;
		c2.gridheight = 1;
		c2.weightx = 0.5;
		c2.weighty = 0.5;
		c2.fill = GridBagConstraints.BOTH;
		c2.insets = new Insets(separator, separator, separator, separator);
		c2.gridx = 0;
		c2.gridy = 0;
		panel.add(new JLabel("Nombre de Usuario de Empleado (Obligatorio)"), c2);
		c2.gridx = 1;
		username = new JTextField(20);
		panel.add(username, c2);

		c2.gridx = 0;
		c2.gridy = 1;
		panel.add(new JLabel("Contraseña Empleado (Obligatorio)"), c2);
		c2.gridx = 1;
		password = new JTextField(20);
		panel.add(password, c2);
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 4;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.5;
		c.weighty = 0.5;
		this.add(panel, c);

		JPanel optionalPanel = new JPanel();
		optionalPanel.setLayout(new GridBagLayout());

		optionalPanel.setBackground(new Color(192, 203, 208));
		optionalPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Datos Opcionales")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		c2.gridx = 0;
		c2.gridy = 0;
		optionalPanel.add(new JLabel("Nombre"), c2);
		c2.gridx = 1;
		this.nomField = new JTextField(20);
		optionalPanel.add(this.nomField, c2);

		c2.gridx = 0;
		c2.gridy = 1;
		optionalPanel.add(new JLabel("Apellido"), c2);
		c2.gridx = 1;
		this.apellidoField = new JTextField(20);
		optionalPanel.add(this.apellidoField, c2);

		c2.gridx = 0;
		c2.gridy = 2;
		optionalPanel.add(new JLabel("Telefono"), c2);
		c2.gridx = 1;
		this.telefono = new JTextField(20);
		optionalPanel.add(this.telefono, c2);

		c2.gridx = 0;
		c2.gridy = 3;
		optionalPanel.add(new JLabel("Movil"), c2);
		c2.gridx = 1;
		this.movil = new JTextField(20);
		optionalPanel.add(this.movil, c2);

		c.weightx = 0.6;
		c.weighty = 0.6;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 2;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = 3;
		this.add(optionalPanel, c);

		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weightx = 0.3;
		c.weighty = 0.3;
		botEnviar = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.CRAR);
		c.insets = new Insets(separator, separator, separator, separator);
		this.add(this.botEnviar, c);
		c.gridx = 1;
		this.add(new FillerPanel(), c);
		this.botAtras = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.ATRS);
		c.gridx = 3;
		this.add(this.botAtras, c);
		c.gridx = 2;
		this.add(new FillerPanel(), c);

		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(separator, separator, separator, separator);
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		add(titleLogo, c);
	}

	public void setController(ActionListener controller) {
		botEnviar.addActionListener(controller);
		botAtras.addActionListener(controller);
	}

	public JTextField getApellidoField() {
		return apellidoField;
	}

	public JTextField getNomField() {
		return nomField;
	}

	public JTextField getTelefono() {
		return telefono;
	}

	public JTextField getMovil() {
		return movil;
	}

	public JButton getBotEnv() {
		return this.botEnviar;
	}

	public JButton getBotAtt() {
		return this.botAtras;
	}

	public JTextField getUserName() {
		return this.username;
	}

	public JTextField getPassword() {
		return this.password;
	}
}
