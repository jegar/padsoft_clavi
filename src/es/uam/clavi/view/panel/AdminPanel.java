package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import es.uam.clavi.controllers.AdminPanelController;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.AdminButtonNames;

public class AdminPanel extends JPanel {

	private static final long serialVersionUID = 8753676673656738110L;

	private JButton listadoArticulos;
	private JButton modificarEmpleados;
	private JButton modificarSubastas;
	private JButton anyadirEmpleado;
	private JButton nuevosArticulos;
	private JButton nuevoLote;
	private JButton propiedadeContratos;
	private JButton salir;
	private JButton cerrarSesion;
	private JLabel titleLogo;
	private AdminAuxPanel panelAuxiliar;

	@SuppressWarnings("unused")
	private AdminPanelController controller;

	public AdminPanel() {
		int separator = 10;

		this.setMinimumSize(new Dimension(800, 600));
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();

		/* Creamos y colocamos cada boton */
		propiedadeContratos = new ClaviButton<AdminButtonNames>(AdminButtonNames.PRCN);
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 4;
		c.gridy = 2;
		c.insets = new Insets(separator, separator, separator, separator);
		c.ipadx = 0;
		c.ipady = 0;
		c.weightx = 1;
		c.weighty = 1;
		this.add(propiedadeContratos, c);

		nuevoLote = new ClaviButton<AdminButtonNames>(AdminButtonNames.NVLT);
		c.gridx = 3;
		c.gridy = 2;
		this.add(nuevoLote, c);

		nuevosArticulos = new ClaviButton<AdminButtonNames>(AdminButtonNames.NVAR);
		c.gridx = 2;
		c.gridy = 3;
		this.add(nuevosArticulos, c);

		anyadirEmpleado = new ClaviButton<AdminButtonNames>(AdminButtonNames.ANEM);
		c.gridx = 2;
		c.gridy = 2;
		this.add(new FillerPanel(), c);

		cerrarSesion = new ClaviButton<AdminButtonNames>(AdminButtonNames.CESE);
		c.gridx = 3;
		c.gridy = 4;
		this.add(cerrarSesion, c);

		salir = new ClaviButton<AdminButtonNames>(AdminButtonNames.SALI);
		c.gridx = 4;
		c.gridy = 4;
		this.add(salir, c);

		/* Los siguientes botones van todos en el panel lateral */
		listadoArticulos = new ClaviButton<AdminButtonNames>(AdminButtonNames.LIAR);
		modificarEmpleados = new ClaviButton<AdminButtonNames>(AdminButtonNames.MOEM);
		modificarSubastas = new ClaviButton<AdminButtonNames>(AdminButtonNames.LISU);

		panelAuxiliar = new AdminAuxPanel(separator, listadoArticulos, anyadirEmpleado, modificarSubastas);
		c.gridheight = 3;
		c.gridx = 1;
		c.gridy = 2;
		c.insets = new Insets(0, separator, 0, separator);
		this.add(panelAuxiliar, c);

		/* Creamos y ajustamos el titulo */
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(separator, separator, separator, separator);
		this.add(titleLogo, c);
		this.setBackground(new Color(192, 203, 208));

	}

	public void setController(AdminPanelController controller) {
		propiedadeContratos.addActionListener(controller);
		modificarSubastas.addActionListener(controller);
		nuevoLote.addActionListener(controller);
		nuevosArticulos.addActionListener(controller);
		listadoArticulos.addActionListener(controller);
		modificarEmpleados.addActionListener(controller);
		salir.addActionListener(controller);
		cerrarSesion.addActionListener(controller);
		anyadirEmpleado.addActionListener(controller);
	}

}
