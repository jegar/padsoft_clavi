package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import es.uam.clavi.controllers.LoginPanelController;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.TitleLogo;

/**
 * Panel para los logins.
 * 
 * @author Miguel Laseca.
 * @version 2.0
 */
public class LoginPanel extends JPanel {

	private static final long serialVersionUID = 8490682489750458347L;
	private ClaviButton<Void> button;
	private JTextField field;
	private JPasswordField passw;
	private TitleLogo titleLogo;

	public LoginPanel() {

		int separator = 15;
		setLayout(new GridBagLayout());
		setSize(new Dimension(32, 250));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(separator, separator, separator, separator);
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		add(titleLogo, c);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setSize(new Dimension(32, 250));

		JLabel label1 = new JLabel("Username");
		field = new JTextField(30);
		JLabel label2 = new JLabel("Password");
		passw = new JPasswordField(30);
		button = new ClaviButton<Void>("<html><center>Login", "login.png", 0);

		label1.setAlignmentX(CENTER_ALIGNMENT);
		label2.setAlignmentX(CENTER_ALIGNMENT);
		button.setAlignmentX(CENTER_ALIGNMENT);

		setAlignmentX(CENTER_ALIGNMENT);
		panel.setAlignmentX(CENTER_ALIGNMENT);

		panel.add(label1);
		panel.add(Box.createVerticalStrut(10));
		JPanel flow = new JPanel(new FlowLayout());
		flow.add(field);
		panel.add(flow);
		panel.add(Box.createVerticalStrut(40));
		panel.add(label2);
		panel.add(Box.createVerticalStrut(10));
		flow = new JPanel(new FlowLayout());
		flow.add(passw);
		panel.add(flow);
		panel.add(Box.createVerticalStrut(40));
		flow = new JPanel(new FlowLayout());
		flow.add(button);
		panel.add(flow);
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED),
				BorderFactory.createEmptyBorder(20, 20, 20, 20)));
		add(panel, c);

		setBackground(new Color(192, 203, 208));
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
	}

	public void setController(LoginPanelController c) {
		button.addActionListener(c);
		button.addKeyListener(c);
	}

	public String getText() {
		return field.getText();
	}

	public String getPassword() {
		return String.copyValueOf(passw.getPassword());
	}

	public void resetFields() {
		this.passw.setText("");
		this.field.setText("");
	}
}
