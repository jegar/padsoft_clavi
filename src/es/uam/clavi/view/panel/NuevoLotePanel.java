package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import es.uam.clavi.view.components.ButtonEnumeration;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.LoteButtonNames;

public class NuevoLotePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private ClaviButton<ButtonEnumeration> crearLote;
	private ClaviButton<ButtonEnumeration> volver;
	private JTable tablaArticulos;
	private JTable tablaLote;
	private JTextField idArticulo;
	private JTextField tipoArticulo;
	private JTextField fecha;
	private JTextField precio;
	private JTextField anyoOrigen;
	private JTextArea descripcion;
	private JComboBox<String> filtTipo;
	private JCheckBox filtCert;
	private JTextField filtFechaMin;
	// private JTextField filtFechaMax;
	private JTextField filtPrecMin;
	private JTextField filtPrecMax;
	private JTextField filtDatacion;
	private ActionListener actController;
	private MouseListener selController;
	private JTextArea nuevaDescripcion;
	private JButton filtrar;

	public NuevoLotePanel() {
		super();
		int separator = 5;

		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		this.setBackground(new Color(192, 203, 208));

		/* Botones a usar */
		crearLote = new ClaviButton<ButtonEnumeration>(LoteButtonNames.CRLO);
		c.insets = new Insets(separator, separator, separator, separator);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.weightx = 0.3;
		c.weighty = 0.3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 4;
		c.fill = GridBagConstraints.BOTH;
		this.add(crearLote, c);

		volver = new ClaviButton<ButtonEnumeration>(LoteButtonNames.VLVR);
		c.gridx = 6;
		this.add(volver, c);

		nuevaDescripcion = new JTextArea(3, 12);
		nuevaDescripcion.setWrapStyleWord(true);
		nuevaDescripcion.setLineWrap(true);
		JScrollPane nuevDesPane = new JScrollPane(nuevaDescripcion);
		nuevDesPane.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Descripcion Lote")),
				BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		c.gridx = 3;
		c.gridwidth = 3;
		this.add(nuevDesPane, c);

		/* Tabla de articulos */
		tablaArticulos = new JTable();
		tablaArticulos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaArticulos.setAutoCreateRowSorter(true);
		JScrollPane scrPanArt = new JScrollPane(tablaArticulos);
		tablaArticulos.setFillsViewportHeight(true);
		scrPanArt.setBackground(new Color(192, 203, 208));
		scrPanArt.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Articulos")),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		c.insets = new Insets(separator, separator, separator, separator);
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 2;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		this.add(scrPanArt, c);

		/* Tabla de lotes */
		tablaLote = new JTable();
		tablaLote.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrPanLot = new JScrollPane(tablaLote);
		scrPanLot.setBackground(new Color(192, 203, 208));
		tablaLote.setFillsViewportHeight(true);
		scrPanLot.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Articulos Incluidos")),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		c.gridx = 3;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		this.add(scrPanLot, c);

		/* Panel de propiedades de articulo y sus contenidos */
		idArticulo = new JTextField(12);
		idArticulo.setEditable(false);
		tipoArticulo = new JTextField(12);
		tipoArticulo.setEditable(false);
		fecha = new JTextField(12);
		fecha.setEditable(false);
		precio = new JTextField(12);
		precio.setEditable(false);
		anyoOrigen = new JTextField(12);
		anyoOrigen.setEditable(false);
		descripcion = new JTextArea(2, 12);
		descripcion.setEditable(false);
		JPanel filtrado = new JPanel();
		filtrado.setBackground(new Color(192, 203, 208));

		GridLayout filtLayout = new GridLayout(4, 4, 5, 5);
		filtrado.setLayout(filtLayout);
		filtrado.add(new JLabel("Tipo"));
		this.filtTipo = new JComboBox<String>();
		this.filtTipo.addItem("");
		this.filtTipo.addItem("Menudencia");
		this.filtTipo.addItem("Lote");
		this.filtTipo.addItem("Voluminoso");
		this.filtTipo.addItem("Arte");
		filtrado.add(filtTipo);
		filtrado.add(new JLabel("Certificado"));
		this.filtCert = new JCheckBox();
		this.filtCert.setBackground(new Color(192, 203, 208));
		filtrado.add(filtCert);
		filtrado.add(new JLabel("Fecha adqusicion"));
		this.filtFechaMin = new JTextField(12);
		filtrado.add(filtFechaMin);
		filtrado.add(new JLabel("Precio min"));
		this.filtPrecMin = new JTextField(12);
		filtrado.add(filtPrecMin);
		filtrado.add(new JLabel("Precio max"));
		this.filtPrecMax = new JTextField(12);
		filtrado.add(filtPrecMax);
		filtrado.add(new JLabel("Fecha de datacion"));
		this.filtDatacion = new JTextField(12);
		filtrado.add(this.filtDatacion);
		this.filtrar = new JButton("Filtrar");
		filtrado.add(filtrar);
		filtrado.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Filtrar")),
				new EmptyBorder(10, 10, 10, 10)));
		c.weightx = 0.2;
		c.weighty = 0.2;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 5;
		c.gridy = 0;
		this.add(filtrado, c);

		JPanel panelPropArticulo = new JPanel();
		GridLayout layoutPropiedades = new GridLayout(2, 1, 5, 5);
		panelPropArticulo.setLayout(layoutPropiedades);
		JPanel propiedades = new JPanel();
		propiedades.setAlignmentX(LEFT_ALIGNMENT);
		GridLayout propLay = new GridLayout(6, 2, 5, 5);
		propiedades.setLayout(propLay);
		propiedades.add(new JLabel("Identificador"));
		propiedades.add(idArticulo);
		propiedades.add(new JLabel("Tipo de Articulo"));
		propiedades.add(tipoArticulo);
		propiedades.add(new JLabel("Precio"));
		propiedades.add(precio);
		propiedades.add(new JLabel("Fecha Adquisicion"));
		propiedades.add(fecha);
		propiedades.add(new JLabel("Año de origen"));
		propiedades.add(anyoOrigen);
		propiedades.add(new JLabel("Descripcion"));
		propiedades.setBackground(new Color(192, 203, 208));
		panelPropArticulo.add(propiedades);
		JPanel propBig = new JPanel();
		propBig.setBackground(new Color(192, 203, 208));
		GridLayout propBigLay = new GridLayout(1, 2, 5, 5);
		propBig.setLayout(propBigLay);
		propBig.add(descripcion);
		panelPropArticulo.add(propBig);
		panelPropArticulo.setBackground(new Color(192, 203, 208));
		panelPropArticulo
				.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createTitledBorder(BorderFactory
								.createTitledBorder(BorderFactory.createEtchedBorder(), "Propiedades del Articulo")),
						new EmptyBorder(10, 10, 10, 10)));

		c.weightx = 0.2;
		c.weighty = 0.2;
		c.gridx = 6;
		c.gridy = 1;
		c.gridwidth = 1;
		c.gridheight = 2;
		this.add(panelPropArticulo, c);

		TitleLogo titleLogo = new TitleLogo("titulo2.png", 260, 130);
		c.insets = new Insets(separator, separator, separator, separator);
		c.weightx = 1.0;
		c.weighty = 0.5;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		this.add(titleLogo, c);

	}

	public void setController(ActionListener buttonController, MouseListener selector) {
		this.actController = buttonController;
		this.selController = selector;
		crearLote.addActionListener(buttonController);
		volver.addActionListener(buttonController);
		tablaLote.addMouseListener(selector);
		tablaArticulos.addMouseListener(selector);
		this.filtrar.addActionListener(buttonController);
	}

	public JTable getArticleTable() {
		return this.tablaArticulos;
	}

	public JTable getLoteTable() {
		return this.tablaLote;
	}

	public ActionListener getButtonController() {
		return this.actController;
	}

	public MouseListener getMouseListener() {
		return this.selController;
	}

	/**
	 * @return El tablaArticulos
	 */
	public JTable getTablaArticulos() {
		return tablaArticulos;
	}

	/**
	 * @return El tipoArticulo
	 */
	public JTextField getTipoArticulo() {
		return tipoArticulo;
	}

	/**
	 * @return El fecha
	 */
	public JTextField getFecha() {
		return fecha;
	}

	/**
	 * @return El precio
	 */
	public JTextField getPrecio() {
		return precio;
	}

	/**
	 * @return El anyoOrigen
	 */
	public JTextField getAnyoOrigen() {
		return anyoOrigen;
	}

	/**
	 * @return El descripcion
	 */
	public JTextArea getDescripcion() {
		return descripcion;
	}

	/**
	 * @return El idArticulo
	 */
	public JTextField getIdArticulo() {
		return idArticulo;
	}

	/**
	 * @return El filtTipo
	 */
	public JComboBox<String> getFiltTipo() {
		return filtTipo;
	}

	/**
	 * @return El filtCert
	 */
	public JCheckBox getFiltCert() {
		return filtCert;
	}

	/**
	 * @return El filtFechaMin
	 */
	public JTextField getFiltFechaMin() {
		return filtFechaMin;
	}

	/*
	 * @return El filtFechaMax
	 * 
	 * public JTextField getFiltFechaMax() { return filtFechaMax; }
	 */

	/**
	 * @return El filtPrecMin
	 */
	public JTextField getFiltPrecMin() {
		return filtPrecMin;
	}

	/**
	 * @return El filtPrecMax
	 */
	public JTextField getFiltPrecMax() {
		return filtPrecMax;
	}

	/**
	 * @return El selController
	 */
	public MouseListener getSelController() {
		return selController;
	}

	/**
	 * @param idArticulo
	 *            El idArticulo para colocar
	 */
	public void setIdArticulo(JTextField idArticulo) {
		this.idArticulo = idArticulo;
	}

	public JTextArea getNuevaDescripcion() {
		return nuevaDescripcion;
	}

	public JButton getBotonFiltrado() {
		return this.filtrar;
	}

	public JTextField getDatacion() {
		return this.filtDatacion;
	}
}
