package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import es.uam.clavi.controllers.UserPanelController;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.UserButtonNames;

public class UserPanel extends JPanel {

	private static final long serialVersionUID = 8753676673656738110L;

	private JPanel userPanel;
	private JButton nuevoCliente;
	private JButton nuevoContrato;
	private JButton actSubasta;
	private JButton articulos;
	private JButton cerrarSesion;
	private TitleLogo titleLogo;
	private JTextField currentUser;

	public UserPanel() {
		int separator = 15;

		this.setMinimumSize(new Dimension(800, 600));
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();

		/* Creamos y colocamos cada boton */
		nuevoCliente = new ClaviButton<UserButtonNames>(UserButtonNames.NVCL);
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 2;
		c.insets = new Insets(separator, separator, separator, separator);
		c.ipadx = 1;
		c.ipady = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		this.add(nuevoCliente, c);

		nuevoContrato = new ClaviButton<UserButtonNames>(UserButtonNames.NVCT);
		c.gridx = 4;
		c.gridy = 2;
		this.add(nuevoContrato, c);

		actSubasta = new ClaviButton<UserButtonNames>(UserButtonNames.SUBS);
		c.gridx = 3;
		c.gridy = 3;
		this.add(actSubasta, c);

		articulos = new ClaviButton<UserButtonNames>(UserButtonNames.ARTS);
		c.gridx = 2;
		c.gridy = 4;
		this.add(articulos, c);

		c.gridx = 1;
		c.gridy = 3;
		this.add(new FillerPanel(), c);

		c.gridx = 1;
		c.gridy = 4;
		this.add(new FillerPanel(), c);

		cerrarSesion = new ClaviButton<UserButtonNames>(UserButtonNames.CESE);
		c.gridheight = 1;
		c.insets = new Insets(separator, separator, separator, separator);
		c.gridx = 4;
		c.gridy = 4;
		this.add(cerrarSesion, c);

		this.userPanel = new JPanel();
		this.userPanel.setBackground(new Color(192, 203, 208));
		this.userPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Identificado Como")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		this.currentUser = new JTextField(40);
		this.userPanel.add(this.currentUser);
		c.anchor = GridBagConstraints.BASELINE_TRAILING;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.ipady = 20;
		c.gridx = 1;
		c.gridy = 2;
		this.add(userPanel, c);

		/* Creamos y ajustamos el titulo */
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(separator, separator, separator, separator);
		this.add(titleLogo, c);
		this.setBackground(new Color(192, 203, 208));
	}

	public void setController(UserPanelController controller) {
		nuevoCliente.addActionListener(controller);
		nuevoContrato.addActionListener(controller);
		actSubasta.addActionListener(controller);
		articulos.addActionListener(controller);
		cerrarSesion.addActionListener(controller);
	}

	public JTextField getCurrentUser() {
		return this.currentUser;
	}

}
