package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.NewClientPanelController;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.ForgeButtonNames;

public class NewClientPanel extends JPanel {

	private static final long serialVersionUID = 497354780838392161L;
	private JTextField telefono; // Obligatorio
	private JTextField nombre; // Obligatorio
	private JTextField pApellido;
	private JTextField sApellido;
	private JTextField nif;
	private JTextField dnia;
	private JTextField movil;
	private JTextField eMail;
	private JTextField direccion;
	private JTextField ciudad;
	private JTextField provincia;
	private JTextField pais;
	private JTextField cP;
	private JTextField empresa;
	private ClaviButton<ForgeButtonNames> botEnviar;
	private ClaviButton<ForgeButtonNames> botAtras;
	private TitleLogo titleLogo;
	private NewClientPanelController controller;
	private JCheckBox notificar;

	public NewClientPanel() {
		super();

		int separator = 5;
		setLayout(new GridBagLayout());
		setBackground(new Color(192, 203, 208));
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		panel.setBackground(new Color(192, 203, 208));

		notificar = new JCheckBox("Notificar", true);

		c2.gridwidth = 1;
		c2.gridheight = 1;
		c2.weightx = 1;
		c2.weighty = 0.2;
		c2.fill = GridBagConstraints.BOTH;
		c2.insets = new Insets(separator, separator, separator, separator);
		c2.gridx = 0;
		c2.gridy = 0;
		panel.add(new JLabel("Nombre"), c2);
		c2.gridx = 1;
		nombre = new JTextField(20);
		panel.add(nombre, c2);

		c2.gridx = 0;
		c2.gridy = 1;
		panel.add(new JLabel("Primer apellido"), c2);
		c2.gridx = 1;
		pApellido = new JTextField(20);
		panel.add(pApellido, c2);

		c2.gridx = 0;
		c2.gridy = 2;
		panel.add(new JLabel("Segundo apellido"), c2);
		c2.gridx = 1;
		sApellido = new JTextField(20);
		panel.add(sApellido, c2);

		c2.gridx = 0;
		c2.gridy = 3;
		panel.add(new JLabel("DNI/NIA"), c2);
		c2.gridx = 1;
		dnia = new JTextField(20);
		panel.add(dnia, c2);

		c2.gridx = 0;
		c2.gridy = 4;
		panel.add(new JLabel("Teléfono fijo"), c2);
		c2.gridx = 1;
		telefono = new JTextField(20);
		panel.add(telefono, c2);

		c2.gridx = 0;
		c2.gridy = 5;
		panel.add(new JLabel("Teléfono móvil"), c2);
		c2.gridx = 1;
		movil = new JTextField(20);
		panel.add(movil, c2);

		c2.gridx = 0;
		c2.gridwidth = 2;
		c2.gridy = 6;
		panel.add(new JLabel("Dirección de email"), c2);
		c2.gridy = 7;
		eMail = new JTextField(40);
		panel.add(eMail, c2);

		c2.gridwidth = 1;
		c2.gridy = 0;
		c2.gridx = 3;
		c2.gridwidth = 2;
		panel.add(new JLabel("Dirección postal (Calle, nº,...)"), c2);
		c2.gridy = 1;
		direccion = new JTextField(20);
		panel.add(direccion, c2);

		c2.gridwidth = 1;
		c2.gridy = 2;
		panel.add(new JLabel("Ciudad"), c2);
		c2.gridx = 4;
		ciudad = new JTextField(20);
		panel.add(ciudad, c2);

		c2.gridy = 3;
		c2.gridx = 3;
		panel.add(new JLabel("Provincia"), c2);
		c2.gridx = 4;
		provincia = new JTextField(20);
		panel.add(provincia, c2);

		c2.gridy = 4;
		c2.gridx = 3;
		panel.add(new JLabel("País"), c2);
		pais = new JTextField(20);
		c2.gridx = 4;
		panel.add(pais, c2);

		c2.gridy = 5;
		c2.gridx = 3;
		panel.add(new JLabel("Código postal"), c2);
		cP = new JTextField(10);
		c2.gridx = 4;
		panel.add(cP, c2);

		c2.gridy = 6;
		c2.gridx = 3;
		panel.add(new JLabel("Empresa"), c2);
		empresa = new JTextField(20);
		c2.gridx = 4;
		panel.add(empresa, c2);

		c2.gridx = 3;
		c2.gridy = 7;
		panel.add(new JLabel("NIF"), c2);
		nif = new JTextField(20);
		c2.gridx = 4;
		panel.add(nif, c2);
		c2.gridx = 4;
		c2.gridy = 8;
		notificar.setBackground(new Color(192, 203, 208));
		panel.add(notificar, c2);

		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Datos del cliente")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		c.weightx = 0.6;
		c.weighty = 0.6;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 2;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = 1;
		add(panel, c);

		c.weightx = 0.3;
		c.weighty = 0.3;
		botEnviar = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.CRAR);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(separator, separator, separator, separator);
		add(this.botEnviar, c);
		c.gridx = 1;
		add(new FillerPanel(), c);
		this.botAtras = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.ATRS);
		c.gridx = 3;
		add(this.botAtras, c);
		c.gridx = 2;
		add(new FillerPanel(), c);

		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(separator, separator, separator, separator);
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		add(titleLogo, c);

	}

	/**
	 * @return the telefono
	 */
	public JTextField getTelefono() {
		return telefono;
	}

	/**
	 * @return the nombre
	 */
	public JTextField getNombre() {
		return nombre;
	}

	/**
	 * @return the pApellido
	 */
	public JTextField getpApellido() {
		return pApellido;
	}

	/**
	 * @return the sApellido
	 */
	public JTextField getsApellido() {
		return sApellido;
	}

	/**
	 * @return the nif
	 */
	public JTextField getNif() {
		return nif;
	}

	/**
	 * @return the dnia
	 */
	public JTextField getDnia() {
		return dnia;
	}

	/**
	 * @return the movil
	 */
	public JTextField getMovil() {
		return movil;
	}

	/**
	 * @return the eMail
	 */
	public JTextField geteMail() {
		return eMail;
	}

	/**
	 * @return the direccion
	 */
	public JTextField getDireccion() {
		return direccion;
	}

	/**
	 * @return the ciudad
	 */
	public JTextField getCiudad() {
		return ciudad;
	}

	/**
	 * @return the provincia
	 */
	public JTextField getProvincia() {
		return provincia;
	}

	/**
	 * @return the pais
	 */
	public JTextField getPais() {
		return pais;
	}

	/**
	 * @return the cP
	 */
	public JTextField getcP() {
		return cP;
	}

	/**
	 * @return the empresa
	 */
	public JTextField getEmpresa() {
		return empresa;
	}

	public void setController(NewClientPanelController c) {
		this.controller = c;
		this.botAtras.addActionListener(c);
		this.botAtras.addKeyListener(c);
		this.botEnviar.addActionListener(c);
		this.botEnviar.addKeyListener(c);
	}

	public NewClientPanelController getController() {
		return this.controller;
	}

	public JCheckBox getNotificar() {
		return notificar;
	}
}
