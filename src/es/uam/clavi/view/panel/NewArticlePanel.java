package es.uam.clavi.view.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.clavi.controllers.NewArticleController;
import es.uam.clavi.model.gart.art.TipoObra;
import es.uam.clavi.view.components.ClaviButton;
import es.uam.clavi.view.components.FillerPanel;
import es.uam.clavi.view.components.TitleLogo;
import es.uam.clavi.view.enums.ForgeButtonNames;

public class NewArticlePanel extends JPanel {

	private static final long serialVersionUID = -8926364381092662377L;
	private JComboBox<String> tipoArt;
	private JComboBox<TipoObra> tipoObra;
	private JTextField descripcion;
	private JTextField fDat;
	private JTextField dd;
	private JTextField mm;
	private JTextField aa;
	private JTextField precioAdq;
	private JTextField precioObj;
	private JTextField ancho;
	private JTextField fondo;
	private JTextField alto;
	private JTextField peso;
	private JTextField descuento;
	private JTextField autor;
	private JCheckBox cert;
	private JLabel tipoObraLabel;
	private JLabel descuentoLabel;
	private JLabel autorLabel;
	private JLabel anchoLabel;
	private JLabel fondoLabel;
	private JLabel altoLabel;
	private JLabel pesoLabel;
	private ClaviButton<ForgeButtonNames> botEnviar;
	private ClaviButton<ForgeButtonNames> botAtras;
	private TitleLogo titleLogo;

	public NewArticlePanel() {
		super();

		int separator = 5;

		setLayout(new GridBagLayout());
		setBackground(new Color(192, 203, 208));
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		panel.setBackground(new Color(192, 203, 208));

		c2.gridwidth = 1;
		c2.gridheight = 1;
		c2.weightx = 1;
		c2.weighty = 0.2;
		c2.fill = GridBagConstraints.BOTH;
		c2.insets = new Insets(separator, separator, separator, separator);
		c2.gridx = 0;
		c2.gridy = 0;

		String[] st = { "Tipo de artículo", "Arte", "Menudencia", "Voluminoso" };
		panel.add(tipoArt = new JComboBox<>(st), c2);

		c2.gridx = 0;
		c2.gridy = 1;
		panel.add(autorLabel = new JLabel("Autor"), c2);
		c2.gridx = 1;
		panel.add(autor = new JTextField(20), c2);

		c2.gridx = 0;
		c2.gridy = 2;
		c2.gridwidth = 1;
		panel.add(new JLabel("Fecha de datación"), c2);
		c2.gridx = 1;
		panel.add(fDat = new JTextField(20), c2);

		c2.gridx = 0;
		c2.gridy = 3;
		panel.add(new JLabel("Fecha de adquisición"), c2);
		c2.gridx = 1;
		JPanel fAdq = new JPanel(new GridBagLayout());
		fAdq.setBackground(new Color(192, 203, 208));
		GridBagConstraints c3 = new GridBagConstraints();
		c3.weightx = 0.5;
		c3.weighty = 0.5;
		c3.gridx = 0;
		c3.gridy = 0;
		c3.gridheight = 1;
		c3.gridwidth = 1;
		c3.fill = GridBagConstraints.BOTH;
		fAdq.add(dd = new JTextField(2), c3);
		c3.gridx = 1;
		fAdq.add(mm = new JTextField(2), c3);
		c3.gridx = 2;
		fAdq.add(aa = new JTextField(2), c3);
		panel.add(fAdq, c2);

		c2.gridx = 0;
		c2.gridy = 4;
		panel.add(new JLabel("Precio de adquisición"), c2);
		c2.gridx = 1;
		panel.add(precioAdq = new JTextField(10), c2);

		c2.gridx = 0;
		c2.gridy = 5;
		panel.add(new JLabel("Precio objetivo"), c2);
		c2.gridx = 1;
		panel.add(precioObj = new JTextField(10), c2);

		c2.gridx = 0;
		c2.gridy = 6;
		panel.add(tipoObraLabel = new JLabel("Tipo de obra"), c2);
		panel.add(descuentoLabel = new JLabel("Descuento"), c2);
		c2.gridx = 1;
		panel.add(tipoObra = new JComboBox<TipoObra>(TipoObra.values()), c2);
		panel.add(descuento = new JTextField(3), c2);

		c2.gridx = 0;
		c2.gridy = 7;
		panel.add(new JLabel("Descripción del artículo"), c2);
		c2.gridx = 1;
		c2.gridwidth = 4;
		panel.add(descripcion = new JTextField(50), c2);

		c2.gridy = 2;
		c2.gridx = 3;
		c2.gridwidth = 1;
		panel.add(anchoLabel = new JLabel("Ancho (cm)"), c2);
		c2.gridx = 4;
		panel.add(ancho = new JTextField(10), c2);
		c2.gridx = 1;
		c2.gridy = 0;
		panel.add(cert = new JCheckBox("Certificado S/N"), c2);

		c2.gridx = 3;
		c2.gridy = 3;
		panel.add(fondoLabel = new JLabel("Fondo (cm)"), c2);
		c2.gridx = 4;
		panel.add(fondo = new JTextField(10), c2);

		c2.gridx = 3;
		c2.gridy = 4;
		panel.add(altoLabel = new JLabel("Alto (cm)"), c2);
		c2.gridx = 4;
		panel.add(alto = new JTextField(10), c2);

		c2.gridx = 3;
		c2.gridy = 5;
		panel.add(pesoLabel = new JLabel("Peso"), c2);
		c2.gridx = 4;
		panel.add(peso = new JTextField(10), c2);

		// tipoObra.setVisible(false);
		ancho.setVisible(false);
		fondo.setVisible(false);
		alto.setVisible(false);
		peso.setVisible(false);
		descuento.setVisible(false);
		// autor.setVisible(false);
		// cert.setVisible(false);
		// tipoObraLabel.setVisible(false);
		descuentoLabel.setVisible(false);
		// autorLabel.setVisible(false);
		anchoLabel.setVisible(false);
		fondoLabel.setVisible(false);
		altoLabel.setVisible(false);
		pesoLabel.setVisible(false);

		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Datos del cliente")),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		c.weightx = 0.6;
		c.weighty = 0.6;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 2;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = 1;
		add(panel, c);

		c.weightx = 0.3;
		c.weighty = 0.3;
		botEnviar = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.CRAR);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(separator, separator, separator, separator);
		add(this.botEnviar, c);
		c.gridx = 1;
		add(new FillerPanel(), c);
		this.botAtras = new ClaviButton<ForgeButtonNames>(ForgeButtonNames.ATRS);
		c.gridx = 3;
		add(this.botAtras, c);
		c.gridx = 2;
		add(new FillerPanel(), c);

		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(separator, separator, separator, separator);
		titleLogo = new TitleLogo("titulo2.png", 260, 130);
		add(titleLogo, c);
	}

	/**
	 * @return the tipoObraLabel
	 */
	public JLabel getTipoObraLabel() {
		return tipoObraLabel;
	}

	/**
	 * @return the descuentoLabel
	 */
	public JLabel getDescuentoLabel() {
		return descuentoLabel;
	}

	/**
	 * @return the autorLabel
	 */
	public JLabel getAutorLabel() {
		return autorLabel;
	}

	/**
	 * @return the anchoLabel
	 */
	public JLabel getAnchoLabel() {
		return anchoLabel;
	}

	/**
	 * @return the fondoLabel
	 */
	public JLabel getFondoLabel() {
		return fondoLabel;
	}

	/**
	 * @return the altoLabel
	 */
	public JLabel getAltoLabel() {
		return altoLabel;
	}

	/**
	 * @return the pesoLabel
	 */
	public JLabel getPesoLabel() {
		return pesoLabel;
	}

	/**
	 * @return the tipoArt
	 */
	public JComboBox<String> getTipoArt() {
		return tipoArt;
	}

	/**
	 * @return the tipoObra
	 */
	public JComboBox<TipoObra> getTipoObra() {
		return tipoObra;
	}

	/**
	 * @return the descripción
	 */
	public JTextField getDescripcion() {
		return descripcion;
	}

	/**
	 * @return the fDat
	 */
	public JTextField getfDat() {
		return fDat;
	}

	/**
	 * @return the dd
	 */
	public JTextField getDd() {
		return dd;
	}

	/**
	 * @return the mm
	 */
	public JTextField getMm() {
		return mm;
	}

	/**
	 * @return the aa
	 */
	public JTextField getAa() {
		return aa;
	}

	/**
	 * @return the precioAdq
	 */
	public JTextField getPrecioAdq() {
		return precioAdq;
	}

	/**
	 * @return the precioObj
	 */
	public JTextField getPrecioObj() {
		return precioObj;
	}

	/**
	 * @return the ancho
	 */
	public JTextField getAncho() {
		return ancho;
	}

	/**
	 * @return the fondo
	 */
	public JTextField getFondo() {
		return fondo;
	}

	/**
	 * @return the alto
	 */
	public JTextField getAlto() {
		return alto;
	}

	/**
	 * @return the peso
	 */
	public JTextField getPeso() {
		return peso;
	}

	/**
	 * @return the descuento
	 */
	public JTextField getDescuento() {
		return descuento;
	}

	/**
	 * @return the autor
	 */
	public JTextField getAutor() {
		return autor;
	}

	/**
	 * @return the cert
	 */
	public JCheckBox getCert() {
		return cert;
	}

	public void setController(NewArticleController c) {
		botEnviar.addActionListener(c);
		botEnviar.addKeyListener(c);
		botAtras.addActionListener(c);
		botAtras.addKeyListener(c);
		tipoArt.addActionListener(c);
	}
}
