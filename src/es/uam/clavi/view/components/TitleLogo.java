package es.uam.clavi.view.components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class TitleLogo extends JLabel {

	private static final long serialVersionUID = -1895211909378082668L;
	private ImageIcon icon;
	private BufferedImage image;

	public TitleLogo(String title_logo, int width, int height) {
		super();
		this.setPreferredSize(new Dimension(width, height));
		try {
			image = ImageIO.read(new File("res/" + title_logo));
			icon = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_SMOOTH));
			this.setIcon(icon);
		} catch (IOException e) {
			this.setText("Clavichord");
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		int height = getHeight();
		int width = getWidth();
		/*
		 * Hay que hacer getScaledInstance cada vez para que no quede pixelado
		 */
		/*
		 * y tampoco se puede hacer antes con un tamanyo mayor y pintarlo con
		 * menor porque
		 */
		/* da problemas */
		icon = new ImageIcon(image.getScaledInstance(width, height, Image.SCALE_SMOOTH));
		g.drawImage(icon.getImage(), 0, 0, width, height, this);
	}
}
