package es.uam.clavi.view.components;

import javax.swing.table.DefaultTableModel;

public class FilterTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	private static String[] columnNames = { "ID", "Tipo", "Fecha", "Año Origen" };

	public FilterTableModel() {
		super(columnNames, 0);
	}

	@Override
	public boolean isCellEditable(int i, int j) {
		return false;
	}
}
