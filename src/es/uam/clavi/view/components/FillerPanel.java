package es.uam.clavi.view.components;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class FillerPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public FillerPanel() {
		super();
		setPreferredSize(new Dimension(120, 120));
		setMaximumSize(new Dimension(300, 300));
		setMinimumSize(new Dimension(85, 85));
		this.setBackground(new Color(192, 203, 208));
	}
}
