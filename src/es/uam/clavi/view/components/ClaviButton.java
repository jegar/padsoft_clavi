package es.uam.clavi.view.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class ClaviButton<T> extends JButton {

	private int id;
	private ButtonEnumeration tag;

	public ClaviButton(String label, String icon_name_in_res, int id) {
		super();
		this.id = id;
		this.tag = null;
		ImageIcon pre_icon = new ImageIcon("res/" + icon_name_in_res);
		Icon icon = new ImageIcon(pre_icon.getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH));
		super.setIcon(icon);
		super.setText(label);
		super.setHorizontalTextPosition(JButton.CENTER);
		super.setVerticalTextPosition(JButton.BOTTOM);
		super.setIconTextGap(10);
		super.setBackground(new Color(235, 245, 225));
		super.setBorder(BorderFactory.createRaisedBevelBorder());
		super.setPreferredSize(new Dimension(120, 120));
		super.setMaximumSize(new Dimension(300, 300));
		super.setMinimumSize(new Dimension(85, 85));
	}

	public ClaviButton(ButtonEnumeration tag) {
		this(tag.getNam(), tag.getImg(), tag.ordinal());
		this.tag = tag;
	}

	public int getId() {
		return this.id;
	}

	public ButtonEnumeration getTag() {
		return tag;
	}
}
