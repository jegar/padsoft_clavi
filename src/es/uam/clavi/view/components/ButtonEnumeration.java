package es.uam.clavi.view.components;

public interface ButtonEnumeration {

	public String getImg();

	public String getNam();

	public int ordinal();
}
