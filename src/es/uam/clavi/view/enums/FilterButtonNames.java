package es.uam.clavi.view.enums;

import es.uam.clavi.view.components.ButtonEnumeration;

public enum FilterButtonNames implements ButtonEnumeration {
	FILTERA("<html><center>Filtrar", "filter.png", 0), FILTERAU("<html><center>Filtrar", "filter.png",
			2), FILTERC("<html><center>Filtrar", "filter.png", 1),

	SELL("<html><center>Vender", "sell.png", 0), LINK("<html><center>Ver lote", "batch_link.png", 0), LINKS(
			"<html><center>Ver subasta", "auction_gavel.png", 0), SPARE("<html><center>Deshacer lote", "delete.png", 0),
	NAUC ("Subastar","auction_gavel.png", 0),

	RENOVE("<html><center>Renovar<br>contrato", "new_deal.png", 1), MODIFY ("Modificar","tux.jpg",1), /*SUSALL(
			"<html><center>Darse de<br>alta a las<br>suscripciones", "tux.jpg",
			1), UNSUSALL("<html><center>Darse de baja<br>de suscripciones", "tux.jpg", 1),*/

	LINKA("<html><center>Ver artículo", "batch_link.png", 2), BID("<html><center>Pujar", "place_bid2.png", 2), CANCEL(
			"<html><center>Cancelar", "cancel_auction2.png", 2), SUSCRIBE("<html><center>Suscribir a<br>un usuario",
					"mail2.png", 2), UNSUSCRIBE("<html><center>Dar de baja<br>a un usuario", "mail2.png", 2),// Igual hay que
																											 // cambiarle el icono

	BACK("<html><center>Atrás", "back.png", -1);

	private final String label;
	private final String img;
	private final int inst; // Contiene al tipo de panel (Art, Cli, Sub) en una
							// enumeración del 0 al 2

	private FilterButtonNames(final String name, final String image, int in) {
		label = name;
		img = image;
		inst = in;
	}

	@Override
	public String getImg() {
		return img;
	}

	@Override
	public String getNam() {
		return label;
	}

	public int getInst() {
		return inst;
	}
}
