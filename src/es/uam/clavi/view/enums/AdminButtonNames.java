package es.uam.clavi.view.enums;

import es.uam.clavi.view.components.ButtonEnumeration;

public enum AdminButtonNames implements ButtonEnumeration {
	PRCN("<html><center>Propiedad<br>de<br>Contratos", "contract.png"), NVSB("<html><center>Nueva<br>Subasta",
			"auction_gavel.png"), NVLT("<html><center>Nuevo<br>Lote", "Moving-Boxes-and-Supplies.png"), NVAR(
					"<html><center>Nuevos<br>Articulos", "new_article.png"), ANEM("<html><center>Añadir<br>Empleado",
							"new_employee.png"), CESE("<html><center>Cerrar<br>Sesion", "logout.png"), SALI(
									"<html><center>Salir",
									"quit.png"), LIAR("<html><center>Listado<br>Articulos", "articles.png"), MOEM(
											"<html><center>Modificar<br>Empleados",
											"employees.png"), LISU("<html><center>Listado<br>Subastas", "auctions.png");

	private final String label;
	private final String img;

	private AdminButtonNames(final String name, final String imagen) {
		this.label = name;
		this.img = imagen;
	}

	@Override
	public String getImg() {
		return img;
	}

	@Override
	public String getNam() {
		return label;
	}
}
