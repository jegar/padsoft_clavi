package es.uam.clavi.view.enums;

import es.uam.clavi.view.components.ButtonEnumeration;

public enum UserButtonNames implements ButtonEnumeration {
	NVCL("<html><center>Nuevo<br>Cliente", "new_client.jpg"), NVCT("<html><center>Nuevo<br>Contrato",
			"new_deal.png"), SUBS("<html><center>Subastas", "auctions.png"), ARTS("<html><center>Articulos",
					"articles.png"), CESE("<html><center>Cerrar<br>Sesion", "logout.png"), EMPTY("", "");

	private String label;
	private String img;

	private UserButtonNames(String label, String img) {
		this.label = label;
		this.img = img;
	}

	@Override
	public String getImg() {
		return this.img;
	}

	@Override
	public String getNam() {
		return this.label;
	}

}
