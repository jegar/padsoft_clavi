package es.uam.clavi.view.enums;

import es.uam.clavi.view.components.ButtonEnumeration;

public enum LoteButtonNames implements ButtonEnumeration {
	CRLO("Crear Lote", "batch_link.png"), VLVR("Volver", "back.png");

	private String name;
	private String image;

	private LoteButtonNames(String name, String image) {
		this.name = name;
		this.image = image;
	}

	@Override
	public String getImg() {
		return this.image;
	}

	@Override
	public String getNam() {
		return this.name;
	}

}