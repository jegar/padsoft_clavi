package es.uam.clavi.view.enums;

import es.uam.clavi.view.components.ButtonEnumeration;

public enum ForgeButtonNames implements ButtonEnumeration {
	CRAR("Crear", "confirm.png"), SAVE ("Guardar cambios", "save.png"), ATRS("Atras", "back.png");

	private String image;
	private String name;

	private ForgeButtonNames(String nombre, String image) {
		this.name = nombre;
		this.image = image;
	}

	@Override
	public String getImg() {
		return this.image;
	}

	@Override
	public String getNam() {
		return this.name;
	}

}
